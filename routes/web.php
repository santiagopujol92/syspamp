<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* RUTEO FRENTE */
Route::get('home', 'HomeController@index');

/* RUTEO LOG */
Route::resource('log', 'LogController');
Route::resource('/', 'LogController');
Route::get('logout', 'LogController@logout');

/* RUTEOS PARA RESTABLEECER PASSWORD, FALTA EL UDPATE DE LA PASS SOLAMENTE */
Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/* RUTEOS MODULOS ABM */
Route::resource('usuarios', 'UserController');
Route::resource('tipo_usuarios', 'TypeUserController');
Route::resource('tipo_productos', 'TypeProductController');
Route::resource('productos', 'ProductoController');
Route::resource('condiciones_ivas', 'CondicionIvaController');
Route::resource('formas_de_pago', 'PaymentMethodController');
Route::resource('tipo_personas', 'TypePersonController');
Route::resource('personas', 'PersonController');
Route::resource('maquinas', 'MachineController');
Route::resource('estados_maquinas', 'MachineStatusController');
Route::resource('registros_logs', 'AuditController');
Route::resource('paises', 'CountryController');
Route::resource('provincias', 'ProvinceController');
Route::resource('ciudades', 'CityController');
Route::resource('empresas', 'LegalPersonController');
Route::resource('estados_remitos', 'StatusShipmentController');
Route::resource('estados_facturas', 'StatusInvoiceController');
Route::resource('facturas', 'InvoiceController');

/* RUTEOS MODULOS ABM NO REALIZADOS */
Route::resource('remitos', 'RemitController');
Route::resource('comprobantes', 'SupportController');

/* RUTEOS DE PETICIONES DE DATOS PARA LLAMADAS AJAX, EN EL CONTROLADOR TRAER DATOS Y LLAMAR DESDE JS RUTA */
Route::get('usuarios_listar', 'UserController@listing');
Route::get('tipo_usuarios_listar', 'TypeUserController@listing');
Route::get('tipo_productos_listar', 'TypeProductController@listing');
Route::get('productos_listar', 'ProductoController@listing');
Route::get('maquinas_listar', 'MachineController@listing');
Route::get('estados_maquinas_listar', 'MachineStatusController@listing');
Route::get('condiciones_ivas_listar', 'CondicionIvaController@listing');
Route::get('formas_de_pago_listar', 'PaymentMethodController@listing');
Route::get('tipo_personas_listar', 'TypePersonController@listing');
Route::get('personas_listar', 'PersonController@listing');
Route::get('paises_listar', 'CountryController@listing');
Route::get('provincias_listar', 'ProvinceController@listing');
Route::get('ciudades_listar', 'CityController@listing');
Route::get('empresas_listar', 'LegalPersonController@listing');
Route::get('estados_facturas_listar', 'StatusInvoiceController@listing');
Route::get('estados_remitos_listar', 'StatusShipmentController@listing');
Route::get('facturas_listar', 'InvoiceController@listing');

/* RUTEOS PARA DEVOLVER DATA DE SELECTS A PARTIR DE UN VALOR DE OTRO SELECT. Funciones findBy.. */
Route::get('provincias_findByCountryId/{id}', 'ProvinceController@findByCountryId');
Route::get('ciudades_findByProvinceId/{id}', 'CityController@findByProvinceId');
Route::get('productos_findByTypeProductId/{id}', 'ProductoController@findByTypeProductId');
Route::get('personas_findByTypePersonId/{id}', 'PersonController@findByTypePersonId');
Route::get('empresas_findByTypePersonId/{id}', 'LegalPersonController@findByTypePersonId');

/* PARA PERFIL DE USUARIO .. EN CONSTRUCCION*/
Route::get('profile', 'UserController@viewProfile');
Route::get('update_profile', 'UserController@updateProfile');

Auth::routes();
