﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>SysPamp v1.0 - Admin</title>
        <!-- Favicon-->
        <link rel="icon" href="../assets_md/favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="../assets_md/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="../assets_md/plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="../assets_md/plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Sweetalert Css -->
        <link href="../assets_md/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

        <!-- JQuery DataTable Css -->
        <link href="../assets_md/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="../assets_md/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

        <!-- Wait Me Css -->
        <link href="../assets_md/plugins/waitme/waitMe.css" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="../assets_md/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="../assets_md/css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="../assets_md/css/themes/all-themes.css" rel="stylesheet" />

        <link href="../mycss/mycss.css" rel="stylesheet">
        
        <!-- CSS For Each Index Page -->
        @section('css')
        @show

    </head>

    <body class="theme-red">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Cargando...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Search Bar -->
        <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            <input type="text" placeholder="START TYPING...">
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div>
        <!-- #END# Search Bar -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="home">SysPamp v1.0 - La Juanita</a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Call Search -->
                        <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                        <!-- #END# Call Search -->
                        <!-- Notifications -->
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">notifications</i>
                                <span class="label-count">7</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">NOTIFICATIONS</li>
                                <li class="body">
                                    <ul class="menu">
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-light-green">
                                                    <i class="material-icons">person_add</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4>12 new members joined</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> 14 mins ago
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-cyan">
                                                    <i class="material-icons">add_shopping_cart</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4>4 sales made</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> 22 mins ago
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-red">
                                                    <i class="material-icons">delete_forever</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4><b>Nancy Doe</b> deleted account</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> 3 hours ago
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-orange">
                                                    <i class="material-icons">mode_edit</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4><b>Nancy</b> changed name</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> 2 hours ago
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-blue-grey">
                                                    <i class="material-icons">comment</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4><b>{!!Auth::user()->name!!}</b> commented your post</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> 4 hours ago
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-light-green">
                                                    <i class="material-icons">cached</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4><b>{!!Auth::user()->name!!}</b> updated status</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> 3 hours ago
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="icon-circle bg-purple">
                                                    <i class="material-icons">settings</i>
                                                </div>
                                                <div class="menu-info">
                                                    <h4>Settings updated</h4>
                                                    <p>
                                                        <i class="material-icons">access_time</i> Yesterday
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="javascript:void(0);">View All Notifications</a>
                                </li>
                            </ul>
                        </li>
                        <!-- #END# Notifications -->
                        <!-- Tasks -->
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">flag</i>
                                <span class="label-count">9</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">TASKS</li>
                                <li class="body">
                                    <ul class="menu tasks">
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Footer display issue
                                                    <small>32%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Make new buttons
                                                    <small>45%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Create new dashboard
                                                    <small>54%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Solve transition issue
                                                    <small>65%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <h4>
                                                    Answer GitHub questions
                                                    <small>92%</small>
                                                </h4>
                                                <div class="progress">
                                                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="javascript:void(0);">View All Tasks</a>
                                </li>
                            </ul>
                        </li>
                        <!-- #END# Tasks -->
                        <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- #Top Bar -->
        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="../assets_md/images/user.png" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{!!Auth::user()->name!!} {!!Auth::user()->lastname!!}</div>
                        <div class="email">{!!Auth::user()->email!!}</div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="profile"><i class="material-icons">person</i>Profile</a></li>
                                <li role="seperator" class="divider"></li>
                                <li><a href="logout"><i class="material-icons">input</i>Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">MENU PRINCIPAL</li>
                        <li class="active">
                            <a href="home">
                                <i class="material-icons">dashboard</i>
                                <span>Tablero</span>
                            </a>
                        </li class="menu-blocked">
                        @if (Auth::user()->type == 1 or Auth::user()->type == 2 or Auth::user()->type == 3)
                            <li class="menu-blocked">
                                <a href="#">
                                    <i class="material-icons">add_shopping_cart</i>
                                    <span>Producción</span>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->type == 1 or Auth::user()->type == 2 or Auth::user()->type == 4)
                            <li class="menu-blocked">
                                <a href="#" class="menu-toggle">
                                    <i class="material-icons">build</i>
                                    <span>Mantenimiento</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="pages/tables/normal-tables.html">Normal Tables</a>
                                    </li>
                                    <li>
                                        <a href="pages/tables/jquery-datatable.html">Jquery Datatables</a>
                                    </li>
                                    <li>
                                        <a href="pages/tables/editable-table.html">Editable Tables</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        @if (Auth::user()->type == 1 or Auth::user()->type == 2 or Auth::user()->type == 5)
                            <li class="menu-blocked">
                                <a href="#">
                                    <i class="material-icons">colorize</i>
                                    <span>Laboratorio</span>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->type == 1 or Auth::user()->type == 2)
                            <li class="menu-blocked">
                                <a href="#">
                                    <i class="material-icons">assignment</i>
                                    <span>Remitos</span>
                                </a>
                            </li>
                            <li class="menu-blocked">
                                <a href="#">
                                    <i class="material-icons">local_shipping</i>
                                    <span>Insumos</span>
                                </a>
                            </li>
                            <li id="menu-facturas">
                                <a href="facturas">
                                    <i class="material-icons">payment</i>
                                    <span>Facturas</span>
                                </a>
                            </li>
                            <li id="menu-personas">
                                <a href="personas">
                                    <i class="material-icons">people</i>
                                    <span>Personas</span>
                                </a>
                            </li>
                            <li id="menu-empresas">
                                <a href="empresas">
                                    <i class="material-icons">domain</i>
                                    <span>Empresas</span>
                                </a>
                            </li>
                            <li id="menu-entradas_salidas" class="menu-blocked" >
                                <a href="#">
                                    <i class="material-icons">watch</i>
                                    <span  >Entradas/Salidas</span>
                                </a>
                            </li>
                            <li id="menu-admin">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="material-icons">https</i>
                                    <span>Administrar</span>
                                </a>
                                <ul class="ml-menu" id="submenu-admin" >
                                    <li id="submenu-admin-maquinas">
                                        <a href="maquinas" >Máquinas</a>
                                    </li>
                                    <li id="submenu-admin-estados_maquinas">
                                        <a href="estados_maquinas" >Estados de Máquinas</a>
                                    </li>
                                    <li id="submenu-admin-productos">
                                        <a href="productos">Materiales</a>
                                    </li>
                                    <li id="submenu-admin-tipo_productos">
                                        <a href="tipo_productos">Tipos de Materiales</a>
                                    </li>
                                    <li id="submenu-admin-tipo_personas">
                                        <a href="tipo_personas">Tipos de Personas</a>
                                    </li>
                                    <li id="submenu-admin-condiciones_ivas">
                                        <a href="condiciones_ivas">Condiciones de Iva</a>
                                    </li>
                                    <li id="submenu-admin-formas_de_pago">
                                        <a href="formas_de_pago">Formas de Pagos</a>
                                    </li>
                                    <li id="submenu-admin-paises">
                                        <a href="paises">Países</a>
                                    </li>
                                    <li id="submenu-admin-provincias">
                                        <a href="provincias">Provincias</a>
                                    </li>
                                    <li id="submenu-admin-ciudades">
                                        <a href="ciudades">Ciudades</a>
                                    </li>
                                    <li id="submenu-admin-estados_remitos">
                                        <a href="estados_remitos">Estados de Remitos</a>
                                    </li>
                                    <li id="submenu-admin-estados_facturas">
                                        <a href="estados_facturas">Estados de Facturas</a>
                                    </li>
                                    @if (Auth::user()->type == 1)
                                        <li id="submenu-admin-usuarios">
                                            <a href="usuarios">Usuarios</a>
                                        </li>
                                        <li id="submenu-admin-tipo_usuarios">
                                             <a href="tipo_usuarios">Tipos de Usuarios</a>
                                        </li>
                                        <li id="submenu-admin-registros_logs">
                                             <a href="registros_logs"> Registros de Actividades</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        <li class="header">Noticias</li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons col-red">donut_large</i>
                                <span>Importante</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons col-amber">donut_large</i>
                                <span>Alerta</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="material-icons col-light-blue">donut_large</i>
                                <span>Información</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- #Menu -->
                <!-- Footer -->
                <div class="legal">
                    <div class="copyright">
                        &copy; 2017 <a href="javascript:void(0);">SysPamp</a>.
                    </div>
                    <div class="version">
                        <b>Version: </b> 1.0
                    </div>
                </div>
                <!-- #Footer -->
            </aside>
            <!-- #END# Left Sidebar -->
            <!-- Right Sidebar -->
            <aside id="rightsidebar" class="right-sidebar">
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                    <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                        <ul class="demo-choose-skin">
                            <li data-theme="red" class="active">
                                <div class="red"></div>
                                <span>Red</span>
                            </li>
                            <li data-theme="pink">
                                <div class="pink"></div>
                                <span>Pink</span>
                            </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                                <span>Purple</span>
                            </li>
                            <li data-theme="deep-purple">
                                <div class="deep-purple"></div>
                                <span>Deep Purple</span>
                            </li>
                            <li data-theme="indigo">
                                <div class="indigo"></div>
                                <span>Indigo</span>
                            </li>
                            <li data-theme="blue">
                                <div class="blue"></div>
                                <span>Blue</span>
                            </li>
                            <li data-theme="light-blue">
                                <div class="light-blue"></div>
                                <span>Light Blue</span>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>
                                <span>Cyan</span>
                            </li>
                            <li data-theme="teal">
                                <div class="teal"></div>
                                <span>Teal</span>
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                                <span>Green</span>
                            </li>
                            <li data-theme="light-green">
                                <div class="light-green"></div>
                                <span>Light Green</span>
                            </li>
                            <li data-theme="lime">
                                <div class="lime"></div>
                                <span>Lime</span>
                            </li>
                            <li data-theme="yellow">
                                <div class="yellow"></div>
                                <span>Yellow</span>
                            </li>
                            <li data-theme="amber">
                                <div class="amber"></div>
                                <span>Amber</span>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                                <span>Orange</span>
                            </li>
                            <li data-theme="deep-orange">
                                <div class="deep-orange"></div>
                                <span>Deep Orange</span>
                            </li>
                            <li data-theme="brown">
                                <div class="brown"></div>
                                <span>Brown</span>
                            </li>
                            <li data-theme="grey">
                                <div class="grey"></div>
                                <span>Grey</span>
                            </li>
                            <li data-theme="blue-grey">
                                <div class="blue-grey"></div>
                                <span>Blue Grey</span>
                            </li>
                            <li data-theme="black">
                                <div class="black"></div>
                                <span>Black</span>
                            </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="settings">
                        <div class="demo-settings">
                            <p>GENERAL SETTINGS</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Report Panel Usage</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <span>Email Redirect</span>
                                    <div class="switch">
                                        <label><input type="checkbox"><span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                            <p>SYSTEM SETTINGS</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Notifications</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <span>Auto Updates</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                            <p>ACCOUNT SETTINGS</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Offline</span>
                                    <div class="switch">
                                        <label><input type="checkbox"><span class="lever"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <span>Location Permission</span>
                                    <div class="switch">
                                        <label><input type="checkbox" checked><span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- #END# Right Sidebar -->
        </section>

    @yield('content')
     
        <!-- Jquery Core Js -->
        <script src="../assets_md/plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="../assets_md/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Select Plugin Js -->
        <script src="../assets_md/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="../assets_md/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../assets_md/plugins/node-waves/waves.js"></script>

        <!-- Custom Js -->
        <script src="../assets_md/js/admin.js"></script>

        <script src="../assets_md/js/pages/ui/dialogs.js"></script>

        <!-- Demo Js -->
        <script src="../assets_md/js/demo.js"></script>

        <!-- Jquery DataTable Plugin Js -->
        <script src="../assets_md/plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="../assets_md/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

        <!-- Moment Plugin Js -->
        <script src="../assets_md/plugins/momentjs/moment.js"></script>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="../assets_md/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Bootstrap Notify Plugin Js -->
        <script src="../assets_md/plugins/bootstrap-notify/bootstrap-notify.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../assets_md/plugins/node-waves/waves.js"></script>

        <!-- SweetAlert Plugin Js -->
        <script src="../assets_md/plugins/sweetalert/sweetalert.min.js"></script>

        <!-- Scripts Personalized Functions -->
        <script src="../scripts/funciones.js"></script>

        <!-- Scripts For Each Index Page -->
        @section('scripts')
        @show

        {{-- Modal generico de Mensaje --}}
        @include('global_modals.modal_message_global')

    </body>
</html>