<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
    </div>
</div>
<div class="input-group">
    <span class="input-group-addon">
        <label class="col-red">*</label> 
        <i class="material-icons">shopping_basket</i>
    </span>
    <div class="form-line">
        <select name="type" class="form-control show-tick" required data-live-search="true" >
            <option value="0">Seleccione Tipo de Material</option>
            @foreach ($data_type_products as $reg)
                <option value="{{$reg->id}}" >{{$reg->description}}</option>
            @endforeach
        </select>
    </div>
</div>