<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true, 'autofocus' => true]) !!}
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Apellido', 'required' => true]) !!}
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">email</i>
        </span>
        <div class="form-line">
        	{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese un Correo', 'required' => true]) !!}
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red" id="label_password_required">*</label> 
            <i class="material-icons">lock</i>
        </span>
        <div class="form-line">
    	   {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Ingrese un password', 'minlength' => '6', 'required' => true]) !!}
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red" id="label_confirm_required">*</label> 
            <i class="material-icons">lock</i>
        </span>
        <div class="form-line">
           {!! Form::password('confirm', ['class' => 'form-control', 'placeholder' => 'Ingrese un password', 'minlength' => '6', 'required' => true]) !!}
        </div>
    </div>
    <div class="input-group">
        <div class="demo-radio-button" id="div_radio_type_users">
            @foreach ($data_type_users as $reg) 
                {{-- Va checkeando true hasta que llega al ultimo que es Consultor si por defecto quiere otro hacer if --}}
                <input name="type" type="radio" checked="true" value="{{$reg->id}}" id="radio_{{$reg->id}}"/> 
                <label for="radio_{{$reg->id}}">{{$reg->description}}</label>
            @endforeach
        </div>
    </div>
    <input type="checkbox" name="status"  id="status" class="filled-in chk-col-pink">
    <label for="status">Activar Usuario</label>
</div>

