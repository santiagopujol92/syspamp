@extends('layouts.main')
@section('content')

    <!-- CSS Only For This Page -->
    @section('css')
    @endsection    
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h3>Perfil de Usuario</h3>
            </div>
    		<!-- MODIFICAR -->
	        <div class="card" id="registrarUsuario">
	        	<div class="header">
	        		<h2>
	        			DATOS DE USUARIO
	        		</h2>
	        	</div>
	            <div class="body">
					{!!Form::open(['id' => 'formEditPerfil'])!!}
						 <div class="input-group">
						    <span class="input-group-addon">
						        <i class="material-icons">person</i>
						    </span>
						    <div class="form-line">
						    {!!Auth::user()->name!!}
						        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'required' => true, 'autofocus' => true]) !!}
						    </div>
						</div>
						<div class="input-group">
						{!!Auth::user()->lastname!!}
						    <span class="input-group-addon">
						        <i class="material-icons">person</i>
						    </span>
						    <div class="form-line">
						        {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Ingrese Apellido', 'required' => true]) !!}
						    </div>
						</div>
						{!!Auth::user()->password!!}
						<div class="input-group">
						    <span class="input-group-addon">
						        <i class="material-icons">lock</i>
						    </span>
						    <div class="form-line">
						        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Nueva Contraseña', 'minlength' => '6', 'required' => true]) !!}
						    </div>
						</div>
						<div class="input-group">
						    <span class="input-group-addon">
						        <i class="material-icons">lock</i>
						    </span>
						    <div class="form-line">
						        {!! Form::password('confirm', ['class' => 'form-control', 'placeholder' => 'Confirmar Nueva Contraseña', 'minlength' => '6', 'required' => true]) !!}
						    </div>
						</div>
						{!!Form::submit($title = 'GUARDAR', $attributes = ['id' => 'btnModificarPerfil', 'onclick' => 'modificarPerfil()', 'class' => 'btn btn-link bg-green waves-effect'], $secure = null)!!}
                		
	                {!! Form::close()!!}
	            </div>
	            <!-- LOADING -->
                <div id="loading_perfil"></div>
                <!-- FIN LOADING -->
	        	<!-- MENSAGE --> 
                <div id="pefil_mensaje"></div>
	            <!-- FIN MENSAJE -->
	        </div>
        </div>
    </section>
@endsection

@section('scripts')
	<script src="/scripts/usuarios.js"></script>
@endsection