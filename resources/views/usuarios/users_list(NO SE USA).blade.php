<div class="users">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Tipo</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody id="tbody_usuarios">
       <!-- RECORRO USUARIOS QUE TRAE DESDE EL MODELO -->
            @foreach ($users as $user) 
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->lastname}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->type}}</td>
                    <td>{{$user->status}}</td>
                    <td>
						<div class="icon-button-demo">
							<button type="button" href="#" data-toggle="modal" title="Editar" onclick="mostrarUsuario({{$user->id}});" data-target="#modalEditUser" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                            	<i class="material-icons">edit</i>
                           	</button>
        	             	<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminarUsuario({{$user->id}});" data-toggle="modal" data-target="#modalDeleteUser" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                				<i class="material-icons">delete</i>
                    		</button>
						</div>
                    </td>
                </tr>
            @endforeach 
        <!-- FIN RECORRIDO -->
        </tbody>
    </table>
    <!-- PAGINADOR -->
    {!!$users->render()!!}