    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">build</i>
        </span>
        <div class="form-line">
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => "Ingrese Nombre " . $modulo_msg, 'required' => true, 'autofocus' => true]) !!}
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <label class="col-red">*</label> 
            <i class="material-icons">radio_button_checked</i>
        </span>
        <div class="form-line">
            <select name="id_status_machine" class="form-control show-tick" required data-live-search="true" >
                <option value="0">Seleccione Estado de Máquina</option>
                @foreach ($data_status_machine as $reg)
                    <option value="{{$reg->id}}" >{{$reg->description}}</option>
                @endforeach
            </select>
        </div>
    </div>