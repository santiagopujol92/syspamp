<?php

namespace Syspamp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class LegalPerson extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'legal_people';

    protected $fillable = [
				    	'name', 
				    	'cuit', 
				    	'id_condicion_iva', 
				    	'id_type_people', 
				    	'id_city',
				    	'phone_1', 
				    	'phone_2', 
				    	'adress',
				    	'url',
			   		];

	protected $dates = ['deleted_at'];
}
