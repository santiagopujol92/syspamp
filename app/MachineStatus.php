<?php

namespace Syspamp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class MachineStatus extends Model implements AuditableContract
{
	use SoftDeletes, Auditable;

    protected $table = 'machine_statuses';
    protected $fillable = ['description'];
    protected $dates = ['deleted_at'];
}
