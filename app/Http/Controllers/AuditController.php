<?php

namespace Syspamp\Http\Controllers;

use Illuminate\Http\Request;
use OwenIt\Auditing\Models\Audit;
use Session;
use Redirect;

class AuditController extends Controller
{

	/*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
	private $titulo;
 	private $modulo_msg;
	private $form;
	private $module;
	private $name_file;
	private $modals_btns;

    public function __construct(){

    	/*PERMISOS DE USUARIO*/
        $this->middleware('auth');
        $this->middleware('admin');
        /**/

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'REGISTROS DE ACTIVIDAD';
        $this->modulo_msg = 'Registros de Actividad';
        $this->form = 'RegistroLog';
        $this->module = 'registros_logs';
        $this->name_file = 'registros_log';
        $this->modals_btns = 'RegistroLog';
    }

    public function index()
    {
        $data_controller = Audit::select('audits.*','users.lastname as user_lastname', 'users.name as user_name', 'type_users.description as type_user')
        	->join('users', 'audits.user_id', '=', 'users.id')
        	->join('type_users', 'users.type', '=', 'type_users.id')
        	->orderBy('users.id', 'desc')
        	->get();
			
        return view($this->module . '.' . $this->name_file . 's_index', compact('data_controller'))
        		->with('titulo', $this->titulo)
        		->with('modulo_msg', $this->modulo_msg)
        		->with('form', $this->form)
        		->with('module', $this->module)
        		->with('name_file', $this->name_file)
        		->with('modals_btns', $this->modals_btns);
    }
}
