<?php

namespace Syspamp\Http\Controllers;

use Illuminate\Http\Request;
use \Syspamp\TypePerson;  
use \Syspamp\CondicionIva;
use \Syspamp\LegalPerson;
use \Syspamp\Country;
use Session;
use Redirect;

class LegalPersonController extends Controller
{
    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'EMPRESA';
        $this->modulo_msg = 'Empresa';
        $this->form = 'Empresa';
        $this->module = 'empresas';
        $this->name_file = 'business';
        $this->modals_btns = 'Business';
        $this->model = new LegalPerson;
    }

    public function listing(){
        $data_controller = $this->model->join('condicion_ivas as ci', 'legal_people.id_condicion_iva', '=', 'ci.id')
            ->join('type_people as tp', 'id_type_people', '=', 'tp.id')
            ->join('cities as c', 'id_city', '=', 'c.id')
            ->join('provinces as p', 'c.id_province', '=', 'p.id')
            ->join('countries as cc', 'p.id_country', '=', 'cc.id')
            ->select('legal_people.*', 'ci.description as condicion_iva', 'tp.description as type_people', 'c.description as city_name', 'p.description as province_name', 'cc.description as country_name')
            ->where('legal_people.id', '!=', '1')
            ->orderBy('legal_people.created_at', 'desc')
            ->get();
        return response()->json(
            $data_controller->toArray()          
        );
    }

 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_controller = $this->model->join('condicion_ivas as ci', 'legal_people.id_condicion_iva', '=', 'ci.id')
            ->join('type_people as tp', 'id_type_people', '=', 'tp.id')
            ->join('cities as c', 'id_city', '=', 'c.id')
            ->join('provinces as p', 'c.id_province', '=', 'p.id')
            ->join('countries as cc', 'p.id_country', '=', 'cc.id')
            ->select('legal_people.*', 'ci.description as condicion_iva', 'tp.description as type_people', 'c.description as city_name', 'p.description as province_name', 'cc.description as country_name')
            ->where('legal_people.id', '!=', '1')
            ->orderBy('legal_people.created_at', 'desc')
            ->get();

        /* OBTENER DATA DE OTRA ENTIDAD A USAR EN LA VISTA, AGREGARLOS EN EL COMPACT*/
        $data_type_person = TypePerson::All();
        $data_condicion_iva = CondicionIva::All();
        $data_countries = Country::All();
        /**/

        return view($this->module . '.' . $this->name_file . '_index', compact('data_controller', 'data_type_person', 'data_condicion_iva', 'data_countries' ))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module . '.' . $this->name_file . '_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data_controller = $this->model->join('condicion_ivas as ci', 'legal_people.id_condicion_iva', '=', 'ci.id')
            ->join('type_people as tp', 'id_type_people', '=', 'tp.id')
            ->join('cities as c', 'id_city', '=', 'c.id')
            ->join('provinces as p', 'c.id_province', '=', 'p.id')
            ->join('countries as cc', 'p.id_country', '=', 'cc.id')
            ->select('legal_people.*', 'ci.description as condicion_iva', 'tp.description as type_people', 'c.description as city_name', 'p.id as id_province', 'p.description as province_name', 'cc.id as id_country', 'cc.description as country_name')
            ->where('legal_people.id', '=', $id) 
            ->get();


        return response()->json(
            $data_controller->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //El Id 1 es un default de personas legales para no tener que modificar la estructura de la db.
        if($id == 1){
            return false;
        }

        $data_controller = $this->model->find($id);
        $data_controller->fill($request->All()); //Rellena el elemento usuario con fill
        $data_controller->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //El Id 1 es un default de personas legales para no tener que modificar la estructura de la db.
        if($id == 1){
            return false;
        }

        $data_controller = $this->model->find($id);
        $data_controller->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente'      
        ]);

    }

    /**
     * Retorna las empresas filtradas por tipo de persona(CLIENTE, PROVEEDOR, EMPLEADO) sin devolver el 1 q es default
     */
    public function findByTypePersonId($id){

        $legal_people = $this->model->join('type_people as tp', 'legal_people.id_type_people', '=', 'tp.id')
            ->select('legal_people.id as id', 'legal_people.name as description')
            ->where('legal_people.id_type_people', '=', $id)
            //El id 1 no lo devolvemos porq es un default.
            ->where('legal_people.id', '!=', '1')
            ->get();

        return response()->json(
            $legal_people->toArray()
        );
    }
}
