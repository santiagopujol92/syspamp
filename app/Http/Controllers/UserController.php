<?php

namespace Syspamp\Http\Controllers;

use Illuminate\Http\Request;
use \Syspamp\User;
use \Syspamp\TypeUser;
use Session;
use Redirect;
use Illuminate\Ŗouting\Rooute; //LIBRERIA PARA PARAMETRIZAR ADENTRO DEL CONTROLADOR

class UserController extends Controller
{

    /*VARIABLES GENERICAS A MODIFICAR PARA CADA CONTROLLER*/
    private $titulo;
    private $modulo_msg;
    private $form;
    private $module;
    private $name_file;
    private $modals_btns;
    private $model;

    //CORROBORAR AUTENTIFICACION
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('admin');

        /*SETEAR VALORES DE VARIABLES GENERICAS*/
        $this->titulo = 'USUARIO';
        $this->modulo_msg = 'Usuario';
        $this->form = 'Usuario';
        $this->module = 'usuarios';
        $this->name_file = 'user';
        $this->modals_btns = 'User';
        $this->model = new User;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //METODO QUE OBTIENE TODOS DATOS, RUTEAR DESDE WEB.PHP Y LLAMAR EN PETICION AJAX
    public function listing(){
        $users = $this->model->join('type_users', 'users.type', '=', 'type_users.id')
            ->select('users.id', 'users.name', 'users.lastname', 'users.password', 'users.email', 'users.status', 'users.created_at', 'type_users.description as type_description')
            ->orderBy('users.lastname', 'asc')
            ->get();

        return response()->json(
            $users->toArray()          
        );
    }

    public function index(Request $request)
    {
        $users = $this->model->join('type_users', 'users.type', '=', 'type_users.id')
            ->select('users.id', 'users.name', 'users.password', 'users.lastname', 'users.email', 'users.status', 'users.created_at', 'type_users.description as type_description')
            ->orderBy('users.lastname', 'asc')
            ->get();
        
        $data_type_users = TypeUser::All();

        // $users = $this->model->onlyTrashed()->paginate(6); //MOSTRAR USUARIOS ELIMINADOS

        return view($this->module . '.' . $this->name_file . 's_index', compact('users', 'data_type_users'))
                ->with('titulo', $this->titulo)
                ->with('modulo_msg', $this->modulo_msg)
                ->with('form', $this->form)
                ->with('module', $this->module)
                ->with('name_file', $this->name_file)
                ->with('modals_btns', $this->modals_btns);


        /*PARA HACERLO CON RENDER MEDIANTE JSON Y IMPRIMIENDO EN BLADE users_list.blade.php*/
        // if ($request->ajax()){
        //     return response()->json(view('usuarios.users_list', compact('user', 'data_type_users'))->render());
        // }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view($this->module . '.' . $this->name_file . 's_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax())
        {
            if ($request['status'] == '')
                $request['status'] = 'off';
            
            $this->model->create($request->all());

            return response()->json([
                'mensaje' => $this->modulo_msg . ' Creado Correctamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->model->find($id);
        return response()->json(
            $user->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        if ($request['status'] == '')
        $request['status'] = 'off';
        // dump($request['password']);
        // die();
        //Excluyo el confirm del request
        $update = $request->except('confirm');

        //Si la password no se asigna Excluyo el confirm y passowrd del request para q no modifique la pass
        if ($request['password'] == ''){
            $update = $request->except('password', 'confirm');
        }
        
        $user = $this->model->find($id);
        $user->fill($update); //Rellena el elemento usuario con fill
        
        $user->save();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Modificado Correctamente'   
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->model->find($id);
        $user->delete();

        return response()->json([
            'mensaje' => $this->modulo_msg . ' Eliminado Correctamente' 
        ]);
    }

    /*va en controlador de perfilController*/
    public function viewProfile(){
        return view('usuarios.user_profile_index');
    }

    public function updateProfile(Request $request, $id){
        // $user = $this->model->find($id);
        // $user->fill($request->All());
        // $user->save();

        // return response()->json([
        //     'mensaje' => 'Perfil Modificado Correctamente'    
        // ]);    
    }
}