<?php

namespace Syspamp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Producto extends Model implements AuditableContract
{
    use SoftDeletes, Auditable;

    protected $table = 'productos';
    protected $fillable = ['description', 'type'];
    protected $dates = ['deleted_at'];

}
