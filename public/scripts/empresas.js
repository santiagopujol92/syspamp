$(document).ready(function(){
	startDatatableExportable('.datatable');
	activarMenu('empresas');
	//Configuracion de modal edit y add
	configEditAddModal('modal-center2 modal-lg');
});

var token = $('meta[name="csrf-token"]').attr('content');

//Inicializar formulario como esta por default donde corresponda
function restartConfigForm(){
	$("#form"+form+" select[name=id_province]").attr('disabled', true).selectpicker('refresh');
	$("#form"+form+" select[name=id_city]").attr('disabled', true).selectpicker('refresh');
}

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Empresa';
var form = 'Empresa';
var module = 'empresas';
var modals_btns = 'Business';

function listar(){
	var route = current_route+"_listar";

	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){
			tabla_datos.append('<tr>'
					+'<td>'+value.name+'</td>'
					+'<td>'+value.condicion_iva+'</td>'
					+'<td>'+value.type_people+'</td>'
					+'<td>'+value.city_name+'</td>'
					+'<td>'+value.province_name+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="mostrarEnModal('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatableExportable('.datatable');
 }

/*ABRIR MODAL EDIT CON DATOS*/
function mostrarEnModal(id){
	$("#modal_mensaje").addClass('hidden');
	var route = current_route+"/"+id+"/edit";

	statusLoading('loading_modal', 1);

	$.get(route, function(result){
		$("#form"+form+" input[name=name]").val(result[0]['name']);
		$("#form"+form+" input[name=cuit]").val(result[0]['cuit']);
		$("#form"+form+" input[name=adress]").val(result[0]['adress']);
		$("#form"+form+" input[name=url]").val(result[0]['url']);
		$("#form"+form+" input[name=phone_1]").val(result[0]['phone_1']);
		$("#form"+form+" input[name=phone_2]").val(result[0]['phone_2']);
		$("#form"+form+" select[name=id_condicion_iva]").val(result[0]['id_condicion_iva']).selectpicker('refresh');
		$("#form"+form+" select[name=id_type_people]").val(result[0]['id_type_people']).selectpicker('refresh');
		$("#form"+form+" select[name=id_country]").val(result[0]['id_country']).selectpicker('refresh');

		/*GENERAR SELECT DE PROVINCIAS DE ACUERDO A LA SELECCION EN PAIS Y ASIGNARLE EL VALOR SELECCIONADO*/
		changeDataSelectTarget('provincias', 'findByCountryId', 'id_province', 'Provincia', 'form'+form+'', result[0]['id_country'], result[0]['id_province']);
		/**/
		/*GENERAR SELECT DE CIUDADES DE ACUERDO A LA SELECCION EN PROVINCIAS Y ASIGNARLE EL VALOR SELECCIONADO*/
		changeDataSelectTarget('ciudades', 'findByProvinceId', 'id_city', 'Ciudad', 'form'+form+'', result[0]['id_province'], result[0]['id_city']);
		/**/

		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+')');

	});
	statusLoading('loading_modal', 0);
}