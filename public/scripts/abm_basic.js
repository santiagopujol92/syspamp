/* SE IMPORTA EN CADA VISTA, YA QUE ALGUNAS TIENEN UN ABM PARTICULAR
LAS VARIABLES QUE SE USAN SE DECLARAN EN EL {slug_modulo}.js CORRESPONDIENTE QUE INCLUYE ESTE ARCHIVO
*/

/* ESTA VARIABLE ES LA RUTA RAIZ PARA LAS LLAMADAS */
var current_route = window.location;
/**/

/* AGREGAR */
function agregar(validar){
	$("#modal_mensaje").addClass('hidden');

	/*SI VALIDAR ES TRUE SE EJECUTA EL CHECK FORM PARA VALIDAR LOS CAMPOS CON ATR required*/
	if (validar == true){
		if (checkForm("form"+form+"") == false){
			showMessage('modal_mensaje', 'bg-orange','<strong>Atención ! </strong> Debe Completar/Seleccionar los campos obligatorios <label class="col-red" >(*)</label> ', 'error');	
			return false;
		}
	}

	var route = current_route+"";
	var datos = $("#form"+form+"").serialize();

		$.ajax({
			url: route,
			headers: {  'X-CSRF-TOKEN': token},
			type: 'POST',
			dataType: 'json',
			data:datos,
            beforeSend: function () {
             	statusLoading('loading_modal', 1, 'Agregando '+modulo_msg+' ..');
            },
			success: function(result){
				listar();
				clearForm('form'+form+'');
				statusLoading('loading_modal', 0);
				showMessageModal(''+modulo_msg+' Agregado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
				$('#modal'+modals_btns+'').modal('hide');
			},
			error: function(){
				statusLoading('loading_modal', 0);
				showMessageModal('No se pudo Agregar un/a '+modulo_msg+'. <br>Complete los campos obligatorios(*) y correctamente<br> En el peor de los casos puede que este ingresando algun dato que no pueda duplicarse.', 'ATENCION', 'bg-yellow', 'warning');
			}
		})	
}

/* UPDATE */
function update(id){
	var route = current_route+"/"+id+"";
	var datos = $("#form"+form+"").serialize();

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: datos,
        beforeSend: function () {
         	statusLoading('loading_modal', 1, 'Guardando ..');
        },
		success: function(result){
			statusLoading('loading_modal', 0);
			showMessageModal(modulo_msg+' Modificado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
			listar();
			$('#modal'+modals_btns+'').modal('hide');
		},
		error: function(){
			statusLoading('loading_modal', 0);
			showMessageModal('No se pudo Modificar el/la '+modulo_msg+'. <br>Asegurese de completar todos los datos obligatorios(*) y correctos', 'ATENCIÓN', 'bg-yellow', 'warning');
		}
	})	
}

/* ELIMINAR */
function eliminar(id){
	var route = current_route+"/"+id+"";

	$.ajax({
		url: route,
		headers: {  'X-CSRF-TOKEN': token},
		type: 'DELETE',
		dataType: 'json',
		data: id,
        beforeSend: function () {
		    statusLoading('loading_list', 1, 'Eliminando/a '+modulo_msg+' ..');
        },
		success: function(result){
			showMessageModal(modulo_msg+' Eliminado/a con Éxito ', 'Buen Trabajo!', 'bg-green', 'done');
			listar();
		},
		error: function(){
			statusLoading('loading_list', 0);
			showMessageModal('No se pudo eliminar el/la '+modulo_msg+'.', 'Atención!', 'bg-yellow', 'error');
		}
	})	
}

/*ABRIR MODAL ELIMINAR*/
function abrirModalEliminar(id){
	$("#btnModalConfirmDelete"+modals_btns+"").attr('onclick', 'eliminar('+id+')');
}

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL DE EDIT EJECUTAR ACTION DEL BOTON*/
$("#modal"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#form"+form+" button[name=btnguardar"+modals_btns+"]").click();
    }
});

/*AL PRESIONAR ENTER ESTANDO EN EL MODAL DE DELETE EJECUTAR ACTION DEL BOTON*/
$("#modalDelete"+modals_btns+"").keyup(function(event){
    if(event.keyCode == 13){
        $("#btnModalConfirmDelete"+modals_btns+"").click();
    }
});

//Agregar clase al modal de edit y Add para el tamaño
function configEditAddModal(class_modal){
	$("#modal_size_class").addClass(class_modal);
}

//Setear boton para agregar
function configSaveButtonAdd(){
	$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'agregar(true)');
}