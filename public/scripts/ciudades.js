$(document).ready(function(){
	startDatatable('.datatable');
	activarMenu('admin', 'ciudades');
})

var token = $('meta[name="csrf-token"]').attr('content');

/* DATOS DINAMICOS A COMPLETAR PARA CADA ABM */
var modulo_msg = 'Ciudad';
var form = 'Ciudad';
var module = 'ciudades';
var modals_btns = 'Citie';

function listar(){
	var route = current_route+"_listar";
	
	var tabla_datos = $("#tbody_"+module+"");
    statusLoading('loading_list', 1, 'Actualizando Listado ..');

	$.get(route, function(result){
		tabla_datos.empty();
		$(result).each(function(key,value){
			tabla_datos.append('<tr>'
					+'<td>'+value.description+'</td>'
					+'<td>'+value.province_name+'</td>'
					+'<td>'+value.country_name+'</td>'
					+'<td>'+formatDateTime(value.created_at, 'd-m-Y h:m:s')+'</td>'
					+'<td>'
						+'<div class="icon-button-demo">'
							+'<button type="button" href="#" data-toggle="modal" title="Editar" onclick="mostrarEnModal('+value.id+');" data-target="#modal'+modals_btns+'" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">'
                            	+'<i class="material-icons">edit</i>'
                           	+'</button>'
        	             	+'<button type="button" href="#" title="Eliminar" onclick="abrirModalEliminar('+value.id+');" data-toggle="modal" data-target="#modalDelete'+modals_btns+'" class="btn bg-red btn-circle waves-effect waves-circle waves-float">'
                				+'<i class="material-icons">delete</i>'
                    		+'</button>'
						+'</div>'
					+'</td>'
				+"</tr>");
		});
	});
	statusLoading('loading_list', 0);
	startDatatable('.datatable');
 }

/*ABRIR MODAL EDIT CON DATOS*/
function mostrarEnModal(id){
	$("#modal_mensaje").addClass('hidden');
	var route = current_route+"/"+id+"/edit";
	$("#form"+form+"").attr('method', 'PUT');
	
	statusLoading('loading_modal', 1);

	$.get(route, function(result){
		$("#form"+form+" input[name=description]").val(result[0]['description']);
		$("#form"+form+" select[name=id_country]").val(result[0]['id_country']).selectpicker('refresh');

		/*GENERAR SELECT DE PROVINCIAS DE ACUERDO A LA SELECCION EN PAIS Y ASIGNARLE EL VALOR SELECCIONADO*/
		changeDataSelectTarget('provincias', 'findByCountryId', 'id_province', 'Provincia', 'form'+form+'', result[0]['id_country'], result[0]['id_province']);
		/**/

		$("#form"+form+" button[name=btnguardar"+modals_btns+"]").attr('onclick', 'update('+id+')');
	});
	statusLoading('loading_modal', 0);
}