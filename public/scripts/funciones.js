/* ESTA VARIABLE (RUTA RAIZ PARA LAS LLAMADAS) SE MODIFICA DE ACUERDO A DONDE SE SUBA EL PROYECTO*/
var root_project = "http://localhost:8000/";
// var root_project = "http://drinky.org/syspamp/public/index.php/";

function showMessage(inputId, color, msg, icon = '', closebutton = true, msgPosition = 'center'){
	$("#"+inputId+"").removeClass();
	$("#"+inputId+"").addClass("alert alert-dismissible " + color);
	$("#"+inputId+"").attr('align', msgPosition).attr('role', 'alert');

	if (closebutton == true){
		$("#"+inputId+"").html('<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
		                        	+'<span aria-hidden="true">&times;</span>'
		                      +'</button>'+msg+"");	
	}else{
		$("#"+inputId+"").html(''+msg+"<i class='material-icons'>"+icon+"</i>");	
	}

	setTimeout(function(){
        $("#"+inputId+"").empty().removeClass();
    }, 6000);
}

function clearForm(form){
	$("#"+form+"").trigger("reset");
    $("#"+form+" select").val(0);
    $("#"+form+" select").val(0).selectpicker('refresh');
}

function resetearPassword(formId){
	var email = $("#"+formId+" input[name=email]").val();
	showMessage('email_reset_mensaje', 'bg-green','Nueva contraseña enviada a <strong>'+email+'</strong>', '');	
}

function statusLoading(idDivLoading, status = 1, text = 'Cargando ..'){                   
	if (status == 1){
		$("#"+idDivLoading+"").html('<div style="margin-bottom:10px;"><p style="text-align:center;"><b>'
                                        +'<img width="30px" style="margin-bottom:5px;" src="../assets_md/images/Rolling.gif"/> ' +text+ '</b>'
                                    +'</p></div>'
									);
		$("#"+idDivLoading+"").removeClass("hidden");
	}else if (status == 0){
		$("#"+idDivLoading+"").addClass("hidden");
	}
}

//INICIALIZAR DATATABLES
function startDatatable(selectorTabla){
    $(selectorTabla).DataTable( {
        destroy: true,
        searching: true,
        paging: true,
        responsive: true,
        searching: true
    });
}

//INICIALIZAR DATATABLES EXPORTABLES
function startDatatableExportable(selectorTabla){
    $(selectorTabla).DataTable({
        dom: 'Bfrtip',
        destroy: true,
        searching: true,
        paging: true,
        responsive: true,
        searching: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}

//INICIALIZAR DATEPICKER POR SELECTOR
function startDatePicker(selectorPicker){
    $(selectorPicker).bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY',
        clearButton: true,
        weekStart: 1
    });
}

/* GENERAR SELECT DE CUALQUIER MODULO CON ID DESCRIPCION*/
function generarSelect(route, divIdSelect, nameSelect, formSelect, defaultOption, defaultValue){
    var route = root_project+route+"";
    var div = $("#"+divIdSelect+"");

    div.html('<select name="'+nameSelect+'" class="form-control show-tick" required data-live-search="true" dropdown>z');
    $("#"+formSelect+" select[name="+nameSelect+"]").append('<option value="'+defaultValue+'">Seleccione '+defaultOption+'</option')

    $.get(route, function(result){
        $(result).each(function(key,value){
            $("#"+formSelect+" select[name="+nameSelect+"]").append('<option value="'+value.id+'">'+value.description+'</option>'); 
        });
    });

}

//FUNCION CHECK FORM PARA VALIDAR NULOS "solo los que tienen el atr required"
function checkForm(idForm){
    var retorno = true;
    $('form#'+idForm+'').find('input').each(function(){
        if($(this).prop('required') && !$(this).hasClass('hidden')){
            if (this.value == ''){
                retorno = false;
                return false;
            }
        }
    });

    /*PARA SELECTS*/
    $('form#'+idForm+'').find('select').each(function(){
        if($(this).prop('required') && !$(this).hasClass('hidden')){
            if (this.value == 0 || this.value == ''){
                retorno = false;
                return false;
            }
        }
    });
    return retorno;
}

//VALIDAR EMAIL
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

//ACTIVAR MENU
// El menu es el boton principal que llama la seccion.
// Si el menu tiene desplegable se utiliza submenu para los submenues.
// EL nombre de submenu debe ser igual al href del boton que hace el submenu EN EL MAIN.BLADE
function activarMenu(menu, submenu){

    //BORRAMOS EL ACTIVO TODOS LOS LI PRINCIPALES DE MENU
    $('.menu .list').find('li').each(function(){
        $(this).removeClass('active');
    });

    //ASIGNAMOS EL ACTIVO A LA NUEVA SECCION
    $("#menu-"+menu+"").addClass('active');

    //ASIGNAMOS EL ACTIVO CON SU ICONO DE SUBMENU SI TIENE
    if (submenu != ''){
        $("#submenu-"+menu+"").show();
        $("#submenu-"+menu+"-"+submenu+"").prop('class', 'active2');
        $("#submenu-"+menu+"-"+submenu+"").addClass('active2');
        $("#submenu-"+menu+" [href='/"+submenu+"']").prepend('<i class="material-icons submenu-icon-custom">keyboard_arrow_right</i> ');
    }
}

//FUNCION PARA FORMATEAR CON DATETIME, SE FILTRA POR COUNTRY AHORA, PERO SI SE USA MAS SE DEBE HACER POR FORMATO
//Puede recibir un formato:
//2017-08-06 22:47:01
//2017-08-06 22:47
//2017-08-06 22
//2017-08-06
//Caso contrario modificar funcion
function formatDateTime(datetime, format){
    var retorno = '';

    if (datetime != null && datetime != ''){
        var dateArr;
        var fullDateTime
        var hourArr;

        if (format == 'd-m-Y'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0];
            retorno = new_date;
        }

        if (format == 'd-m-Y h'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1];
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0] + ' ' + hourArr[0];
            retorno = new_date;
        }

        if (format == 'd-m-Y h:m'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1].split(':');
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0] + ' ' + hourArr[0] + ':' + hourArr[1];
            retorno = new_date;
        }

        if (format == 'd-m-Y h:m:s'){
            fullDateTime = datetime.split(' ');
            dateArr = fullDateTime[0].split('-');
            hourArr = fullDateTime[1].split(':');
            new_date = dateArr[2] + '/' + dateArr[1] + '/' + dateArr[0] + ' ' + hourArr[0] + ':' + hourArr[1] + ':' + hourArr[2];
            retorno = new_date;
        }
    }
    return retorno;
}

/*
Funcion para generar un select desp de seleccionar valor en otro, los parametros se identifican,
la data que debe recibir es tipo arary y solo nombres id y description, si posee algun otro campo 
el result darle alias correspondiente
La ruta dirigida debe ser ejemplo: provincias_findByCountry/{id} y en el controlador correspondiente con su funcion
*/
function changeDataSelectTarget(modulo, func, targetSelectName, moduleMsg, formId, valueSelectedSource, optionSelectedTarget, defaultMsg = true){
    var route = root_project+modulo+"_"+func+"/"+valueSelectedSource+"";

    var targetSelect = $("#"+formId+" select[name="+targetSelectName+"]")

    if (valueSelectedSource == 0){
        //Anular select target si se selecciona 0 en el source
        validateNoSelectionToSelect(targetSelectName, moduleMsg, formId, valueSelectedSource);
    }else{
        $.ajax({
            url: route,
            headers: {  'X-CSRF-TOKEN': token},
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                targetSelect.html('<option value="">Cargando '+moduleMsg+'s ..</option>');
            },
            success: function(result){
                targetSelect.attr('disabled', false);
                targetSelect.html('<option value="0">Seleccione '+moduleMsg+'</option>');
                
                // //Default Msg falso y se anula el option 0 por 1(Utilizado para facturas con el tema de personas y empresas)
                if (!defaultMsg){
                    targetSelect.html('<option value="1">'+moduleMsg+' No Asignada - </option>');
                }

                $(result).each(function(index, element) {
                    if (element.description2 && element.description3){
                        targetSelect.append('<option value="'+element.id+'">'+element.description+' '+element.description2+' - '+element.description3+'</option>');
                    }else if(element.description2){
                        targetSelect.append('<option value="'+element.id+'">'+element.description+' '+element.description2+'</option>');
                    }else{
                        targetSelect.append('<option value="'+element.id+'">'+element.description+'</option>');
                    }
                });

                /*VALOR SELECCIONADO EN EL TARGET SOLO SI VIENE LA VARIABLE Y ES MAYOR A 0*/
                if (optionSelectedTarget && optionSelectedTarget > 0){
                    targetSelect.val(optionSelectedTarget);
                }

                targetSelect.selectpicker('refresh');
            },
            error: function(){
                console.log("No se pudo obtener los datos");
            }
        })    
    }
}

/**
* Funcion para anular un select luego de un onchange to 0 de otro select
* Es utilizada cuando se selecciona 0 en la funcion anterior en el source select, y se afecta el target select
*/
function validateNoSelectionToSelect(nameSelect, textDefaultDisplay, formId, valueSelected){
    var select = $("#"+formId+" select[name="+nameSelect+"]")
    if (valueSelected == 0){
        select.attr('disabled', true);
        select.html('<option value="0">Seleccione '+textDefaultDisplay+'</option>');
        select.val(0).selectpicker('refresh');
    }
}

/**
* Mostrar mensaje generico en modal parametrizado, se debe incluir un archivo modal_message_global_blade.php
* Se ejecuta un boton que abre el modal para que funcione como modal y luego se configura el contenido por parametro
*/
/**
* Se modifico mensaje con otro tipo de modal y los iconos cambiaron y se excluyo background.
*/
function showMessageModal(msg, title = 'ATENCIÓN', background = 'bg-yellow', icon = 'warning'){
    $("#executeMessageModal").click();
    // $("#modalMessageHeader").removeClass();
    // $("#modalMessageHeader").addClass('modal-header '+background);

    if (icon == 'done'){
        icon = '<div class="sa-icon sa-success animate" style="display: block;">'
                    +'<span class="sa-line sa-tip animateSuccessTip"></span>'
                    +'<span class="sa-line sa-long animateSuccessLong"></span>'
                    +'<div class="sa-placeholder"></div>'
                    +'<div class="sa-fix"></div>'
               +'</div>';
    }else if (icon == 'error'){
        icon = '<div class="sa-icon sa-error animateErrorIcon" style="display: block;">'
                    +'<span class="sa-x-mark animateXMark">'
                        +'<span class="sa-line sa-left"></span>'
                        +'<span class="sa-line sa-right"></span>'
                  +'</span>'
               +'</div>';
    }else if (icon == 'warning'){
        icon = '<div class="sa-icon sa-warning pulseWarning" style="display: block;">'
                   +'<span class="sa-body pulseWarningIns"></span>'
                   +'<span class="sa-dot pulseWarningIns"></span>'
                +'</div>';
    }

    $("#modalMessageIcon").html(icon);
    $("#modalMessageBody").html(msg);
    $("#modalMessageTitle").html(title);

}









//------------------OTRAS NO UTILIZADOS POR EL MOMENTO -------------------------

function cargarLoading(divId, msg = '', width = ''){
    $("#"+divId+"").html("<div algin='center' style='text-align:center'>"+msg+"<img width='"+width+"' src='../images/ring-alt.gif'></img></div>");
}

function cargarProgressBar(input, porcentaje, texto = ''){
	$("."+input+"").css('width', porcentaje);
	$("."+input+"").attr('aria-valuenow', porcentaje);
	$("."+input+"").html(porcentaje + ' ' + texto);
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : ((event != undefined) ? event.keyCode : 0);
    if (charCode != 44 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isIntegerNumberKey(evt){
    var charCode = (evt.which) ? evt.which : ((event != undefined) ? event.keyCode : 0);
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

//VALIDAR CUIT
function esCUITValida(inputValor) {
    inputString = inputValor.toString()
    if (inputString.length == 11) {
        var Caracters_1_2 = inputString.charAt(0) + inputString.charAt(1)
        if (Caracters_1_2 == "20" || Caracters_1_2 == "23" || Caracters_1_2 == "24" || Caracters_1_2 == "27" || Caracters_1_2 == "30" || Caracters_1_2 == "33" || Caracters_1_2 == "34") {
            var Count = inputString.charAt(0) * 5 + inputString.charAt(1) * 4 + inputString.charAt(2) * 3 + inputString.charAt(3) * 2 + inputString.charAt(4) * 7 + inputString.charAt(5) * 6 + inputString.charAt(6) * 5 + inputString.charAt(7) * 4 + inputString.charAt(8) * 3 + inputString.charAt(9) * 2 + inputString.charAt(10) * 1
            Division = Count / 11;
            if (Division == Math.floor(Division)) {
                return true
            }
        }
    }
    return false;
}