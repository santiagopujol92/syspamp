<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('type_users')->insert([
        	'id' => 1,
            'description' => 'Admin'
        ]);
        DB::table('type_users')->insert([
        	'id' => 2,
            'description' => 'Administracion'
        ]);
        DB::table('type_users')->insert([
        	'id' => 3,
            'description' => 'Produccion'
        ]);
        DB::table('type_users')->insert([
        	'id' => 4,
            'description' => 'Mantenimiento'
        ]);
        DB::table('type_users')->insert([
        	'id' => 5,
            'description' => 'Laboratorio'
        ]);
        DB::table('type_users')->insert([
        	'id' => 6,
            'description' => 'Consultor'
        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'lastname' => 'Admin',
            'email' => 'admin',
            'password' => bcrypt('asd123'),
            'type' => '1',
            'status' => 'on',
        ]);
        DB::table('users')->insert([
            'name' => 'Administracion',
            'lastname' => 'Administracion',
            'email' => 'administracion',
            'password' => bcrypt('asd123'),
            'type' => '2',
            'status' => 'on',
        ]);
        DB::table('users')->insert([
            'name' => 'Produccion',
            'lastname' => 'Produccion',
            'email' => 'produccion',
            'password' => bcrypt('asd123'),
            'type' => '3',
            'status' => 'on',
        ]);
        DB::table('users')->insert([
            'name' => 'Mantenimiento',
            'lastname' => 'Mantenimiento',
            'email' => 'mantenimiento',
            'password' => bcrypt('asd123'),
            'type' => '4',
            'status' => 'on',
        ]);
        DB::table('users')->insert([
            'name' => 'Laboratorio',
            'lastname' => 'Laboratorio',
            'email' => 'laboratorio',
            'password' => bcrypt('asd123'),
            'type' => '5',
            'status' => 'on',
        ]);
        DB::table('users')->insert([
            'name' => 'Consultor',
            'lastname' => 'Consultor',
            'email' => 'consultor',
            'password' => bcrypt('asd123'),
            'type' => '6',
            'status' => 'on',
        ]);
        DB::table('people')->insert([
            'id' => '1',
            'name' => 'Persona',
            'lastname' => 'No Asignada',
            'email' => 'admin@syspamp.com',
            'cuit' => '',
            'id_condicion_iva' => '1',
            'id_type_people' => '1',
            'id_city' => '3',
            'adress' => '',
            'floor' => NULL,
            'department' => NULL,
            'phone_1' => NULL,
            'phone_2' => NULL,
            'phone_3' => NULL
        ]);

        DB::table('legal_people')->insert([
            'id' => '1',
            'name' => 'Empresa No Asignada',
            'cuit' => '',
            'id_condicion_iva' => '1',
            'id_type_people' => '1',
            'id_city' => '3',
            'adress' => NULL
        ]);
    }
}
