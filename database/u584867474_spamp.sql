-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-08-2018 a las 04:00:45
-- Versión del servidor: 10.2.16-MariaDB
-- Versión de PHP: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u584867474_juani`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audits`
--

CREATE TABLE `audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` int(10) UNSIGNED NOT NULL,
  `auditable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_values` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_values` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `audits`
--

INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(1, 11, 'updated', 11, 'Syspamp\\User', '{\"lastname\":\"admin\"}', '{\"lastname\":\"adminaso\"}', 'http://localhost:8000/usuarios/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-06-29 08:14:07'),
(2, 11, 'created', 45, 'Syspamp\\User', '[]', '{\"name\":\"SANTIAGO\",\"lastname\":\"PUJOL\",\"email\":\"santiag@ASDA\",\"password\":\"$2y$10$0ZnEXWzrGl2QJCw05d2Quu38KwpXruUbBEHzcObFgRshqXQetA4b2\",\"type\":\"1\",\"status\":\"on\",\"id\":45}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-06-29 08:14:47'),
(3, 11, 'created', 49, 'Syspamp\\User', '[]', '{\"name\":\"aSD\",\"lastname\":\"ASD\",\"email\":\"ASD@ASD.COM\",\"password\":\"$2y$10$7gDihFZLgTarllOoyqkhVePvlTh7xm9H5W82HsQwRBPyQ6rnyfWcG\",\"type\":\"1\",\"status\":\"on\",\"id\":49}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-06-29 08:25:06'),
(4, 11, 'created', 57, 'Syspamp\\User', '[]', '{\"name\":\"ASDSA\",\"lastname\":\"ASD\",\"email\":\"AdD\",\"password\":\"$2y$10$UIzqr0LDAXScGJcVoSlBku6anEw4ZhPbdAfaHCJt78GA\\/ZcOQz.5u\",\"type\":\"1\",\"status\":\"on\",\"id\":57}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-06-29 08:33:00'),
(5, 11, 'created', 61, 'Syspamp\\User', '[]', '{\"name\":\"ASD\",\"lastname\":\"ASD\",\"email\":\"TEST@TEST.COM.CAR\",\"password\":\"$2y$10$sZDCP2CkAM\\/6yPH.Pj.z5eJvOP0BS3CYm0n564JjriB84TWaUjTG6\",\"type\":\"1\",\"status\":\"on\",\"id\":61}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-06-29 08:36:45'),
(6, 11, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"PRUEBA\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-06-29 08:40:56'),
(7, 11, 'created', 1, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Feldespato\",\"id\":1}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 21:34:46'),
(8, 11, 'created', 2, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"ASDSAD\",\"id\":2}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 21:42:18'),
(9, 11, 'created', 3, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Asdsad\",\"id\":3}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:05:29'),
(10, 11, 'deleted', 3, 'Syspamp\\TypeProduct', '{\"id\":3,\"description\":\"Asdsad\",\"deleted_at\":\"2017-07-16 19:05:42\"}', '[]', 'http://localhost:8000/tipo_productos/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:05:42'),
(11, 11, 'deleted', 2, 'Syspamp\\TypeProduct', '{\"id\":2,\"description\":\"ASDSAD\",\"deleted_at\":\"2017-07-16 19:05:50\"}', '[]', 'http://localhost:8000/tipo_productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:05:50'),
(12, 11, 'updated', 1, 'Syspamp\\TypeProduct', '{\"description\":\"Feldespato\"}', '{\"description\":\"JAJAJ\"}', 'http://localhost:8000/tipo_productos/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:32:12'),
(13, 11, 'updated', 1, 'Syspamp\\TypeProduct', '{\"description\":\"JAJAJ\"}', '{\"description\":\"madsad\"}', 'http://localhost:8000/tipo_productos/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:32:36'),
(14, 11, 'deleted', 1, 'Syspamp\\TypeProduct', '{\"id\":1,\"description\":\"madsad\",\"deleted_at\":\"2017-07-16 19:32:54\"}', '[]', 'http://localhost:8000/tipo_productos/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:32:54'),
(15, 11, 'created', 4, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"feas\",\"id\":4}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:32:58'),
(16, 11, 'updated', 4, 'Syspamp\\TypeProduct', '{\"description\":\"feas\"}', '{\"description\":\"SI BOLUDO\"}', 'http://localhost:8000/tipo_productos/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:33:05'),
(17, 11, 'created', 5, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"feas\",\"id\":5}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:33:08'),
(18, 11, 'created', 6, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"no no voy nada\",\"id\":6}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-16 22:33:12'),
(19, 11, 'deleted', 4, 'Syspamp\\TypeProduct', '{\"id\":4,\"description\":\"SI BOLUDO\",\"deleted_at\":\"2017-07-16 23:04:05\"}', '[]', 'http://localhost:8000/tipo_productos/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:04:05'),
(20, 11, 'deleted', 5, 'Syspamp\\TypeProduct', '{\"id\":5,\"description\":\"feas\",\"deleted_at\":\"2017-07-16 23:04:09\"}', '[]', 'http://localhost:8000/tipo_productos/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:04:09'),
(21, 11, 'updated', 6, 'Syspamp\\TypeProduct', '{\"description\":\"no no voy nada\"}', '{\"description\":\"AJAJA\"}', 'http://localhost:8000/tipo_productos/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:04:19'),
(22, 11, 'created', 7, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"JAJAJA\",\"id\":7}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:05:36'),
(23, 11, 'updated', 6, 'Syspamp\\TypeProduct', '{\"description\":\"AJAJA\"}', '{\"description\":\"no no va nada\"}', 'http://localhost:8000/tipo_productos/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:05:44'),
(24, 11, 'deleted', 7, 'Syspamp\\TypeProduct', '{\"id\":7,\"description\":\"JAJAJA\",\"deleted_at\":\"2017-07-16 23:05:48\"}', '[]', 'http://localhost:8000/tipo_productos/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:05:48'),
(25, 11, 'created', 8, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"JAJAJA\",\"id\":8}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:06:08'),
(26, 11, 'created', 9, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"DADSAD\",\"id\":9}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:06:32'),
(27, 11, 'deleted', 6, 'Syspamp\\TypeProduct', '{\"id\":6,\"description\":\"no no va nada\",\"deleted_at\":\"2017-07-16 23:06:40\"}', '[]', 'http://localhost:8000/tipo_productos/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:06:40'),
(28, 11, 'deleted', 8, 'Syspamp\\TypeProduct', '{\"id\":8,\"description\":\"JAJAJA\",\"deleted_at\":\"2017-07-16 23:10:27\"}', '[]', 'http://localhost:8000/tipo_productos/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:10:27'),
(29, 11, 'deleted', 9, 'Syspamp\\TypeProduct', '{\"id\":9,\"description\":\"DADSAD\",\"deleted_at\":\"2017-07-16 23:11:53\"}', '[]', 'http://localhost:8000/tipo_productos/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:11:53'),
(30, 11, 'created', 10, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Feldespato\",\"id\":10}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:11:57'),
(31, 11, 'created', 11, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Piedra\",\"id\":11}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:12:14'),
(32, 11, 'created', 12, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Polvo\",\"id\":12}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:12:19'),
(33, 11, 'deleted', 10, 'Syspamp\\TypeProduct', '{\"id\":10,\"description\":\"Feldespato\",\"deleted_at\":\"2017-07-16 23:12:21\"}', '[]', 'http://localhost:8000/tipo_productos/10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:12:21'),
(34, 11, 'updated', 11, 'Syspamp\\User', '{\"remember_token\":\"fLN1NebDoAQ6NU5DKm9EalSz4MNopgzr6KORK1bNIZ27ED5K6ZMB18xOq97Z\"}', '{\"remember_token\":\"y4HdWmi4CwyEu5KoP7sbp9RJlRb0M89UDCDGSd8vB0XcbIDeWn1jqdoG1u4H\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:24:23'),
(35, 11, 'deleted', 7, 'Syspamp\\TypeUser', '{\"id\":7,\"description\":\"PRUEBA\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:34:14'),
(36, 11, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"TEST\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:43:06'),
(37, 11, 'deleted', 15, 'Syspamp\\TypeUser', '{\"id\":15,\"description\":\"TEST\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:43:21'),
(38, 11, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"asd\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:44:39'),
(39, 11, 'deleted', 115, 'Syspamp\\TypeUser', '{\"id\":115,\"description\":\"asd\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/115', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:45:26'),
(40, 11, 'created', 62, 'Syspamp\\User', '[]', '{\"name\":\"TESdt\",\"lastname\":\"test\",\"email\":\"teste@test.com\",\"password\":\"$2y$10$XerjPfaENq3avHr8JKs9S.ta2Gac6ioQ.00i228rTd0gagytedb8a\",\"type\":\"2\",\"status\":\"on\",\"id\":62}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:57:30'),
(41, 11, 'updated', 62, 'Syspamp\\User', '{\"type\":2}', '{\"type\":\"5\"}', 'http://localhost:8000/usuarios/62', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:57:45'),
(42, 11, 'updated', 62, 'Syspamp\\User', '{\"type\":5}', '{\"type\":\"3\"}', 'http://localhost:8000/usuarios/62', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 02:57:52'),
(43, 11, 'created', 13, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"JAJAJA\",\"id\":13}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 03:44:42'),
(44, 11, 'created', 14, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Asdsad\",\"id\":14}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 03:45:09'),
(45, 11, 'created', 15, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"DSFDS\",\"id\":15}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 03:45:23'),
(46, 11, 'deleted', 15, 'Syspamp\\TypeProduct', '{\"id\":15,\"description\":\"DSFDS\",\"deleted_at\":\"2017-07-17 00:45:25\"}', '[]', 'http://localhost:8000/tipo_productos/15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 03:45:25'),
(47, 11, 'deleted', 14, 'Syspamp\\TypeProduct', '{\"id\":14,\"description\":\"Asdsad\",\"deleted_at\":\"2017-07-17 00:45:30\"}', '[]', 'http://localhost:8000/tipo_productos/14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 03:45:30'),
(48, 11, 'deleted', 13, 'Syspamp\\TypeProduct', '{\"id\":13,\"description\":\"JAJAJA\",\"deleted_at\":\"2017-07-17 00:45:32\"}', '[]', 'http://localhost:8000/tipo_productos/13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 03:45:32'),
(49, 11, 'created', 1, 'Syspamp\\Producto', '[]', '{\"description\":\"Feldespato\",\"type\":\"11\",\"id\":1}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:45:38'),
(50, 11, 'created', 2, 'Syspamp\\Producto', '[]', '{\"description\":\"Moco\",\"type\":\"12\",\"id\":2}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:51:17'),
(51, 11, 'created', 3, 'Syspamp\\Producto', '[]', '{\"description\":\"caca\",\"type\":\"11\",\"id\":3}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:52:04'),
(52, 11, 'deleted', 3, 'Syspamp\\Producto', '{\"id\":3,\"description\":\"caca\",\"type\":11,\"deleted_at\":\"2017-07-17 03:52:08\"}', '[]', 'http://localhost:8000/productos/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:52:08'),
(53, 11, 'deleted', 1, 'Syspamp\\Producto', '{\"id\":1,\"description\":\"Feldespato\",\"type\":11,\"deleted_at\":\"2017-07-17 03:52:11\"}', '[]', 'http://localhost:8000/productos/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:52:11'),
(54, 11, 'created', 16, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"CACA\",\"id\":16}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:58:47'),
(55, 11, 'deleted', 16, 'Syspamp\\TypeProduct', '{\"id\":16,\"description\":\"CACA\",\"deleted_at\":\"2017-07-17 03:59:01\"}', '[]', 'http://localhost:8000/tipo_productos/16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 06:59:01'),
(56, 11, 'created', 4, 'Syspamp\\Producto', '[]', '{\"description\":\"asdsa\",\"type\":\"12\",\"id\":4}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:07:15'),
(57, 11, 'created', 5, 'Syspamp\\Producto', '[]', '{\"description\":\"adas\",\"type\":\"11\",\"id\":5}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:10:03'),
(58, 11, 'deleted', 5, 'Syspamp\\Producto', '{\"id\":5,\"description\":\"adas\",\"type\":11,\"deleted_at\":\"2017-07-17 04:10:17\"}', '[]', 'http://localhost:8000/productos/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:10:17'),
(59, 11, 'created', 6, 'Syspamp\\Producto', '[]', '{\"description\":\"45435\",\"type\":\"11\",\"id\":6}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:11:13'),
(60, 11, 'created', 7, 'Syspamp\\Producto', '[]', '{\"description\":\"435435\",\"type\":\"12\",\"id\":7}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:11:21'),
(61, 11, 'created', 8, 'Syspamp\\Producto', '[]', '{\"description\":\"sdfdsf\",\"type\":\"11\",\"id\":8}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:12:29'),
(62, 11, 'created', 9, 'Syspamp\\Producto', '[]', '{\"description\":\"435\",\"type\":\"11\",\"id\":9}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:14:34'),
(63, 11, 'updated', 8, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:24:43'),
(64, 11, 'updated', 2, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:25:10'),
(65, 11, 'updated', 7, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:25:18'),
(66, 11, 'updated', 2, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:25:44'),
(67, 11, 'updated', 2, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:25:57'),
(68, 11, 'updated', 2, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:09'),
(69, 11, 'updated', 2, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:17'),
(70, 11, 'updated', 4, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:23'),
(71, 11, 'updated', 7, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:28'),
(72, 11, 'updated', 2, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:40'),
(73, 11, 'updated', 9, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:52'),
(74, 11, 'updated', 4, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:26:55'),
(75, 11, 'updated', 8, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:27:00'),
(76, 11, 'updated', 3, 'Syspamp\\User', '{\"type\":3}', '{\"type\":\"6\"}', 'http://localhost:8000/usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:27:25'),
(77, 11, 'deleted', 61, 'Syspamp\\User', '{\"id\":61,\"name\":\"ASD\",\"lastname\":\"ASD\",\"email\":\"TEST@TEST.COM.CAR\",\"password\":\"$2y$10$sZDCP2CkAM\\/6yPH.Pj.z5eJvOP0BS3CYm0n564JjriB84TWaUjTG6\",\"type\":1,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:27:32\"}', '[]', 'http://localhost:8000/usuarios/61', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:27:32'),
(78, 11, 'deleted', 57, 'Syspamp\\User', '{\"id\":57,\"name\":\"ASDSA\",\"lastname\":\"ASD\",\"email\":\"AdD\",\"password\":\"$2y$10$UIzqr0LDAXScGJcVoSlBku6anEw4ZhPbdAfaHCJt78GA\\/ZcOQz.5u\",\"type\":1,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:27:34\"}', '[]', 'http://localhost:8000/usuarios/57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:27:34'),
(79, 11, 'updated', 45, 'Syspamp\\User', '{\"type\":1}', '{\"type\":\"5\"}', 'http://localhost:8000/usuarios/45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:27:37'),
(80, 11, 'updated', 35, 'Syspamp\\User', '{\"type\":2}', '{\"type\":\"6\"}', 'http://localhost:8000/usuarios/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:27:53'),
(81, 11, 'deleted', 35, 'Syspamp\\User', '{\"id\":35,\"name\":\"ASD\",\"lastname\":\"asD@ASDSA.COM\",\"email\":\"ASD\",\"password\":\"$2y$10$7tVM3cXIeMO5\\/WLY3\\/op8OiMfXvZ5D959RZhmTO7kGiKRFVOOY1\\/a\",\"type\":6,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:29:04\"}', '[]', 'http://localhost:8000/usuarios/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:29:04'),
(82, 11, 'deleted', 45, 'Syspamp\\User', '{\"id\":45,\"name\":\"SANTIAGO\",\"lastname\":\"PUJOL\",\"email\":\"santiag@ASDA\",\"password\":\"$2y$10$0ZnEXWzrGl2QJCw05d2Quu38KwpXruUbBEHzcObFgRshqXQetA4b2\",\"type\":5,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:29:07\"}', '[]', 'http://localhost:8000/usuarios/45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:29:07'),
(83, 11, 'deleted', 49, 'Syspamp\\User', '{\"id\":49,\"name\":\"aSD\",\"lastname\":\"ASD\",\"email\":\"ASD@ASD.COM\",\"password\":\"$2y$10$7gDihFZLgTarllOoyqkhVePvlTh7xm9H5W82HsQwRBPyQ6rnyfWcG\",\"type\":1,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:29:09\"}', '[]', 'http://localhost:8000/usuarios/49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:29:09'),
(84, 11, 'deleted', 62, 'Syspamp\\User', '{\"id\":62,\"name\":\"TESdt\",\"lastname\":\"test\",\"email\":\"teste@test.com\",\"password\":\"$2y$10$XerjPfaENq3avHr8JKs9S.ta2Gac6ioQ.00i228rTd0gagytedb8a\",\"type\":3,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:29:12\"}', '[]', 'http://localhost:8000/usuarios/62', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:29:12'),
(85, 11, 'created', 63, 'Syspamp\\User', '[]', '{\"name\":\"asdasd\",\"lastname\":\"asdsa\",\"email\":\"asdsa@sadas.co\",\"password\":\"$2y$10$OnEyz3BbzR2jLrOUXG43we0N1cZlxKakErZR.ELX5J.rPvO7z60Qq\",\"type\":\"2\",\"status\":\"on\",\"id\":63}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:32:09'),
(86, 11, 'deleted', 63, 'Syspamp\\User', '{\"id\":63,\"name\":\"asdasd\",\"lastname\":\"asdsa\",\"email\":\"asdsa@sadas.co\",\"password\":\"$2y$10$OnEyz3BbzR2jLrOUXG43we0N1cZlxKakErZR.ELX5J.rPvO7z60Qq\",\"type\":2,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-17 04:32:18\"}', '[]', 'http://localhost:8000/usuarios/63', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-17 07:32:18'),
(87, 11, 'deleted', 9, 'Syspamp\\Producto', '{\"id\":9,\"description\":\"435\",\"type\":12,\"deleted_at\":\"2017-07-22 18:30:02\"}', '[]', 'http://localhost:8000/productos/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:30:02'),
(88, 11, 'deleted', 8, 'Syspamp\\Producto', '{\"id\":8,\"description\":\"sdfdsf\",\"type\":11,\"deleted_at\":\"2017-07-22 18:30:04\"}', '[]', 'http://localhost:8000/productos/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:30:04'),
(89, 11, 'deleted', 7, 'Syspamp\\Producto', '{\"id\":7,\"description\":\"435435\",\"type\":12,\"deleted_at\":\"2017-07-22 18:30:06\"}', '[]', 'http://localhost:8000/productos/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:30:06'),
(90, 11, 'deleted', 6, 'Syspamp\\Producto', '{\"id\":6,\"description\":\"45435\",\"type\":11,\"deleted_at\":\"2017-07-22 18:30:08\"}', '[]', 'http://localhost:8000/productos/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:30:08'),
(91, 11, 'deleted', 4, 'Syspamp\\Producto', '{\"id\":4,\"description\":\"asdsa\",\"type\":12,\"deleted_at\":\"2017-07-22 18:30:12\"}', '[]', 'http://localhost:8000/productos/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:30:12'),
(92, 11, 'updated', 11, 'Syspamp\\User', '{\"remember_token\":\"y4HdWmi4CwyEu5KoP7sbp9RJlRb0M89UDCDGSd8vB0XcbIDeWn1jqdoG1u4H\"}', '{\"remember_token\":\"wwcGC9KQrqpWGyqW2jnmhzdwZzj8fIROi878gTUzFzf1ZumcepmNxUw6kRhB\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:44:06'),
(93, 64, 'updated', 64, 'Syspamp\\User', '{\"remember_token\":null}', '{\"remember_token\":\"aGziCtGP9EiITwuEnZKWRdT2tDHKcy2bb3Cq5oqop1rojGWQDkbskETshOx7\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 21:47:08'),
(94, 3, 'updated', 3, 'Syspamp\\User', '{\"remember_token\":null}', '{\"remember_token\":\"tBX1Ll1kvisMuh1x8m3U2f3JMhsBAdvLPGhzCLcxVgUfN0C0PuDRhyoa9iZu\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 22:00:21'),
(95, 5, 'updated', 5, 'Syspamp\\User', '{\"remember_token\":null}', '{\"remember_token\":\"EnbjTDa3UEmClpip8LX1olhFrePQH4wLheE2DVo92iQ8bmXxtvyj2VpHn88z\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 22:00:30'),
(96, 3, 'updated', 2, 'Syspamp\\Producto', '{\"type\":12}', '{\"type\":\"11\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-22 23:55:31'),
(97, 3, 'created', 10, 'Syspamp\\Producto', '[]', '{\"description\":\"Addsa\",\"type\":\"11\",\"id\":10}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:02:46'),
(98, 3, 'deleted', 10, 'Syspamp\\Producto', '{\"id\":10,\"description\":\"Addsa\",\"type\":11,\"deleted_at\":\"2017-07-22 21:02:52\"}', '[]', 'http://localhost:8000/productos/10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:02:52'),
(99, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"JAJA\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:03:52'),
(100, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"dsf\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:05'),
(101, 3, 'deleted', 89, 'Syspamp\\TypeUser', '{\"id\":89,\"description\":\"dsf\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/89', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:09'),
(102, 3, 'deleted', 8, 'Syspamp\\TypeUser', '{\"id\":8,\"description\":\"JAJA\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:11'),
(103, 3, 'created', 11, 'Syspamp\\Producto', '[]', '{\"description\":\"TEST\",\"type\":\"12\",\"id\":11}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:19'),
(104, 3, 'deleted', 11, 'Syspamp\\Producto', '{\"id\":11,\"description\":\"TEST\",\"type\":12,\"deleted_at\":\"2017-07-22 21:28:26\"}', '[]', 'http://localhost:8000/productos/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:26'),
(105, 3, 'created', 17, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"TEST\",\"id\":17}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:38'),
(106, 3, 'deleted', 17, 'Syspamp\\TypeProduct', '{\"id\":17,\"description\":\"TEST\",\"deleted_at\":\"2017-07-22 21:28:43\"}', '[]', 'http://localhost:8000/tipo_productos/17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:28:43'),
(107, 3, 'created', 12, 'Syspamp\\Producto', '[]', '{\"description\":\"test\",\"type\":\"12\",\"id\":12}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:33:47'),
(108, 3, 'deleted', 12, 'Syspamp\\Producto', '{\"id\":12,\"description\":\"test\",\"type\":12,\"deleted_at\":\"2017-07-22 21:33:50\"}', '[]', 'http://localhost:8000/productos/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:33:50'),
(109, 3, 'updated', 3, 'Syspamp\\User', '{\"remember_token\":\"tBX1Ll1kvisMuh1x8m3U2f3JMhsBAdvLPGhzCLcxVgUfN0C0PuDRhyoa9iZu\"}', '{\"remember_token\":\"MrR82OkdiZ21IWcNXxjvQL1u06QNvRVRNn1NNt0cdcVhOpq59QyhIm9xdys4\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:57:44'),
(110, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"asd\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:58:02'),
(111, 3, 'deleted', 9, 'Syspamp\\TypeUser', '{\"id\":9,\"description\":\"asd\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-23 00:58:06'),
(112, 3, 'created', 18, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"jua\",\"id\":18}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 03:59:02'),
(113, 3, 'deleted', 18, 'Syspamp\\TypeProduct', '{\"id\":18,\"description\":\"jua\",\"deleted_at\":\"2017-07-30 00:59:35\"}', '[]', 'http://localhost:8000/tipo_productos/18', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 03:59:35'),
(114, 3, 'updated', 12, 'Syspamp\\TypeProduct', '{\"description\":\"Polvo\"}', '{\"description\":\"Polvoa\"}', 'http://localhost:8000/tipo_productos/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 03:59:50'),
(115, 3, 'updated', 12, 'Syspamp\\TypeProduct', '{\"description\":\"Polvoa\"}', '{\"description\":\"Polvo\"}', 'http://localhost:8000/tipo_productos/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 03:59:53'),
(116, 3, 'created', 1, 'Syspamp\\Machine', '[]', '{\"description\":\"Molino 1\",\"id\":1}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 04:21:51'),
(117, 3, 'created', 2, 'Syspamp\\Machine', '[]', '{\"description\":\"Molino 2\",\"id\":2}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 04:21:57'),
(118, 3, 'deleted', 2, 'Syspamp\\Machine', '{\"id\":2,\"description\":\"Molino 2\",\"deleted_at\":\"2017-07-30 01:22:04\"}', '[]', 'http://localhost:8000/maquinas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 04:22:04'),
(119, 3, 'created', 3, 'Syspamp\\Machine', '[]', '{\"description\":\"Molino 2\",\"id\":3}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 04:22:09'),
(120, 3, 'created', 4, 'Syspamp\\Machine', '[]', '{\"description\":\"Juanm\",\"id\":4}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:03:06'),
(121, 3, 'deleted', 4, 'Syspamp\\Machine', '{\"id\":4,\"description\":\"Juanm\",\"deleted_at\":\"2017-07-30 03:03:10\"}', '[]', 'http://localhost:8000/maquinas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:03:10'),
(122, 3, 'updated', 3, 'Syspamp\\Machine', '{\"description\":\"Molino 2\"}', '{\"description\":\"Molino 233\"}', 'http://localhost:8000/maquinas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:03:13'),
(123, 3, 'updated', 3, 'Syspamp\\Machine', '{\"description\":\"Molino 233\"}', '{\"description\":\"Molino 2\"}', 'http://localhost:8000/maquinas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:03:17'),
(124, 3, 'updated', 1, 'Syspamp\\Machine', '{\"description\":\"Molino 1\"}', '{\"description\":\"Molino 12\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:03:21'),
(125, 3, 'updated', 1, 'Syspamp\\Machine', '{\"description\":\"Molino 12\"}', '{\"description\":\"Molino 1\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:03:23'),
(126, 3, 'created', 19, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asd\",\"id\":19}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:35:35'),
(127, 3, 'deleted', 19, 'Syspamp\\TypeProduct', '{\"id\":19,\"description\":\"asd\",\"deleted_at\":\"2017-07-30 03:35:38\"}', '[]', 'http://localhost:8000/tipo_productos/19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:35:38'),
(128, 3, 'updated', 11, 'Syspamp\\TypeProduct', '{\"description\":\"Piedra\"}', '{\"description\":\"Piedras\"}', 'http://localhost:8000/tipo_productos/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:36:39'),
(129, 3, 'updated', 11, 'Syspamp\\TypeProduct', '{\"description\":\"Piedras\"}', '{\"description\":\"Piedra\"}', 'http://localhost:8000/tipo_productos/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:36:43'),
(130, 3, 'created', 20, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asd\",\"id\":20}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:36:47'),
(131, 3, 'deleted', 20, 'Syspamp\\TypeProduct', '{\"id\":20,\"description\":\"asd\",\"deleted_at\":\"2017-07-30 03:36:50\"}', '[]', 'http://localhost:8000/tipo_productos/20', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:36:50'),
(132, 3, 'created', 21, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asd\",\"id\":21}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:40:53'),
(133, 3, 'deleted', 21, 'Syspamp\\TypeProduct', '{\"id\":21,\"description\":\"asd\",\"deleted_at\":\"2017-07-30 03:40:56\"}', '[]', 'http://localhost:8000/tipo_productos/21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 06:40:56'),
(134, 3, 'created', 13, 'Syspamp\\Producto', '[]', '{\"description\":\"sadsa\",\"type\":\"11\",\"id\":13}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:13:42'),
(135, 3, 'deleted', 13, 'Syspamp\\Producto', '{\"id\":13,\"description\":\"sadsa\",\"type\":11,\"deleted_at\":\"2017-07-30 04:13:45\"}', '[]', 'http://localhost:8000/productos/13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:13:45'),
(136, 3, 'created', 22, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asdsad\",\"id\":22}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:13:57'),
(137, 3, 'deleted', 22, 'Syspamp\\TypeProduct', '{\"id\":22,\"description\":\"asdsad\",\"deleted_at\":\"2017-07-30 04:14:00\"}', '[]', 'http://localhost:8000/tipo_productos/22', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:14:00'),
(138, 3, 'updated', 1, 'Syspamp\\TypeUser', '{\"description\":\"Admin\"}', '{\"description\":\"Admind\"}', 'http://localhost:8000/tipo_usuarios/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:29:09'),
(139, 3, 'updated', 1, 'Syspamp\\TypeUser', '{\"description\":\"Admind\"}', '{\"description\":\"Admin\"}', 'http://localhost:8000/tipo_usuarios/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:29:12'),
(140, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"dfg\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:31:08'),
(141, 3, 'deleted', 88, 'Syspamp\\TypeUser', '{\"id\":88,\"description\":\"dfg\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/88', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 07:31:10'),
(142, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"xcvcxvcxv\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:10:37'),
(143, 3, 'deleted', 8, 'Syspamp\\TypeUser', '{\"id\":8,\"description\":\"xcvcxvcxv\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:10:40'),
(144, 3, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracion\"}', '{\"description\":\"Administracione\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:10:53'),
(145, 3, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracione\"}', '{\"description\":\"Administracion\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:10:56'),
(146, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"asdsad\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:15:17'),
(147, 3, 'updated', 8, 'Syspamp\\TypeUser', '{\"description\":\"asdsad\"}', '{\"description\":\"asdsadjaja\"}', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:15:23'),
(148, 3, 'deleted', 8, 'Syspamp\\TypeUser', '{\"id\":8,\"description\":\"asdsadjaja\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:15:25'),
(149, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"sdfdsf\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:16:43'),
(150, 3, 'updated', 8, 'Syspamp\\TypeUser', '{\"description\":\"sdfdsf\"}', '{\"description\":\"DAAA\"}', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:16:47'),
(151, 3, 'deleted', 8, 'Syspamp\\TypeUser', '{\"id\":8,\"description\":\"DAAA\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:16:49'),
(152, 3, 'created', 23, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"dsfdsfds\",\"id\":23}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:18:54'),
(153, 3, 'updated', 23, 'Syspamp\\TypeProduct', '{\"description\":\"dsfdsfds\"}', '{\"description\":\"JAJAJA\"}', 'http://localhost:8000/tipo_productos/23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:18:59'),
(154, 3, 'deleted', 23, 'Syspamp\\TypeProduct', '{\"id\":23,\"description\":\"JAJAJA\",\"deleted_at\":\"2017-07-30 20:19:01\"}', '[]', 'http://localhost:8000/tipo_productos/23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:19:01'),
(155, 3, 'updated', 1, 'Syspamp\\Machine', '{\"description\":\"Molino 1\"}', '{\"description\":\"Molino 122\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:20:19'),
(156, 3, 'updated', 1, 'Syspamp\\Machine', '{\"description\":\"Molino 122\"}', '{\"description\":\"Molino 1\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:20:23'),
(157, 3, 'created', 5, 'Syspamp\\Machine', '[]', '{\"description\":\"JAJAj\",\"id\":5}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:20:27'),
(158, 3, 'updated', 5, 'Syspamp\\Machine', '{\"description\":\"JAJAj\"}', '{\"description\":\"JAJAjnnnan\"}', 'http://localhost:8000/maquinas/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:20:42'),
(159, 3, 'deleted', 5, 'Syspamp\\Machine', '{\"id\":5,\"description\":\"JAJAjnnnan\",\"deleted_at\":\"2017-07-30 20:20:44\"}', '[]', 'http://localhost:8000/maquinas/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:20:44'),
(160, 3, 'created', 6, 'Syspamp\\Machine', '[]', '{\"description\":\"see\",\"id\":6}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:21:33'),
(161, 3, 'updated', 6, 'Syspamp\\Machine', '{\"description\":\"see\"}', '{\"description\":\"seedaa\"}', 'http://localhost:8000/maquinas/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:21:36'),
(162, 3, 'deleted', 6, 'Syspamp\\Machine', '{\"id\":6,\"description\":\"seedaa\",\"deleted_at\":\"2017-07-30 20:21:38\"}', '[]', 'http://localhost:8000/maquinas/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:21:38');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(163, 3, 'created', 14, 'Syspamp\\Producto', '[]', '{\"description\":\"JAja\",\"type\":\"12\",\"id\":14}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:27:51'),
(164, 3, 'deleted', 14, 'Syspamp\\Producto', '{\"id\":14,\"description\":\"JAja\",\"type\":12,\"deleted_at\":\"2017-07-30 20:27:54\"}', '[]', 'http://localhost:8000/productos/14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:27:54'),
(165, 3, 'updated', 2, 'Syspamp\\Producto', '{\"description\":\"Moco\"}', '{\"description\":\"MocoA\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:27:57'),
(166, 3, 'updated', 2, 'Syspamp\\Producto', '{\"description\":\"MocoA\"}', '{\"description\":\"Moco\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:28:01'),
(167, 3, 'created', 15, 'Syspamp\\Producto', '[]', '{\"description\":\"JAaa\",\"type\":\"12\",\"id\":15}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:55:46'),
(168, 3, 'updated', 2, 'Syspamp\\Producto', '{\"type\":11}', '{\"type\":\"12\"}', 'http://localhost:8000/productos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-30 23:56:01'),
(169, 3, 'created', 16, 'Syspamp\\Producto', '[]', '{\"description\":\"JAJA\",\"type\":\"11\",\"id\":16}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:05'),
(170, 3, 'updated', 16, 'Syspamp\\Producto', '{\"description\":\"JAJA\",\"type\":11}', '{\"description\":\"JAJAsee\",\"type\":\"12\"}', 'http://localhost:8000/productos/16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:15'),
(171, 3, 'deleted', 16, 'Syspamp\\Producto', '{\"id\":16,\"description\":\"JAJAsee\",\"type\":12,\"deleted_at\":\"2017-07-30 21:29:18\"}', '[]', 'http://localhost:8000/productos/16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:18'),
(172, 3, 'deleted', 15, 'Syspamp\\Producto', '{\"id\":15,\"description\":\"JAaa\",\"type\":12,\"deleted_at\":\"2017-07-30 21:29:20\"}', '[]', 'http://localhost:8000/productos/15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:20'),
(173, 3, 'updated', 1, 'Syspamp\\Machine', '{\"description\":\"Molino 1\"}', '{\"description\":\"Molino 133\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:28'),
(174, 3, 'updated', 1, 'Syspamp\\Machine', '{\"description\":\"Molino 133\"}', '{\"description\":\"Molino 1\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:31'),
(175, 3, 'created', 7, 'Syspamp\\Machine', '[]', '{\"description\":\"sees\",\"id\":7}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:33'),
(176, 3, 'deleted', 7, 'Syspamp\\Machine', '{\"id\":7,\"description\":\"sees\",\"deleted_at\":\"2017-07-30 21:29:35\"}', '[]', 'http://localhost:8000/maquinas/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:35'),
(177, 3, 'created', 24, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"Daaa\",\"id\":24}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:46'),
(178, 3, 'updated', 24, 'Syspamp\\TypeProduct', '{\"description\":\"Daaa\"}', '{\"description\":\"Daaae\"}', 'http://localhost:8000/tipo_productos/24', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:50'),
(179, 3, 'deleted', 24, 'Syspamp\\TypeProduct', '{\"id\":24,\"description\":\"Daaae\",\"deleted_at\":\"2017-07-30 21:29:52\"}', '[]', 'http://localhost:8000/tipo_productos/24', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:52'),
(180, 3, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracion\"}', '{\"description\":\"Administracione\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:29:59'),
(181, 3, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracione\"}', '{\"description\":\"Administracion\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:30:01'),
(182, 3, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"Dfsd\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:30:05'),
(183, 3, 'deleted', 9, 'Syspamp\\TypeUser', '{\"id\":9,\"description\":\"Dfsd\",\"deleted_at\":null}', '[]', 'http://localhost:8000/tipo_usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:30:09'),
(184, 3, 'updated', 3, 'Syspamp\\User', '{\"lastname\":\"Admin\"}', '{\"lastname\":\"Admine\"}', 'http://localhost:8000/usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:54:29'),
(185, 3, 'updated', 3, 'Syspamp\\User', '{\"lastname\":\"Admine\"}', '{\"lastname\":\"Admin\"}', 'http://localhost:8000/usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 00:54:32'),
(186, 3, 'created', 9, 'Syspamp\\User', '[]', '{\"name\":\"sadsad\",\"lastname\":\"dsfdsf\",\"email\":\"dsfdsf@asdsad.com\",\"password\":\"$2y$10$nt\\/rzBlGOuVRGhUQDiw28Ofho.K69d\\/o4\\/wYgg18BMKMeDm6GIHp.\",\"type\":\"2\",\"status\":\"on\",\"id\":9}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 01:01:13'),
(187, 3, 'updated', 9, 'Syspamp\\User', '{\"type\":2}', '{\"type\":\"5\"}', 'http://localhost:8000/usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 01:01:19'),
(188, 3, 'updated', 9, 'Syspamp\\User', '{\"type\":5}', '{\"type\":\"6\"}', 'http://localhost:8000/usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 01:01:22'),
(189, 3, 'deleted', 9, 'Syspamp\\User', '{\"id\":9,\"name\":\"sadsad\",\"lastname\":\"dsfdsf\",\"email\":\"dsfdsf@asdsad.com\",\"password\":\"$2y$10$nt\\/rzBlGOuVRGhUQDiw28Ofho.K69d\\/o4\\/wYgg18BMKMeDm6GIHp.\",\"type\":6,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-07-30 22:01:24\"}', '[]', 'http://localhost:8000/usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 01:01:24'),
(190, 3, 'updated', 5, 'Syspamp\\User', '{\"email\":\"produccion\"}', '{\"email\":\"produccione\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 01:12:01'),
(191, 3, 'updated', 5, 'Syspamp\\User', '{\"email\":\"produccione\"}', '{\"email\":\"produccion\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-07-31 01:12:06'),
(192, 3, 'created', 1, 'Syspamp\\CondicionIva', '[]', '{\"description\":\"Monotributo\",\"id\":1}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:13:04'),
(193, 3, 'created', 2, 'Syspamp\\CondicionIva', '[]', '{\"description\":\"Responsable Inscripto\",\"id\":2}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:13:15'),
(194, 3, 'updated', 1, 'Syspamp\\CondicionIva', '{\"description\":\"Monotributo\"}', '{\"description\":\"Monotributose\"}', 'http://localhost:8000/condiciones_ivas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:13:18'),
(195, 3, 'updated', 1, 'Syspamp\\CondicionIva', '{\"description\":\"Monotributose\"}', '{\"description\":\"Monotributo\"}', 'http://localhost:8000/condiciones_ivas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:13:21'),
(196, 3, 'created', 1, 'Syspamp\\PaymentMethod', '[]', '{\"description\":\"Tarjeta\",\"id\":1}', 'http://localhost:8000/formas_de_pago', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:26:43'),
(197, 3, 'created', 2, 'Syspamp\\PaymentMethod', '[]', '{\"description\":\"Efectivo\",\"id\":2}', 'http://localhost:8000/formas_de_pago', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:26:47'),
(198, 3, 'created', 3, 'Syspamp\\PaymentMethod', '[]', '{\"description\":\"Debito\",\"id\":3}', 'http://localhost:8000/formas_de_pago', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:26:53'),
(199, 3, 'created', 4, 'Syspamp\\PaymentMethod', '[]', '{\"description\":\"Cheque\",\"id\":4}', 'http://localhost:8000/formas_de_pago', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-02 06:26:57'),
(200, 3, 'created', 1, 'Syspamp\\TypePerson', '[]', '{\"description\":\"CLIENTE\",\"id\":1}', 'http://localhost:8000/tipo_personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-03 06:53:57'),
(201, 3, 'created', 2, 'Syspamp\\TypePerson', '[]', '{\"description\":\"PROVEEDOR\",\"id\":2}', 'http://localhost:8000/tipo_personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-03 06:54:43'),
(202, 3, 'created', 3, 'Syspamp\\TypePerson', '[]', '{\"description\":\"EMPLEADO\",\"id\":3}', 'http://localhost:8000/tipo_personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-03 06:55:05'),
(203, 3, 'created', 1, 'Syspamp\\Person', '[]', '{\"name\":\"Sanmtiago\",\"lastname\":\"piujola\",\"cuit\":\"as324234\",\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"adress\":\"adfasd\",\"adress_number\":\"asdasd\",\"floor\":\"asdsa\",\"department\":\"qadsda\",\"phone_1\":\"435645\",\"phone_2\":\"546\",\"phone_3\":\"456456\",\"id\":1}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-03 08:07:50'),
(204, 3, 'updated', 3, 'Syspamp\\User', '{\"remember_token\":\"MrR82OkdiZ21IWcNXxjvQL1u06QNvRVRNn1NNt0cdcVhOpq59QyhIm9xdys4\"}', '{\"remember_token\":\"zWf7Ni7Qnv7q1xe7EulX1ElfF4irbaLJeAdOwHIDwgnd0weE4OW1A6VnmMV7\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 08:35:55'),
(205, 3, 'created', 10, 'Syspamp\\User', '[]', '{\"password\":\"$2y$10$gf5lHooq2nEqMS8pg3KnC.mzUvRsGc54rm7LQGzeVmU3cYWrayzfm\",\"type\":\"1\",\"status\":\"off\",\"id\":10}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 08:49:23'),
(206, 3, 'updated', 3, 'Syspamp\\User', '{\"remember_token\":null}', '{\"remember_token\":\"Z7oWcHbg0BTGkcMt6G53Y15X1niwM92umSCUxMLXPsvh94zBLSPwcvagmd4J\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 09:27:07'),
(207, 1, 'created', 4, 'Syspamp\\Person', '[]', '{\"name\":\"TEST2JAJA\",\"lastname\":\"TEST\",\"email\":\"TEST@TEST324324.COM\",\"cuit\":\"TESTT\",\"adress\":\"EST\",\"floor\":\"324\",\"department\":\"32423\",\"phone_1\":\"324\",\"phone_2\":\"3242\",\"phone_3\":\"234\",\"id_condicion_iva\":\"2\",\"id_type_people\":\"2\",\"id\":4}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 10:19:00'),
(208, 1, 'updated', 4, 'Syspamp\\Person', '{\"floor\":\"324\",\"phone_1\":\"324\",\"phone_2\":\"3242\",\"phone_3\":\"234\"}', '{\"floor\":\"54545\",\"phone_1\":\"32423\",\"phone_2\":\"32423\",\"phone_3\":\"32423\"}', 'http://localhost:8000/personas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 19:20:37'),
(209, 1, 'created', 7, 'Syspamp\\Person', '[]', '{\"name\":\"DA\",\"lastname\":\"KE\",\"email\":\"santiagopujol92@gmail.co\",\"cuit\":\"546546\",\"adress\":\"Chascomus 1782\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"2\",\"id_type_people\":\"1\",\"id\":7}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 19:21:09'),
(210, 1, 'updated', 7, 'Syspamp\\Person', '{\"id_condicion_iva\":2}', '{\"id_condicion_iva\":\"1\"}', 'http://localhost:8000/personas/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:50:01'),
(211, 1, 'updated', 3, 'Syspamp\\Person', '{\"id_type_people\":2,\"floor\":\"\",\"department\":\"\",\"phone_1\":\"\",\"phone_2\":\"\",\"phone_3\":\"\"}', '{\"id_type_people\":\"1\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null}', 'http://localhost:8000/personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:50:09'),
(212, 1, 'updated', 2, 'Syspamp\\Person', '{\"floor\":\"\",\"department\":\"\",\"phone_1\":\"\",\"phone_2\":\"\",\"phone_3\":\"\"}', '{\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null}', 'http://localhost:8000/personas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:52:56'),
(213, 1, 'updated', 7, 'Syspamp\\Person', '{\"id_condicion_iva\":1}', '{\"id_condicion_iva\":\"2\"}', 'http://localhost:8000/personas/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:53:04'),
(214, 1, 'updated', 3, 'Syspamp\\Person', '{\"id_condicion_iva\":2}', '{\"id_condicion_iva\":\"1\"}', 'http://localhost:8000/personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:53:07'),
(215, 1, 'updated', 3, 'Syspamp\\Person', '{\"id_type_people\":1}', '{\"id_type_people\":\"2\"}', 'http://localhost:8000/personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:54:00'),
(216, 1, 'updated', 4, 'Syspamp\\Person', '{\"id_condicion_iva\":2}', '{\"id_condicion_iva\":\"1\"}', 'http://localhost:8000/personas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:54:05'),
(217, 1, 'updated', 2, 'Syspamp\\Person', '{\"id_condicion_iva\":1}', '{\"id_condicion_iva\":\"2\"}', 'http://localhost:8000/personas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 21:54:12'),
(218, 1, 'updated', 3, 'Syspamp\\Person', '{\"id_condicion_iva\":1}', '{\"id_condicion_iva\":\"2\"}', 'http://localhost:8000/personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 22:53:47'),
(219, 1, 'updated', 7, 'Syspamp\\Person', '{\"id_type_people\":1}', '{\"id_type_people\":\"3\"}', 'http://localhost:8000/personas/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 22:53:51'),
(220, 1, 'updated', 2, 'Syspamp\\Person', '{\"id_condicion_iva\":2}', '{\"id_condicion_iva\":\"1\"}', 'http://localhost:8000/personas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 23:02:15'),
(221, 1, 'created', 17, 'Syspamp\\Producto', '[]', '{\"description\":\"Administracione\",\"type\":\"11\",\"id\":17}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 23:25:27'),
(222, 1, 'deleted', 17, 'Syspamp\\Producto', '{\"id\":17,\"description\":\"Administracione\",\"type\":11,\"deleted_at\":\"2017-08-05 20:25:38\"}', '[]', 'http://localhost:8000/productos/17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-05 23:25:38'),
(223, 1, 'created', 9, 'Syspamp\\Person', '[]', '{\"name\":\"TEST\",\"lastname\":\"TET\",\"email\":\"SAS@ASDSAD.COM\",\"cuit\":\"234234\",\"adress\":\"SDASDASD\",\"floor\":\"SAD\",\"department\":\"324\",\"phone_1\":\"234\",\"phone_2\":\"234\",\"phone_3\":\"234\",\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id\":9}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 03:57:42'),
(224, 1, 'created', 10, 'Syspamp\\Person', '[]', '{\"name\":\"ASDASD\",\"lastname\":\"SADASDSAD\",\"email\":\"SADSA@ASDSA.COM\",\"cuit\":\"4353455\",\"adress\":\"34534\",\"floor\":\"DSAFDS\",\"department\":\"345\",\"phone_1\":\"3453\",\"phone_2\":\"34545\",\"phone_3\":\"345\",\"id_condicion_iva\":\"2\",\"id_type_people\":\"3\",\"id\":10}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 03:58:49'),
(225, 1, 'updated', 10, 'Syspamp\\Person', '{\"phone_1\":\"3453\",\"phone_2\":\"34545\"}', '{\"phone_1\":\"345\",\"phone_2\":\"345\"}', 'http://localhost:8000/personas/10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 05:21:04'),
(226, 1, 'updated', 9, 'Syspamp\\Person', '{\"phone_1\":\"234\",\"phone_2\":\"234\",\"phone_3\":\"234\"}', '{\"phone_1\":\"324\",\"phone_2\":\"324\",\"phone_3\":\"324\"}', 'http://localhost:8000/personas/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 05:22:22'),
(227, 1, 'created', 16, 'Syspamp\\Person', '[]', '{\"name\":\"sdfsdf\",\"lastname\":\"dsfds\",\"email\":\"dsfdsfsdfsd435@asdfasd\",\"cuit\":\"dfdsf35\",\"adress\":\"dsfdsf\",\"floor\":\"45435\",\"department\":\"435435\",\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id\":16}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 05:55:26'),
(228, 1, 'created', 19, 'Syspamp\\Person', '[]', '{\"name\":\"dsfdsf\",\"lastname\":\"dsfds\",\"email\":\"dsfdsdsaf@asdsad\",\"cuit\":\"435435\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id\":19}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 05:58:49'),
(229, 1, 'created', 31, 'Syspamp\\Person', '[]', '{\"name\":\"ASDasd\",\"lastname\":\"asdasd\",\"email\":\"sadsa@asdsad\",\"cuit\":\"3245234\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id\":31}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:17:22'),
(230, 1, 'created', 32, 'Syspamp\\Person', '[]', '{\"name\":\"asdsad\",\"lastname\":\"asdsa\",\"email\":\"asdas@asdsad\",\"cuit\":\"w4w5435\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id\":32}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:27:16'),
(231, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"test\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:32:20'),
(232, 1, 'updated', 8, 'Syspamp\\TypeUser', '{\"description\":\"test\"}', '{\"description\":\"tester\"}', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:32:24'),
(233, 1, 'deleted', 8, 'Syspamp\\TypeUser', '{\"id\":8,\"description\":\"tester\",\"deleted_at\":\"2017-08-06 03:32:27\"}', '[]', 'http://localhost:8000/tipo_usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:32:27'),
(234, 1, 'created', 35, 'Syspamp\\Person', '[]', '{\"name\":\"asdasd\",\"lastname\":\"sadsa\",\"email\":\"as@xn--asdsa-pja\",\"cuit\":\"235345\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id\":35}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:43:13'),
(235, 1, 'created', 39, 'Syspamp\\Person', '[]', '{\"name\":\"asdasd\",\"lastname\":\"asdsad\",\"email\":\"SAASA@ASDSAm\",\"cuit\":\"353454\",\"adress\":\"adasd\",\"floor\":\"34534\",\"department\":\"5345435\",\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id\":39}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:44:29'),
(236, 1, 'created', 40, 'Syspamp\\Person', '[]', '{\"name\":\"asdsa\",\"lastname\":\"asdsa\",\"email\":\"asda@asdas\",\"cuit\":\"345345\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id\":40}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 06:52:47'),
(237, 1, 'updated', 31, 'Syspamp\\Person', '{\"id_condicion_iva\":1,\"id_type_people\":2}', '{\"id_condicion_iva\":\"2\",\"id_type_people\":\"3\"}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 07:30:56'),
(238, 1, 'deleted', 10, 'Syspamp\\Person', '{\"id\":10,\"name\":\"ASDASD\",\"lastname\":\"SADASDSAD\",\"email\":\"SADSA@ASDSA.COM\",\"cuit\":\"4353455\",\"id_condicion_iva\":2,\"id_type_people\":3,\"adress\":\"34534\",\"floor\":\"DSAFDS\",\"department\":\"345\",\"phone_1\":\"345\",\"phone_2\":\"345\",\"phone_3\":\"345\",\"deleted_at\":\"2017-08-06 16:01:59\"}', '[]', 'http://localhost:8000/personas/10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:01:59'),
(239, 1, 'deleted', 3, 'Syspamp\\Person', '{\"id\":3,\"name\":\"ADRIANA\",\"lastname\":\"RUBIO\",\"email\":\"SADASDAS\",\"cuit\":\"3453453\",\"id_condicion_iva\":2,\"id_type_people\":2,\"adress\":\"DSFDSF\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-08-06 16:02:02\"}', '[]', 'http://localhost:8000/personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:02:02'),
(240, 1, 'deleted', 4, 'Syspamp\\Person', '{\"id\":4,\"name\":\"TEST2JAJA\",\"lastname\":\"TEST\",\"email\":\"TEST@TEST324324.COM\",\"cuit\":\"TESTT\",\"id_condicion_iva\":1,\"id_type_people\":2,\"adress\":\"EST\",\"floor\":\"54545\",\"department\":\"32423\",\"phone_1\":\"32423\",\"phone_2\":\"32423\",\"phone_3\":\"32423\",\"deleted_at\":\"2017-08-06 16:02:07\"}', '[]', 'http://localhost:8000/personas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:02:07'),
(241, 1, 'deleted', 2, 'Syspamp\\Person', '{\"id\":2,\"name\":\"SANTIAGO\",\"lastname\":\"PUJOL\",\"email\":\"ASD\",\"cuit\":\"23423423\",\"id_condicion_iva\":1,\"id_type_people\":3,\"adress\":\"SFDFDSFDS\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-08-06 16:02:12\"}', '[]', 'http://localhost:8000/personas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:02:12'),
(242, 1, 'deleted', 9, 'Syspamp\\Person', '{\"id\":9,\"name\":\"TEST\",\"lastname\":\"TET\",\"email\":\"SAS@ASDSAD.COM\",\"cuit\":\"234234\",\"id_condicion_iva\":1,\"id_type_people\":2,\"adress\":\"SDASDASD\",\"floor\":\"SAD\",\"department\":\"324\",\"phone_1\":\"324\",\"phone_2\":\"324\",\"phone_3\":\"324\",\"deleted_at\":\"2017-08-06 16:02:15\"}', '[]', 'http://localhost:8000/personas/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:02:15'),
(243, 1, 'deleted', 40, 'Syspamp\\Person', '{\"id\":40,\"name\":\"asdsa\",\"lastname\":\"asdsa\",\"email\":\"asda@asdas\",\"cuit\":\"345345\",\"id_condicion_iva\":1,\"id_type_people\":2,\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-08-06 16:02:17\"}', '[]', 'http://localhost:8000/personas/40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:02:17'),
(244, 1, 'deleted', 39, 'Syspamp\\Person', '{\"id\":39,\"name\":\"asdasd\",\"lastname\":\"asdsad\",\"email\":\"SAASA@ASDSAm\",\"cuit\":\"353454\",\"id_condicion_iva\":1,\"id_type_people\":1,\"adress\":\"adasd\",\"floor\":\"34534\",\"department\":\"5345435\",\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-08-06 16:02:19\"}', '[]', 'http://localhost:8000/personas/39', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:02:19'),
(245, 1, 'created', 8, 'Syspamp\\Machine', '[]', '{\"description\":\"JAJA\",\"id\":8}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:16:39'),
(246, 1, 'deleted', 8, 'Syspamp\\Machine', '{\"id\":8,\"description\":\"JAJA\",\"deleted_at\":\"2017-08-06 16:16:46\"}', '[]', 'http://localhost:8000/maquinas/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:16:46'),
(247, 1, 'created', 9, 'Syspamp\\Machine', '[]', '{\"description\":\"TEST\",\"id\":9}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:17:40'),
(248, 1, 'deleted', 9, 'Syspamp\\Machine', '{\"id\":9,\"description\":\"TEST\",\"deleted_at\":\"2017-08-06 16:17:45\"}', '[]', 'http://localhost:8000/maquinas/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:17:45'),
(249, 1, 'created', 26, 'Syspamp\\Producto', '[]', '{\"description\":\"dsfdsf\",\"type\":\"12\",\"id\":26}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:20:37'),
(250, 1, 'deleted', 26, 'Syspamp\\Producto', '{\"id\":26,\"description\":\"dsfdsf\",\"type\":12,\"deleted_at\":\"2017-08-06 16:20:44\"}', '[]', 'http://localhost:8000/productos/26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:20:44'),
(251, 1, 'created', 27, 'Syspamp\\Producto', '[]', '{\"description\":\"Asdas\",\"type\":\"11\",\"id\":27}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:40:26'),
(252, 1, 'deleted', 27, 'Syspamp\\Producto', '{\"id\":27,\"description\":\"Asdas\",\"type\":11,\"deleted_at\":\"2017-08-06 16:40:29\"}', '[]', 'http://localhost:8000/productos/27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 19:40:29'),
(253, 1, 'created', 7, 'Syspamp\\User', '[]', '{\"name\":\"TESTTE\",\"lastname\":\"TEST\",\"email\":\"TEST@asd.com\",\"password\":\"$2y$10$ip7.Mjmu9alexOWA8H1RAOlYEmTHgHniApswDlUdc6eJAD7EEA\\/oq\",\"type\":\"1\",\"status\":\"on\",\"id\":7}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:37:46'),
(254, 1, 'deleted', 7, 'Syspamp\\User', '{\"id\":7,\"name\":\"TESTTE\",\"lastname\":\"TEST\",\"email\":\"TEST@asd.com\",\"password\":\"$2y$10$ip7.Mjmu9alexOWA8H1RAOlYEmTHgHniApswDlUdc6eJAD7EEA\\/oq\",\"type\":1,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 19:37:50\"}', '[]', 'http://localhost:8000/usuarios/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:37:50'),
(255, 1, 'created', 8, 'Syspamp\\User', '[]', '{\"name\":\"TEST2\",\"lastname\":\"TEST23\",\"email\":\"TEST45@ASDSAD.COM\",\"password\":\"$2y$10$QDzXN29eGA926YmA4m6uI.J0GUAbYFl\\/bXzR2s07h5mBTHaulSM6q\",\"type\":\"2\",\"status\":\"on\",\"id\":8}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:53:07'),
(256, 1, 'updated', 2, 'Syspamp\\User', '{\"type\":2}', '{\"type\":\"6\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:54:26'),
(257, 1, 'updated', 6, 'Syspamp\\User', '{\"type\":6}', '{\"type\":\"3\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:54:30'),
(258, 1, 'updated', 6, 'Syspamp\\User', '{\"type\":3}', '{\"type\":\"6\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:54:35'),
(259, 1, 'updated', 2, 'Syspamp\\User', '{\"type\":6}', '{\"type\":\"2\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:54:41'),
(260, 1, 'deleted', 8, 'Syspamp\\User', '{\"id\":8,\"name\":\"TEST2\",\"lastname\":\"TEST23\",\"email\":\"TEST45@ASDSAD.COM\",\"password\":\"$2y$10$QDzXN29eGA926YmA4m6uI.J0GUAbYFl\\/bXzR2s07h5mBTHaulSM6q\",\"type\":2,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 19:54:44\"}', '[]', 'http://localhost:8000/usuarios/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:54:44'),
(261, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"TEST\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:56:21'),
(262, 1, 'updated', 123, 'Syspamp\\TypeUser', '{\"description\":\"TEST\"}', '{\"description\":\"TESTasdsa\"}', 'http://localhost:8000/tipo_usuarios/123', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:56:24'),
(263, 1, 'deleted', 123, 'Syspamp\\TypeUser', '{\"id\":123,\"description\":\"TESTasdsa\",\"deleted_at\":\"2017-08-06 19:56:26\"}', '[]', 'http://localhost:8000/tipo_usuarios/123', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:56:26'),
(264, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"TEST6\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 22:59:14'),
(265, 1, 'updated', 34, 'Syspamp\\TypeUser', '{\"description\":\"TEST6\"}', '{\"description\":\"asdsaTEST6\"}', 'http://localhost:8000/tipo_usuarios/34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:00:10'),
(266, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"teST\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:00:15'),
(267, 1, 'deleted', 99, 'Syspamp\\TypeUser', '{\"id\":99,\"description\":\"teST\",\"deleted_at\":\"2017-08-06 20:00:18\"}', '[]', 'http://localhost:8000/tipo_usuarios/99', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:00:18'),
(268, 1, 'deleted', 34, 'Syspamp\\TypeUser', '{\"id\":34,\"description\":\"asdsaTEST6\",\"deleted_at\":\"2017-08-06 20:00:22\"}', '[]', 'http://localhost:8000/tipo_usuarios/34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:00:22'),
(269, 1, 'created', 9, 'Syspamp\\User', '[]', '{\"name\":\"TEST23\",\"lastname\":\"TEST4\",\"email\":\"444@ASDSAD.COM\",\"password\":\"$2y$10$M.0gL7skT.\\/wnnQDFPfP..sO1xUouWFvqWEIBiXJOEInvyx7D5S36\",\"type\":\"5\",\"status\":\"off\",\"id\":9}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:02:29'),
(270, 1, 'updated', 9, 'Syspamp\\User', '{\"status\":\"off\"}', '{\"status\":\"on\"}', 'http://localhost:8000/usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:02:36'),
(271, 1, 'deleted', 9, 'Syspamp\\User', '{\"id\":9,\"name\":\"TEST23\",\"lastname\":\"TEST4\",\"email\":\"444@ASDSAD.COM\",\"password\":\"$2y$10$M.0gL7skT.\\/wnnQDFPfP..sO1xUouWFvqWEIBiXJOEInvyx7D5S36\",\"type\":5,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 20:02:40\"}', '[]', 'http://localhost:8000/usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:02:40'),
(272, 1, 'created', 10, 'Syspamp\\User', '[]', '{\"name\":\"ASDSAd\",\"lastname\":\"asdsad\",\"email\":\"asdsa@asdsad.com\",\"password\":\"$2y$10$Q34sP728V.O53y0cCEf2OulYH37OFNKlKd1\\/5HHIKL7HLbCqL5hG6\",\"type\":\"5\",\"status\":\"on\",\"id\":10}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:03:36'),
(273, 1, 'created', 11, 'Syspamp\\User', '[]', '{\"name\":\"ASD\",\"lastname\":\"ASDSA\",\"email\":\"ASDA2ASDSAD@ASDSAD.COM\",\"password\":\"$2y$10$bFFdoMmW57fwtj9m1vrHUOB5sq.uKYg\\/VgQc65Dn1DXZs2oFylN6C\",\"type\":\"3\",\"status\":\"on\",\"id\":11}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:37:57'),
(274, 1, 'updated', 11, 'Syspamp\\User', '{\"type\":3}', '{\"type\":\"2\"}', 'http://localhost:8000/usuarios/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:38:02'),
(275, 1, 'deleted', 11, 'Syspamp\\User', '{\"id\":11,\"name\":\"ASD\",\"lastname\":\"ASDSA\",\"email\":\"ASDA2ASDSAD@ASDSAD.COM\",\"password\":\"$2y$10$bFFdoMmW57fwtj9m1vrHUOB5sq.uKYg\\/VgQc65Dn1DXZs2oFylN6C\",\"type\":2,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 20:38:07\"}', '[]', 'http://localhost:8000/usuarios/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:38:07'),
(276, 1, 'deleted', 10, 'Syspamp\\User', '{\"id\":10,\"name\":\"ASDSAd\",\"lastname\":\"asdsad\",\"email\":\"asdsa@asdsad.com\",\"password\":\"$2y$10$Q34sP728V.O53y0cCEf2OulYH37OFNKlKd1\\/5HHIKL7HLbCqL5hG6\",\"type\":5,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 20:38:10\"}', '[]', 'http://localhost:8000/usuarios/10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-06 23:38:10'),
(277, 1, 'updated', 2, 'Syspamp\\User', '{\"password\":\"$2y$10$dANSqVSJPdu.T922sW2DvOmRKpMxEv4HjkRRT86HbherJ9Mz46xpy\"}', '{\"password\":\"$2y$10$UUIsdsWh5JBMdCL6Du95F.vrvqn6hRb8tMZ3\\/HdZTuOQsO0MLaPEK\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:24:05'),
(278, 1, 'updated', 2, 'Syspamp\\User', '{\"password\":\"$2y$10$UUIsdsWh5JBMdCL6Du95F.vrvqn6hRb8tMZ3\\/HdZTuOQsO0MLaPEK\"}', '{\"password\":\"$2y$10$cwZA8FHXI17VTtWRWFGcWuBURfbTTRPm1Vt.GYX33aPqzATAzd28q\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:25:23'),
(279, 1, 'updated', 6, 'Syspamp\\User', '{\"password\":\"$2y$10$MI5qeRjIOrplQxj1OFtxAOyU\\/TQzH4SriBxCL1grR3Q3SnnefiFN6\"}', '{\"password\":\"$2y$10$0CPqMRmq4rVX5ZPcPccbzO2Em8Ne.N5bjnfbQ5F.LwWQ.R7E3Qud2\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:25:47'),
(280, 1, 'updated', 6, 'Syspamp\\User', '{\"password\":\"$2y$10$0CPqMRmq4rVX5ZPcPccbzO2Em8Ne.N5bjnfbQ5F.LwWQ.R7E3Qud2\"}', '{\"password\":\"$2y$10$GUe88kr1FrtG0BGh\\/3YIY.nRU54KRdBk.BgTJ\\/GJ9tl\\/OrVxQzE1u\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:26:06'),
(281, 1, 'updated', 6, 'Syspamp\\User', '{\"password\":\"$2y$10$GUe88kr1FrtG0BGh\\/3YIY.nRU54KRdBk.BgTJ\\/GJ9tl\\/OrVxQzE1u\"}', '{\"password\":\"$2y$10$CJgSubbXaxmomENXKw2OuuXYbt3lc.zs2ohrIYUZyikUw6LiHGy\\/G\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:36:14'),
(282, 1, 'updated', 4, 'Syspamp\\User', '{\"password\":\"$2y$10$g33iZ0INnI36UDGGocOCIeZfZoevkmLFLeZ0B\\/zpK143uWTFH1Dcq\"}', '{\"password\":\"$2y$10$JhiXfzc9tW17u3rGAarAn.2d7cvVO3BRt8SMmDtQRVORF4WtBc.qG\"}', 'http://localhost:8000/usuarios/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:36:20'),
(283, 1, 'created', 12, 'Syspamp\\User', '[]', '{\"name\":\"TEST\",\"lastname\":\"TEST\",\"email\":\"TEST@TEST.COM\",\"password\":\"$2y$10$.1uyxAbTC\\/\\/c6AyPhMZkCuDAdKHWx.04kZAdFQPYcl\\/Q\\/FyJria5i\",\"type\":\"2\",\"status\":\"on\",\"id\":12}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:38:01'),
(284, 1, 'updated', 12, 'Syspamp\\User', '{\"name\":\"TEST\",\"lastname\":\"TEST\",\"email\":\"TEST@TEST.COM\"}', '{\"name\":\"TEST3434\",\"lastname\":\"TEST34\",\"email\":\"TEST@TEST.COM3434\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:38:10'),
(285, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$.1uyxAbTC\\/\\/c6AyPhMZkCuDAdKHWx.04kZAdFQPYcl\\/Q\\/FyJria5i\"}', '{\"password\":\"$2y$10$pNFL4373Kp8jeSikuY4pfe3RprV1w7taVVhehSBM8rjd1c7SsVl5S\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:38:35'),
(286, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$pNFL4373Kp8jeSikuY4pfe3RprV1w7taVVhehSBM8rjd1c7SsVl5S\"}', '{\"password\":\"$2y$10$Q9pr6wm5v9Xwj4OA9AD.aeIMmEUILjIeqh57mcYoVNgc5TbVFkNri\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:40:49'),
(287, 1, 'updated', 12, 'Syspamp\\User', '{\"name\":\"TEST3434\",\"email\":\"TEST@TEST.COM3434\",\"password\":\"$2y$10$Q9pr6wm5v9Xwj4OA9AD.aeIMmEUILjIeqh57mcYoVNgc5TbVFkNri\"}', '{\"name\":\"TEST\",\"email\":\"TEST@TEST.COM\",\"password\":\"$2y$10$I92l2zatbbCFBQRoXuroMOQ7pxkxl1rKSmllxqhkd9a0HEKy50ncy\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:48:36'),
(288, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":null}', '{\"remember_token\":\"ClKqI66tljYe2Hhf6pMZmjOQl7u7kesxhoxWMFPxNSWQA2h8FOql9VWYMJb7\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:48:46'),
(289, 12, 'updated', 12, 'Syspamp\\User', '{\"remember_token\":null}', '{\"remember_token\":\"tOJqy7yUWvRbJQmuDrb3Pn2gvEtwnwnGHulqoEf9oHIMEQc1iNjGcL5MDsDU\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:49:03'),
(290, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$I92l2zatbbCFBQRoXuroMOQ7pxkxl1rKSmllxqhkd9a0HEKy50ncy\"}', '{\"password\":\"$2y$10$EZ93yZIOFyxrrSlZRLy3reX4UA\\/h5M54W.5kHFErNSauC0yudZIAG\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:55:32'),
(291, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$EZ93yZIOFyxrrSlZRLy3reX4UA\\/h5M54W.5kHFErNSauC0yudZIAG\"}', '{\"password\":\"$2y$10$QXELp8yaSkiOFhtqC8IgRuerlQ3iQ0X1l1kR1s7cY4X6BMOHnmm.S\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 00:55:37'),
(292, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$QXELp8yaSkiOFhtqC8IgRuerlQ3iQ0X1l1kR1s7cY4X6BMOHnmm.S\"}', '{\"password\":\"$2y$10$4ELxQRKYvW5Dmksh1yUOW.liTFS7rtJu3aKt7\\/USSQ.DgylpdjF9y\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:00:26'),
(293, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$4ELxQRKYvW5Dmksh1yUOW.liTFS7rtJu3aKt7\\/USSQ.DgylpdjF9y\"}', '{\"password\":\"$2y$10$lGuSJl3QSD1eFUk4xLu2KeXO\\/K1iYGKtZSyPWT5yW7vOWEc3ob5v6\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:01:09'),
(294, 1, 'updated', 12, 'Syspamp\\User', '{\"password\":\"$2y$10$lGuSJl3QSD1eFUk4xLu2KeXO\\/K1iYGKtZSyPWT5yW7vOWEc3ob5v6\"}', '{\"password\":\"$2y$10$qkgSSZRVwG1LJK\\/kVUtAWeL6DmfUVvnN7AvpwX0igm8ouwh06bNKC\"}', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:01:30'),
(295, 1, 'created', 13, 'Syspamp\\User', '[]', '{\"name\":\"ASDSAD\",\"lastname\":\"ASDSAD\",\"email\":\"ASDSA@SADASD.com\",\"password\":\"$2y$10$RThabVdqVDnIA6LD0YGWJew2jB\\/UUudWUBdeSzBos.CL1zXzRySJq\",\"type\":\"2\",\"status\":\"off\",\"id\":13}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:03:57'),
(296, 1, 'deleted', 13, 'Syspamp\\User', '{\"id\":13,\"name\":\"ASDSAD\",\"lastname\":\"ASDSAD\",\"email\":\"ASDSA@SADASD.com\",\"password\":\"$2y$10$RThabVdqVDnIA6LD0YGWJew2jB\\/UUudWUBdeSzBos.CL1zXzRySJq\",\"type\":2,\"status\":\"off\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 22:04:03\"}', '[]', 'http://localhost:8000/usuarios/13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:04:03'),
(297, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"ClKqI66tljYe2Hhf6pMZmjOQl7u7kesxhoxWMFPxNSWQA2h8FOql9VWYMJb7\"}', '{\"remember_token\":\"NJtHRU8LXDZ0o2QQCTH70xaqoQzuqB5b0vrxhjj2HBbwVEhts1ESFz2Zm1tP\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:10:11'),
(298, 1, 'created', 14, 'Syspamp\\User', '[]', '{\"name\":\"TEST\",\"lastname\":\"TEST\",\"email\":\"TEST@xn--est-o0a.com\",\"password\":\"$2y$10$xawOCLa4nd5RVLr1pPt6NeOd4UgPD6ZI1CiSbjQHShyrVNsh4CZUO\",\"type\":\"2\",\"status\":\"on\",\"id\":14}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:10:27'),
(299, 1, 'deleted', 14, 'Syspamp\\User', '{\"id\":14,\"name\":\"TEST\",\"lastname\":\"TEST\",\"email\":\"TEST@xn--est-o0a.com\",\"password\":\"$2y$10$xawOCLa4nd5RVLr1pPt6NeOd4UgPD6ZI1CiSbjQHShyrVNsh4CZUO\",\"type\":2,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-08-06 22:10:33\"}', '[]', 'http://localhost:8000/usuarios/14', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:10:33'),
(300, 1, 'deleted', 12, 'Syspamp\\User', '{\"id\":12,\"name\":\"TEST\",\"lastname\":\"TEST34\",\"email\":\"TEST@TEST.COM\",\"password\":\"$2y$10$qkgSSZRVwG1LJK\\/kVUtAWeL6DmfUVvnN7AvpwX0igm8ouwh06bNKC\",\"type\":2,\"status\":\"on\",\"remember_token\":\"tOJqy7yUWvRbJQmuDrb3Pn2gvEtwnwnGHulqoEf9oHIMEQc1iNjGcL5MDsDU\",\"deleted_at\":\"2017-08-06 22:13:01\"}', '[]', 'http://localhost:8000/usuarios/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:13:01');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(301, 1, 'updated', 5, 'Syspamp\\User', '{\"password\":\"$2y$10$SxJNyakgotdKwEufPwNfRuH08oZE5lk6fQHJDdqLDuqcqkLo69J62\"}', '{\"password\":\"$2y$10$KlcpkFw6zE1vqX3aE3oKYOCpLnS.WDjijobKHZdwvkRB3FBHSVLiW\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:13:09'),
(302, 1, 'updated', 6, 'Syspamp\\User', '{\"password\":\"$2y$10$CJgSubbXaxmomENXKw2OuuXYbt3lc.zs2ohrIYUZyikUw6LiHGy\\/G\"}', '{\"password\":\"$2y$10$dzjoSKxap1JEEh5habO4y.KYbp5fShhJERqKryMQpq1.l\\/M1jVzE6\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:13:24'),
(303, 1, 'updated', 6, 'Syspamp\\User', '{\"password\":\"$2y$10$dzjoSKxap1JEEh5habO4y.KYbp5fShhJERqKryMQpq1.l\\/M1jVzE6\"}', '{\"password\":\"$2y$10$wa2FUEXEGffZ3k4kXGny9.meses7Egn\\/dJtxDcTdL\\/kPO8AZUtMWq\"}', 'http://localhost:8000/usuarios/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:13:29'),
(304, 1, 'updated', 5, 'Syspamp\\User', '{\"password\":\"$2y$10$KlcpkFw6zE1vqX3aE3oKYOCpLnS.WDjijobKHZdwvkRB3FBHSVLiW\"}', '{\"password\":\"$2y$10$jPI9nnwjIq0QpTTWm2tmJe9zsi4ZFeTuyRXLdc1gQ8bfxSllzDX56\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:13:34'),
(305, 1, 'updated', 1, 'Syspamp\\User', '{\"password\":\"$2y$10$Ub.6e0uDHYisDw6niCslEuPwiiHYOEhs2ciCqfCkUYuqJYVVH4Rcu\"}', '{\"password\":\"$2y$10$2hqnGsiOJvXlA5LdBJgNSOWZBeJjrG3OookVtnoLYlZaSj.5qlzui\"}', 'http://localhost:8000/usuarios/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:13:45'),
(306, 1, 'updated', 3, 'Syspamp\\User', '{\"password\":\"$2y$10$Ezy5RT7y15rFzY8pDNMqNOeOXeh0GGd5LFCXQvr6nYVEcyfn3wVYW\"}', '{\"password\":\"$2y$10$ezvW0hfWQtxo68OZU3ySPuiVzTY6hB0vEfbafzXZGub53vW.h6TCG\"}', 'http://localhost:8000/usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:14:01'),
(307, 1, 'updated', 5, 'Syspamp\\User', '{\"password\":\"$2y$10$jPI9nnwjIq0QpTTWm2tmJe9zsi4ZFeTuyRXLdc1gQ8bfxSllzDX56\"}', '{\"password\":\"$2y$10$fMFCcS\\/beGdrIp2SJiIZ6.XlRKkeUiInafdkPYVe203Y0mjbyiruO\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:14:07'),
(308, 1, 'updated', 4, 'Syspamp\\User', '{\"password\":\"$2y$10$JhiXfzc9tW17u3rGAarAn.2d7cvVO3BRt8SMmDtQRVORF4WtBc.qG\"}', '{\"password\":\"$2y$10$kQghFJPSUMgOmNxGHC0pT.qno1ctpaByBAwgoVWM72Wueh6mS437e\"}', 'http://localhost:8000/usuarios/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:14:13'),
(309, 1, 'updated', 3, 'Syspamp\\User', '{\"password\":\"$2y$10$ezvW0hfWQtxo68OZU3ySPuiVzTY6hB0vEfbafzXZGub53vW.h6TCG\"}', '{\"password\":\"$2y$10$eQonoZlSZpQ1HwPsHBTPPuCDNgXJXDXdB6FEd2tFVDUec3AsJo1Ya\"}', 'http://localhost:8000/usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:14:21'),
(310, 1, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracion\"}', '{\"description\":\"Administracione\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:44:28'),
(311, 1, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracione\"}', '{\"description\":\"Administracion\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:44:30'),
(312, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"HAASD\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:46:31'),
(313, 1, 'deleted', 9, 'Syspamp\\TypeUser', '{\"id\":9,\"description\":\"HAASD\",\"deleted_at\":\"2017-08-06 22:46:39\"}', '[]', 'http://localhost:8000/tipo_usuarios/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:46:39'),
(314, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"asd\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:47:01'),
(315, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"DSFDS\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 01:49:25'),
(316, 1, 'created', 0, 'Syspamp\\TypeUser', '[]', '{\"id\":0,\"description\":\"TESTTE\"}', 'http://localhost:8000/tipo_usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 02:21:00'),
(317, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"NJtHRU8LXDZ0o2QQCTH70xaqoQzuqB5b0vrxhjj2HBbwVEhts1ESFz2Zm1tP\"}', '{\"remember_token\":\"bI59Ot1qcyYjKAEwTlXymSRahngeYl4Hk809u7Bpv5Nc2fMXaxDsW32jNDrB\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 03:27:54'),
(318, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"bI59Ot1qcyYjKAEwTlXymSRahngeYl4Hk809u7Bpv5Nc2fMXaxDsW32jNDrB\"}', '{\"remember_token\":\"ZAeSSfTfk8gr8e4pDz2yZMRP0ODHCd4zfQ2lMWCbUStA8Ycor9mdzVkWzATJ\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-07 03:28:06'),
(319, 3, 'updated', 3, 'Syspamp\\User', '{\"remember_token\":\"Z7oWcHbg0BTGkcMt6G53Y15X1niwM92umSCUxMLXPsvh94zBLSPwcvagmd4J\"}', '{\"remember_token\":\"6LtFSqb8bLdact1Xp0LLaHwvs6grP0IM3k5HjtatnTixKLKHiwosmWdvm5YR\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 06:31:24'),
(320, 1, 'updated', 2, 'Syspamp\\User', '{\"lastname\":\"Administracion\"}', '{\"lastname\":\"jaja\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 06:46:08'),
(321, 1, 'updated', 2, 'Syspamp\\User', '{\"lastname\":\"jaja\"}', '{\"lastname\":\"Administracion\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 06:46:21'),
(322, 1, 'deleted', 11, 'Syspamp\\TypeUser', '{\"id\":11,\"description\":\"asd\",\"deleted_at\":\"2017-08-09 03:50:00\"}', '[]', 'http://localhost:8000/tipo_usuarios/11', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 06:50:00'),
(323, 1, 'deleted', 45, 'Syspamp\\TypeUser', '{\"id\":45,\"description\":\"DSFDS\",\"deleted_at\":\"2017-08-09 03:53:19\"}', '[]', 'http://localhost:8000/tipo_usuarios/45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 06:53:19'),
(324, 1, 'updated', 5, 'Syspamp\\TypeUser', '{\"description\":\"Laboratorio\"}', '{\"description\":\"Laboratorioa\"}', 'http://localhost:8000/tipo_usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:23:03'),
(325, 1, 'updated', 5, 'Syspamp\\TypeUser', '{\"description\":\"Laboratorioa\"}', '{\"description\":\"Laboratorio\"}', 'http://localhost:8000/tipo_usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:23:07'),
(326, 1, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracion\"}', '{\"description\":\"Administracione\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:28:28'),
(327, 1, 'updated', 2, 'Syspamp\\TypeUser', '{\"description\":\"Administracione\"}', '{\"description\":\"Administracion\"}', 'http://localhost:8000/tipo_usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:28:31'),
(328, 1, 'deleted', 56, 'Syspamp\\TypeUser', '{\"id\":56,\"description\":\"TESTTE\",\"deleted_at\":\"2017-08-09 04:28:33\"}', '[]', 'http://localhost:8000/tipo_usuarios/56', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:28:33'),
(329, 1, 'created', 3, 'Syspamp\\CondicionIva', '[]', '{\"description\":\"asdsad\",\"id\":3}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:37:11'),
(330, 1, 'updated', 3, 'Syspamp\\TypeUser', '{\"description\":\"Produccion\"}', '{\"description\":\"dfgfdProduccion\"}', 'http://localhost:8000/tipo_usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:38:01'),
(331, 1, 'updated', 3, 'Syspamp\\TypeUser', '{\"description\":\"dfgfdProduccion\"}', '{\"description\":\"Produccion\"}', 'http://localhost:8000/tipo_usuarios/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 07:38:09'),
(332, 1, 'deleted', 3, 'Syspamp\\CondicionIva', '{\"id\":3,\"description\":\"asdsad\",\"deleted_at\":\"2017-08-09 05:13:52\"}', '[]', 'http://localhost:8000/condiciones_ivas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:13:52'),
(333, 1, 'updated', 3, 'Syspamp\\TypePerson', '{\"description\":\"EMPLEADO\"}', '{\"description\":\"EMPLEADOAA\"}', 'http://localhost:8000/tipo_personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:25:10'),
(334, 1, 'updated', 3, 'Syspamp\\TypePerson', '{\"description\":\"EMPLEADOAA\"}', '{\"description\":\"EMPLEADO\"}', 'http://localhost:8000/tipo_personas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:25:15'),
(335, 1, 'created', 25, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asdasd\",\"id\":25}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:34:31'),
(336, 1, 'deleted', 25, 'Syspamp\\TypeProduct', '{\"id\":25,\"description\":\"asdasd\",\"deleted_at\":\"2017-08-09 05:35:13\"}', '[]', 'http://localhost:8000/tipo_productos/25', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:35:13'),
(337, 1, 'created', 26, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asdasdsa\",\"id\":26}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:35:16'),
(338, 1, 'updated', 26, 'Syspamp\\TypeProduct', '{\"description\":\"asdasdsa\"}', '{\"description\":\"asdasdas\"}', 'http://localhost:8000/tipo_productos/26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:35:22'),
(339, 1, 'updated', 26, 'Syspamp\\TypeProduct', '{\"description\":\"asdasdas\"}', '{\"description\":\"NO BLDOasdasdas\"}', 'http://localhost:8000/tipo_productos/26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:35:36'),
(340, 1, 'deleted', 26, 'Syspamp\\TypeProduct', '{\"id\":26,\"description\":\"NO BLDOasdasdas\",\"deleted_at\":\"2017-08-09 05:35:47\"}', '[]', 'http://localhost:8000/tipo_productos/26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:35:47'),
(341, 1, 'created', 4, 'Syspamp\\TypePerson', '[]', '{\"description\":\"MONO\",\"id\":4}', 'http://localhost:8000/tipo_personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:36:07'),
(342, 1, 'deleted', 4, 'Syspamp\\TypePerson', '{\"id\":4,\"description\":\"MONO\",\"deleted_at\":\"2017-08-09 05:36:10\"}', '[]', 'http://localhost:8000/tipo_personas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-09 08:36:10'),
(343, 1, 'created', 1, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"En Reparacion\",\"id\":1}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:30:29'),
(344, 1, 'created', 2, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"Activa\",\"id\":2}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:30:43'),
(345, 1, 'created', 3, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"Inactivo\",\"id\":3}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:30:47'),
(346, 1, 'updated', 1, 'Syspamp\\MachineStatus', '{\"description\":\"En Reparacion\"}', '{\"description\":\"En Reparaci\\u00f3n\"}', 'http://localhost:8000/estados_maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:30:57'),
(347, 1, 'created', 4, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"JAJa\",\"id\":4}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:31:00'),
(348, 1, 'deleted', 4, 'Syspamp\\MachineStatus', '{\"id\":4,\"description\":\"JAJa\",\"deleted_at\":\"2017-08-20 22:31:33\"}', '[]', 'http://localhost:8000/estados_maquinas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:31:33'),
(349, 1, 'created', 1, 'Syspamp\\Machine', '[]', '{\"description\":\"Molino\",\"id_status_machine\":\"2\",\"id\":1}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:49:30'),
(350, 1, 'updated', 1, 'Syspamp\\Machine', '{\"id_status_machine\":2}', '{\"id_status_machine\":\"1\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:56:47'),
(351, 1, 'updated', 1, 'Syspamp\\Machine', '{\"id_status_machine\":1}', '{\"id_status_machine\":\"3\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 01:59:44'),
(352, 1, 'created', 2, 'Syspamp\\Machine', '[]', '{\"description\":\"Molino 2\",\"id_status_machine\":\"2\",\"id\":2}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 02:00:07'),
(353, 1, 'updated', 2, 'Syspamp\\Machine', '{\"id_status_machine\":2}', '{\"id_status_machine\":\"1\"}', 'http://localhost:8000/maquinas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 02:00:12'),
(354, 1, 'updated', 1, 'Syspamp\\CondicionIva', '{\"description\":\"Monotributo\"}', '{\"description\":\"Monotributoa\"}', 'http://localhost:8000/condiciones_ivas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 04:42:12'),
(355, 1, 'updated', 1, 'Syspamp\\CondicionIva', '{\"description\":\"Monotributoa\"}', '{\"description\":\"Monotributo\"}', 'http://localhost:8000/condiciones_ivas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 04:42:15'),
(356, 1, 'created', 4, 'Syspamp\\CondicionIva', '[]', '{\"description\":\"na\",\"id\":4}', 'http://localhost:8000/condiciones_ivas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 04:42:18'),
(357, 1, 'deleted', 4, 'Syspamp\\CondicionIva', '{\"id\":4,\"description\":\"na\",\"deleted_at\":\"2017-08-21 01:42:20\"}', '[]', 'http://localhost:8000/condiciones_ivas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 04:42:20'),
(358, 1, 'updated', 1, 'Syspamp\\Machine', '{\"id_status_machine\":3}', '{\"id_status_machine\":\"1\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 06:15:48'),
(359, 1, 'updated', 1, 'Syspamp\\Machine', '{\"id_status_machine\":1}', '{\"id_status_machine\":\"2\"}', 'http://localhost:8000/maquinas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 06:16:02'),
(360, 1, 'updated', 2, 'Syspamp\\Machine', '{\"id_status_machine\":1}', '{\"id_status_machine\":\"3\"}', 'http://localhost:8000/maquinas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 07:44:47'),
(361, 1, 'created', 1, 'Syspamp\\Country', '[]', '{\"description\":\"Argentina\",\"id\":1}', 'http://localhost:8000/paises', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:19:03'),
(362, 1, 'created', 2, 'Syspamp\\Country', '[]', '{\"description\":\"Brazil\",\"id\":2}', 'http://localhost:8000/paises', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:19:31'),
(363, 1, 'created', 3, 'Syspamp\\Country', '[]', '{\"description\":\"Venezuela\",\"id\":3}', 'http://localhost:8000/paises', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:19:33'),
(364, 1, 'created', 4, 'Syspamp\\Country', '[]', '{\"description\":\"Bolivia\",\"id\":4}', 'http://localhost:8000/paises', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:19:35'),
(365, 1, 'updated', 1, 'Syspamp\\Country', '{\"description\":\"Argentina\"}', '{\"description\":\"Argentinae\"}', 'http://localhost:8000/paises/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:21:13'),
(366, 1, 'updated', 1, 'Syspamp\\Country', '{\"description\":\"Argentinae\"}', '{\"description\":\"Argentina\"}', 'http://localhost:8000/paises/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:21:15'),
(367, 1, 'created', 1, 'Syspamp\\Province', '[]', '{\"description\":\"Cordoba\",\"id_country\":\"1\",\"id\":1}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:28:45'),
(368, 1, 'created', 2, 'Syspamp\\Province', '[]', '{\"description\":\"Brasilia\",\"id_country\":\"2\",\"id\":2}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:29:37'),
(369, 1, 'created', 3, 'Syspamp\\Province', '[]', '{\"description\":\"Buenos Aires\",\"id_country\":\"1\",\"id\":3}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:31:51'),
(370, 1, 'created', 4, 'Syspamp\\Province', '[]', '{\"description\":\"JAJa\",\"id_country\":\"4\",\"id\":4}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:37:26'),
(371, 1, 'deleted', 4, 'Syspamp\\Province', '{\"id\":4,\"description\":\"JAJa\",\"id_country\":4,\"deleted_at\":\"2017-08-21 05:37:29\"}', '[]', 'http://localhost:8000/provincias/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:37:29'),
(372, 1, 'updated', 1, 'Syspamp\\Province', '{\"id_country\":1}', '{\"id_country\":\"2\"}', 'http://localhost:8000/provincias/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:38:20'),
(373, 1, 'updated', 1, 'Syspamp\\Province', '{\"id_country\":2}', '{\"id_country\":\"1\"}', 'http://localhost:8000/provincias/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 08:38:25'),
(374, 1, 'created', 1, 'Syspamp\\City', '[]', '{\"description\":\"Capital\",\"id_province\":\"1\",\"id\":1}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:09:37'),
(375, 1, 'created', 2, 'Syspamp\\City', '[]', '{\"description\":\"Alta Gracia\",\"id_province\":\"1\",\"id\":2}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:10:41'),
(376, 1, 'updated', 1, 'Syspamp\\City', '{\"description\":\"Capital\"}', '{\"description\":\"C\\u00f3rdoba\"}', 'http://localhost:8000/ciudades/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:10:57'),
(377, 1, 'updated', 1, 'Syspamp\\City', '{\"description\":\"C\\u00f3rdoba\"}', '{\"description\":\"Capital\"}', 'http://localhost:8000/ciudades/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:11:07'),
(378, 1, 'updated', 1, 'Syspamp\\City', '{\"description\":\"Capital\"}', '{\"description\":\"C\\u00f3rdoba\"}', 'http://localhost:8000/ciudades/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:11:54'),
(379, 1, 'created', 3, 'Syspamp\\City', '[]', '{\"description\":\"TEST\",\"id_province\":\"2\",\"id\":3}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:26:29'),
(380, 1, 'deleted', 3, 'Syspamp\\City', '{\"id\":3,\"description\":\"TEST\",\"id_province\":2,\"deleted_at\":\"2017-08-21 06:26:34\"}', '[]', 'http://localhost:8000/ciudades/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:26:34'),
(381, 1, 'created', 4, 'Syspamp\\City', '[]', '{\"description\":\"TESt\",\"id_province\":\"2\",\"id\":4}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:26:46'),
(382, 1, 'created', 6, 'Syspamp\\City', '[]', '{\"description\":\"JAJA\",\"id_province\":\"2\",\"id\":6}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-21 09:32:11'),
(383, 1, 'deleted', 6, 'Syspamp\\City', '{\"id\":6,\"description\":\"JAJA\",\"id_province\":2,\"deleted_at\":\"2017-08-22 00:55:02\"}', '[]', 'http://localhost:8000/ciudades/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 03:55:02'),
(384, 1, 'created', 5, 'Syspamp\\Province', '[]', '{\"description\":\"Chaco\",\"id_country\":\"1\",\"id\":5}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:15:00'),
(385, 1, 'created', 6, 'Syspamp\\Province', '[]', '{\"description\":\"Chaco\",\"id_country\":\"1\",\"id\":6}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:15:25'),
(386, 1, 'deleted', 6, 'Syspamp\\Province', '{\"id\":6,\"description\":\"Chaco\",\"id_country\":1,\"deleted_at\":\"2017-08-22 03:15:28\"}', '[]', 'http://localhost:8000/provincias/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:15:28'),
(387, 1, 'updated', 1, 'Syspamp\\Province', '{\"description\":\"\"}', '{\"description\":\"Cordoba\"}', 'http://localhost:8000/provincias/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:17:29'),
(388, 1, 'updated', 2, 'Syspamp\\Province', '{\"description\":\"\"}', '{\"description\":\"Sao Pablo\"}', 'http://localhost:8000/provincias/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:17:34'),
(389, 1, 'updated', 5, 'Syspamp\\Province', '{\"description\":\"\"}', '{\"description\":\"Buenos Aires\"}', 'http://localhost:8000/provincias/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:17:40'),
(390, 1, 'updated', 3, 'Syspamp\\Province', '{\"description\":\"\"}', '{\"description\":\"Tucuman\"}', 'http://localhost:8000/provincias/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:17:45'),
(391, 1, 'created', 7, 'Syspamp\\Province', '[]', '{\"description\":\"Caracas\",\"id_country\":\"3\",\"id\":7}', 'http://localhost:8000/provincias', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:17:54'),
(392, 1, 'created', 8, 'Syspamp\\City', '[]', '{\"description\":\"Monte Cristo\",\"id_province\":\"1\",\"id\":8}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:44:18'),
(393, 1, 'deleted', 4, 'Syspamp\\City', '{\"id\":4,\"description\":\"TESt\",\"id_province\":2,\"deleted_at\":\"2017-08-22 03:57:46\"}', '[]', 'http://localhost:8000/ciudades/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:57:46'),
(394, 1, 'created', 9, 'Syspamp\\City', '[]', '{\"id_province\":\"2\",\"description\":\"JAJA\",\"id\":9}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:59:13'),
(395, 1, 'deleted', 9, 'Syspamp\\City', '{\"id\":9,\"description\":\"JAJA\",\"id_province\":2,\"deleted_at\":\"2017-08-22 03:59:16\"}', '[]', 'http://localhost:8000/ciudades/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 06:59:16'),
(396, 1, 'created', 10, 'Syspamp\\City', '[]', '{\"id_province\":\"2\",\"description\":\"Mono\",\"id\":10}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 07:54:11'),
(397, 1, 'updated', 8, 'Syspamp\\City', '{\"id_province\":1}', '{\"id_province\":\"3\"}', 'http://localhost:8000/ciudades/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 08:10:57'),
(398, 1, 'updated', 35, 'Syspamp\\Person', '{\"name\":\"asdasd\"}', '{\"name\":\"HOLA\"}', 'http://localhost:8000/personas/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 08:17:17'),
(399, 1, 'updated', 31, 'Syspamp\\Person', '{\"name\":\"ASDasd\"}', '{\"name\":\"ASDasdasdasdsa\"}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 08:17:51'),
(400, 1, 'updated', 35, 'Syspamp\\Person', '{\"lastname\":\"sadsa\"}', '{\"lastname\":\"DALE\"}', 'http://localhost:8000/personas/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-22 08:18:00'),
(401, 1, 'updated', 31, 'Syspamp\\Person', '{\"phone_1\":null,\"phone_2\":null,\"phone_3\":null}', '{\"phone_1\":\"64645\",\"phone_2\":\"456456\",\"phone_3\":\"456456\"}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 20:33:19'),
(402, 1, 'updated', 32, 'Syspamp\\Person', '{\"phone_2\":null}', '{\"phone_2\":\"4545\"}', 'http://localhost:8000/personas/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 20:33:52'),
(403, 1, 'created', 1, 'Syspamp\\LegalPerson', '[]', '{\"name\":\"SDFDSF\",\"cuit\":\"345435\",\"url\":\"SDFDSF\",\"adress\":\"SDFDSF\",\"phone_1\":\"SDFDS34534534\",\"phone_2\":\"34534\",\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id_city\":\"1\",\"id\":1}', 'http://localhost:8000/empresas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:42:54'),
(404, 1, 'created', 2, 'Syspamp\\LegalPerson', '[]', '{\"name\":\"TEST\",\"cuit\":\"435345\",\"url\":\"ASDFDSF\",\"adress\":\"345345\",\"phone_1\":\"43534\",\"phone_2\":\"345435\",\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id_city\":\"2\",\"id\":2}', 'http://localhost:8000/empresas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:43:37'),
(405, 1, 'updated', 2, 'Syspamp\\LegalPerson', '{\"id_condicion_iva\":1}', '{\"id_condicion_iva\":\"2\"}', 'http://localhost:8000/empresas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:43:48'),
(406, 1, 'updated', 2, 'Syspamp\\LegalPerson', '{\"id_type_people\":1}', '{\"id_type_people\":\"2\"}', 'http://localhost:8000/empresas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:43:53'),
(407, 1, 'updated', 2, 'Syspamp\\LegalPerson', '{\"id_city\":2}', '{\"id_city\":\"1\"}', 'http://localhost:8000/empresas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:43:58'),
(408, 1, 'updated', 1, 'Syspamp\\LegalPerson', '{\"id_city\":1}', '{\"id_city\":\"2\"}', 'http://localhost:8000/empresas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:44:05'),
(409, 1, 'updated', 31, 'Syspamp\\Person', '{\"adress\":null}', '{\"adress\":\"sdfdsfdsf\"}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:45:48'),
(410, 1, 'updated', 35, 'Syspamp\\Person', '{\"name\":\"HOLA\"}', '{\"name\":\"JAJAJ\"}', 'http://localhost:8000/personas/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:45:55'),
(411, 1, 'deleted', 7, 'Syspamp\\Person', '{\"id\":7,\"name\":\"DA\",\"lastname\":\"KE\",\"email\":\"santiagopujol92@gmail.co\",\"cuit\":\"546546\",\"id_condicion_iva\":2,\"id_type_people\":3,\"adress\":\"Chascomus 1782\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-08-26 18:46:04\"}', '[]', 'http://localhost:8000/personas/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 21:46:05'),
(412, 1, 'updated', 1, 'Syspamp\\User', '{\"lastname\":\"Admin\"}', '{\"lastname\":\"Admin3\"}', 'http://localhost:8000/usuarios/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:04:38'),
(413, 1, 'updated', 1, 'Syspamp\\User', '{\"lastname\":\"Admin3\"}', '{\"lastname\":\"Admin\"}', 'http://localhost:8000/usuarios/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:04:41'),
(414, 1, 'updated', 1, 'Syspamp\\CondicionIva', '{\"description\":\"Monotributo\"}', '{\"description\":\"Monotributoe\"}', 'http://localhost:8000/condiciones_ivas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:06:11'),
(415, 1, 'updated', 1, 'Syspamp\\CondicionIva', '{\"description\":\"Monotributoe\"}', '{\"description\":\"Monotributo\"}', 'http://localhost:8000/condiciones_ivas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:06:15'),
(416, 1, 'created', 12, 'Syspamp\\City', '[]', '{\"id_province\":\"2\",\"description\":\"asdasd\",\"id\":12}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:12:48'),
(417, 1, 'updated', 12, 'Syspamp\\City', '{\"description\":\"asdasd\",\"id_province\":2}', '{\"description\":\"teST\",\"id_province\":\"7\"}', 'http://localhost:8000/ciudades/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:12:57'),
(418, 1, 'deleted', 12, 'Syspamp\\City', '{\"id\":12,\"description\":\"teST\",\"id_province\":7,\"deleted_at\":\"2017-08-26 19:13:00\"}', '[]', 'http://localhost:8000/ciudades/12', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:13:00'),
(419, 1, 'created', 13, 'Syspamp\\City', '[]', '{\"id_province\":\"1\",\"description\":\"asdas\",\"id\":13}', 'http://localhost:8000/ciudades', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:13:09'),
(420, 1, 'updated', 13, 'Syspamp\\City', '{\"id_province\":1}', '{\"id_province\":\"7\"}', 'http://localhost:8000/ciudades/13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:13:18'),
(421, 1, 'deleted', 13, 'Syspamp\\City', '{\"id\":13,\"description\":\"asdas\",\"id_province\":7,\"deleted_at\":\"2017-08-26 19:13:20\"}', '[]', 'http://localhost:8000/ciudades/13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:13:20'),
(422, 1, 'created', 3, 'Syspamp\\LegalPerson', '[]', '{\"name\":\"TEST\",\"cuit\":\"32234\",\"url\":\"ASDSAD\",\"adress\":\"DASFAD\",\"phone_1\":\"345435\",\"phone_2\":\"345435\",\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id_city\":\"10\",\"id\":3}', 'http://localhost:8000/empresas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 22:50:45'),
(423, 1, 'updated', 2, 'Syspamp\\LegalPerson', '{\"name\":\"TEST\",\"cuit\":\"435345\",\"id_condicion_iva\":2,\"adress\":\"345345\",\"url\":\"ASDFDSF\",\"phone_1\":\"43534\",\"phone_2\":\"345435\"}', '{\"name\":\"fdgdf\",\"cuit\":\"fdg\",\"id_condicion_iva\":\"1\",\"adress\":\"dfgfdg\",\"url\":\"dfgdfg\",\"phone_1\":\"dfgdfg\",\"phone_2\":\"dfgfdg\"}', 'http://localhost:8000/empresas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:08:08'),
(424, 1, 'created', 4, 'Syspamp\\LegalPerson', '[]', '{\"name\":\"TEST\",\"cuit\":\"TEST\",\"url\":\"ASDFSA\",\"adress\":\"ASDSAD\",\"phone_1\":\"435345\",\"phone_2\":\"435\",\"id_condicion_iva\":\"2\",\"id_type_people\":\"1\",\"id_city\":\"10\",\"id\":4}', 'http://localhost:8000/empresas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:09:35'),
(425, 1, 'updated', 4, 'Syspamp\\LegalPerson', '{\"id_city\":10}', '{\"id_city\":\"8\"}', 'http://localhost:8000/empresas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:13:45'),
(426, 1, 'updated', 31, 'Syspamp\\Person', '{\"floor\":null,\"department\":null,\"phone_3\":\"456456\"}', '{\"floor\":\"45\",\"department\":\"45\",\"phone_3\":\"45\"}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:28:29'),
(427, 1, 'updated', 32, 'Syspamp\\Person', '{\"floor\":null,\"department\":null,\"phone_2\":\"4545\",\"phone_3\":null}', '{\"floor\":\"543\",\"department\":\"345\",\"phone_2\":null,\"phone_3\":\"345\"}', 'http://localhost:8000/personas/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:28:50'),
(428, 1, 'updated', 32, 'Syspamp\\Person', '{\"department\":\"345\",\"phone_2\":null}', '{\"department\":null,\"phone_2\":\"345\"}', 'http://localhost:8000/personas/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:28:55'),
(429, 1, 'updated', 32, 'Syspamp\\Person', '{\"adress\":null}', '{\"adress\":\"sad234\"}', 'http://localhost:8000/personas/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:28:58'),
(430, 1, 'updated', 35, 'Syspamp\\Person', '{\"phone_3\":null}', '{\"phone_3\":\"345\"}', 'http://localhost:8000/personas/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:29:05'),
(431, 1, 'updated', 16, 'Syspamp\\Person', '{\"phone_3\":null}', '{\"phone_3\":\"345\"}', 'http://localhost:8000/personas/16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:29:18'),
(432, 1, 'updated', 35, 'Syspamp\\Person', '{\"id_city\":1,\"phone_2\":null}', '{\"id_city\":\"8\",\"phone_2\":\"345\"}', 'http://localhost:8000/personas/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-26 23:29:35'),
(433, 1, 'updated', 31, 'Syspamp\\Person', '{\"id_city\":1,\"phone_2\":\"456456\",\"phone_3\":\"45\"}', '{\"id_city\":\"8\",\"phone_2\":\"45\",\"phone_3\":null}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 00:04:51'),
(434, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"ZAeSSfTfk8gr8e4pDz2yZMRP0ODHCd4zfQ2lMWCbUStA8Ycor9mdzVkWzATJ\"}', '{\"remember_token\":\"AJ3hNZcDxQyMXmmoJO1RujZKWVNikCSSOk7WCRzxgMhVdAzFdJrJ4HreDhoP\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 00:56:56'),
(435, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"AJ3hNZcDxQyMXmmoJO1RujZKWVNikCSSOk7WCRzxgMhVdAzFdJrJ4HreDhoP\"}', '{\"remember_token\":\"m6kQCOp8nyynRH13AZx4mxfqWVBJRDFiISCMNlwQZ5NEfGqDBgUoVZafLrzl\"}', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 00:57:47'),
(436, 1, 'created', 1, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"Activa\",\"id\":1}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 01:58:47'),
(437, 1, 'updated', 1, 'Syspamp\\StatusInvoice', '{\"description\":\"Activa\"}', '{\"description\":\"Activas\"}', 'http://localhost:8000/estados_facturas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 01:59:38'),
(438, 1, 'updated', 1, 'Syspamp\\StatusInvoice', '{\"description\":\"Activas\"}', '{\"description\":\"Activa\"}', 'http://localhost:8000/estados_facturas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 01:59:43'),
(439, 1, 'updated', 1, 'Syspamp\\StatusInvoice', '{\"description\":\"Activa\"}', '{\"description\":\"Activae\"}', 'http://localhost:8000/estados_facturas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:00:12'),
(440, 1, 'updated', 1, 'Syspamp\\StatusInvoice', '{\"description\":\"Activae\"}', '{\"description\":\"Activa\"}', 'http://localhost:8000/estados_facturas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:00:14'),
(441, 1, 'created', 2, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"JAJA\",\"id\":2}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:00:21'),
(442, 1, 'created', 3, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"asdsad\",\"id\":3}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:01:10'),
(443, 1, 'created', 5, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"asd\",\"id\":5}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:02:00'),
(444, 1, 'created', 6, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"asdsad\",\"id\":6}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:02:09'),
(445, 1, 'deleted', 5, 'Syspamp\\MachineStatus', '{\"id\":5,\"description\":\"asd\",\"deleted_at\":\"2017-08-26 23:02:20\"}', '[]', 'http://localhost:8000/estados_maquinas/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:02:20'),
(446, 1, 'deleted', 6, 'Syspamp\\MachineStatus', '{\"id\":6,\"description\":\"asdsad\",\"deleted_at\":\"2017-08-26 23:02:22\"}', '[]', 'http://localhost:8000/estados_maquinas/6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:02:22'),
(447, 1, 'created', 1, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"Activo\",\"id\":1}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:03:12'),
(448, 1, 'created', 2, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"Pendiente de Pago\",\"id\":2}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:03:15'),
(449, 1, 'created', 3, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"Pagado\",\"id\":3}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:03:20'),
(450, 1, 'deleted', 1, 'Syspamp\\StatusShipment', '{\"id\":1,\"description\":\"Activo\",\"deleted_at\":\"2017-08-26 23:03:31\"}', '[]', 'http://localhost:8000/estados_remitos/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:03:31'),
(451, 1, 'deleted', 2, 'Syspamp\\StatusShipment', '{\"id\":2,\"description\":\"Pendiente de Pago\",\"deleted_at\":\"2017-08-26 23:04:27\"}', '[]', 'http://localhost:8000/estados_remitos/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:27'),
(452, 1, 'deleted', 3, 'Syspamp\\StatusInvoice', '{\"id\":3,\"description\":\"asdsad\",\"deleted_at\":\"2017-08-26 23:04:34\"}', '[]', 'http://localhost:8000/estados_facturas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:34'),
(453, 1, 'deleted', 1, 'Syspamp\\StatusInvoice', '{\"id\":1,\"description\":\"Activa\",\"deleted_at\":\"2017-08-26 23:04:36\"}', '[]', 'http://localhost:8000/estados_facturas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:36'),
(454, 1, 'deleted', 2, 'Syspamp\\StatusInvoice', '{\"id\":2,\"description\":\"JAJA\",\"deleted_at\":\"2017-08-26 23:04:37\"}', '[]', 'http://localhost:8000/estados_facturas/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:37'),
(455, 1, 'created', 4, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"Pagada\",\"id\":4}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:41'),
(456, 1, 'created', 5, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"Pendiente de Pago\",\"id\":5}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:44'),
(457, 1, 'created', 6, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"Anulada\",\"id\":6}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:04:50'),
(458, 1, 'deleted', 3, 'Syspamp\\StatusShipment', '{\"id\":3,\"description\":\"Pagado\",\"deleted_at\":\"2017-08-26 23:06:32\"}', '[]', 'http://localhost:8000/estados_remitos/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:06:32'),
(459, 1, 'created', 4, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"A confirmar\",\"id\":4}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:06:40'),
(460, 1, 'created', 5, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"Preparado Para Facturar\",\"id\":5}', 'http://localhost:8000/estados_remitos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-08-27 02:06:59');
INSERT INTO `audits` (`id`, `user_id`, `event`, `auditable_id`, `auditable_type`, `old_values`, `new_values`, `url`, `ip_address`, `user_agent`, `created_at`) VALUES
(461, 1, 'created', 28, 'Syspamp\\Producto', '[]', '{\"description\":\"Feldespato\",\"type\":\"11\",\"id\":28}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 01:24:18'),
(462, 1, 'created', 29, 'Syspamp\\Producto', '[]', '{\"description\":\"Molido\",\"type\":\"12\",\"id\":29}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 01:24:26'),
(463, 1, 'updated', 4, 'Syspamp\\StatusInvoice', '{\"description\":\"Pagada\"}', '{\"description\":\"Activa\"}', 'http://localhost:8000/estados_facturas/4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 01:38:59'),
(464, 1, 'deleted', 5, 'Syspamp\\StatusInvoice', '{\"id\":5,\"description\":\"Pendiente de Pago\",\"deleted_at\":\"2017-09-02 22:39:09\"}', '[]', 'http://localhost:8000/estados_facturas/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 01:39:09'),
(465, 1, 'created', 7, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"Finalizada\",\"id\":7}', 'http://localhost:8000/estados_facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 01:39:19'),
(466, 1, 'created', 15, 'Syspamp\\User', '[]', '{\"name\":\"ASDSAD\",\"lastname\":\"ASDASD\",\"email\":\"ASDSAD@HOTMOAIL.COM\",\"password\":\"$2y$10$2iYf\\/4MZ6IeJEtXIwG3DG.7YZzHEQOJXsBxd4snqiFcn7TfUYN9rS\",\"type\":\"6\",\"status\":\"on\",\"id\":15}', 'http://localhost:8000/usuarios', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 02:26:02'),
(467, 1, 'deleted', 15, 'Syspamp\\User', '{\"id\":15,\"name\":\"ASDSAD\",\"lastname\":\"ASDASD\",\"email\":\"ASDSAD@HOTMOAIL.COM\",\"password\":\"$2y$10$2iYf\\/4MZ6IeJEtXIwG3DG.7YZzHEQOJXsBxd4snqiFcn7TfUYN9rS\",\"type\":6,\"status\":\"on\",\"remember_token\":null,\"deleted_at\":\"2017-09-02 23:26:14\"}', '[]', 'http://localhost:8000/usuarios/15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 02:26:14'),
(468, 1, 'created', 8, 'Syspamp\\Invoice', '[]', '{\"id_legal_person\":\"0\",\"id_person\":\"19\",\"invoice_number\":\"4356543\",\"total_amount\":\"54654\",\"id_status_invoice\":\"4\",\"id_payment_method\":\"2\",\"fees\":null,\"parcial_amount\":null,\"administrative_expenses\":null,\"id_product\":\"29\",\"issue_date\":\"2017-09-02\",\"effective_date\":\"2017-09-02\",\"id\":8}', 'http://localhost:8000/facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 03:12:05'),
(469, 1, 'updated', 1, 'Syspamp\\User', '{\"type\":1}', '{\"type\":\"3\"}', 'http://localhost:8000/usuarios/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 20:48:03'),
(470, 1, 'updated', 5, 'Syspamp\\User', '{\"type\":5}', '{\"type\":\"3\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 20:49:08'),
(471, 1, 'updated', 5, 'Syspamp\\User', '{\"type\":3}', '{\"type\":\"5\"}', 'http://localhost:8000/usuarios/5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 20:49:30'),
(472, 1, 'updated', 2, 'Syspamp\\User', '{\"type\":2}', '{\"type\":\"3\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 21:04:00'),
(473, 1, 'updated', 2, 'Syspamp\\User', '{\"type\":3}', '{\"type\":\"2\"}', 'http://localhost:8000/usuarios/2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-03 21:04:02'),
(474, 1, 'created', 3, 'Syspamp\\Machine', '[]', '{\"description\":\"LA NUEVA\",\"id_status_machine\":\"1\",\"id\":3}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:28:15'),
(475, 1, 'created', 6, 'Syspamp\\Machine', '[]', '{\"description\":\"Jaja\",\"id_status_machine\":\"3\",\"id\":6}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:28:33'),
(476, 1, 'created', 7, 'Syspamp\\Machine', '[]', '{\"description\":\"sabee\",\"id_status_machine\":\"1\",\"id\":7}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:29:12'),
(477, 1, 'deleted', 3, 'Syspamp\\Machine', '{\"id\":3,\"description\":\"LA NUEVA\",\"id_status_machine\":1,\"deleted_at\":\"2017-09-03 21:30:40\"}', '[]', 'http://localhost:8000/maquinas/3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:30:40'),
(478, 1, 'deleted', 7, 'Syspamp\\Machine', '{\"id\":7,\"description\":\"sabee\",\"id_status_machine\":1,\"deleted_at\":\"2017-09-03 21:30:45\"}', '[]', 'http://localhost:8000/maquinas/7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:30:45'),
(479, 1, 'created', 30, 'Syspamp\\Producto', '[]', '{\"description\":\"ASD\",\"type\":\"11\",\"id\":30}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:32:42'),
(480, 1, 'created', 31, 'Syspamp\\Producto', '[]', '{\"description\":\"Addsad\",\"type\":\"11\",\"id\":31}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:33:29'),
(481, 1, 'created', 32, 'Syspamp\\Producto', '[]', '{\"description\":\"sdfds\",\"type\":\"11\",\"id\":32}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:34:09'),
(482, 1, 'created', 33, 'Syspamp\\Producto', '[]', '{\"description\":\"sef\",\"type\":\"12\",\"id\":33}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:37:24'),
(483, 1, 'created', 34, 'Syspamp\\Producto', '[]', '{\"description\":\"sdfds\",\"type\":\"12\",\"id\":34}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:37:48'),
(484, 1, 'created', 35, 'Syspamp\\Producto', '[]', '{\"description\":\"asd\",\"type\":\"12\",\"id\":35}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:38:04'),
(485, 1, 'created', 36, 'Syspamp\\Producto', '[]', '{\"description\":\"sdf\",\"type\":\"12\",\"id\":36}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:38:25'),
(486, 1, 'created', 37, 'Syspamp\\Producto', '[]', '{\"description\":\"sdfsdf\",\"type\":\"11\",\"id\":37}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:39:32'),
(487, 1, 'created', 38, 'Syspamp\\Producto', '[]', '{\"description\":\"dsfdsf\",\"type\":\"11\",\"id\":38}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:39:49'),
(488, 1, 'created', 39, 'Syspamp\\Producto', '[]', '{\"description\":\"sdfdsf\",\"type\":\"12\",\"id\":39}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:25'),
(489, 1, 'created', 40, 'Syspamp\\Producto', '[]', '{\"description\":\"sdfds\",\"type\":\"12\",\"id\":40}', 'http://localhost:8000/productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:33'),
(490, 1, 'deleted', 40, 'Syspamp\\Producto', '{\"id\":40,\"description\":\"sdfds\",\"type\":12,\"deleted_at\":\"2017-09-03 21:41:44\"}', '[]', 'http://localhost:8000/productos/40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:44'),
(491, 1, 'deleted', 38, 'Syspamp\\Producto', '{\"id\":38,\"description\":\"dsfdsf\",\"type\":11,\"deleted_at\":\"2017-09-03 21:41:46\"}', '[]', 'http://localhost:8000/productos/38', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:46'),
(492, 1, 'deleted', 37, 'Syspamp\\Producto', '{\"id\":37,\"description\":\"sdfsdf\",\"type\":11,\"deleted_at\":\"2017-09-03 21:41:48\"}', '[]', 'http://localhost:8000/productos/37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:48'),
(493, 1, 'deleted', 36, 'Syspamp\\Producto', '{\"id\":36,\"description\":\"sdf\",\"type\":12,\"deleted_at\":\"2017-09-03 21:41:49\"}', '[]', 'http://localhost:8000/productos/36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:49'),
(494, 1, 'deleted', 35, 'Syspamp\\Producto', '{\"id\":35,\"description\":\"asd\",\"type\":12,\"deleted_at\":\"2017-09-03 21:41:50\"}', '[]', 'http://localhost:8000/productos/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:50'),
(495, 1, 'deleted', 34, 'Syspamp\\Producto', '{\"id\":34,\"description\":\"sdfds\",\"type\":12,\"deleted_at\":\"2017-09-03 21:41:52\"}', '[]', 'http://localhost:8000/productos/34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:52'),
(496, 1, 'deleted', 33, 'Syspamp\\Producto', '{\"id\":33,\"description\":\"sef\",\"type\":12,\"deleted_at\":\"2017-09-03 21:41:53\"}', '[]', 'http://localhost:8000/productos/33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:53'),
(497, 1, 'deleted', 39, 'Syspamp\\Producto', '{\"id\":39,\"description\":\"sdfdsf\",\"type\":12,\"deleted_at\":\"2017-09-03 21:41:55\"}', '[]', 'http://localhost:8000/productos/39', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:55'),
(498, 1, 'deleted', 31, 'Syspamp\\Producto', '{\"id\":31,\"description\":\"Addsad\",\"type\":11,\"deleted_at\":\"2017-09-03 21:41:58\"}', '[]', 'http://localhost:8000/productos/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:41:58'),
(499, 1, 'deleted', 32, 'Syspamp\\Producto', '{\"id\":32,\"description\":\"sdfds\",\"type\":11,\"deleted_at\":\"2017-09-03 21:42:01\"}', '[]', 'http://localhost:8000/productos/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:42:01'),
(500, 1, 'deleted', 30, 'Syspamp\\Producto', '{\"id\":30,\"description\":\"ASD\",\"type\":11,\"deleted_at\":\"2017-09-03 21:42:03\"}', '[]', 'http://localhost:8000/productos/30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:42:03'),
(501, 1, 'created', 7, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"asd\",\"id\":7}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:46:29'),
(502, 1, 'created', 8, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"AHORA SI ?\",\"id\":8}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:50:50'),
(503, 1, 'created', 9, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"NAASD\",\"id\":9}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:51:02'),
(504, 1, 'created', 10, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dfgfdg\",\"id\":10}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:51:34'),
(505, 1, 'created', 11, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dfgd\",\"id\":11}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:51:42'),
(506, 1, 'created', 12, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"hjhjhj\",\"id\":12}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:51:52'),
(507, 1, 'created', 13, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dfgfdg\",\"id\":13}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:53:27'),
(508, 1, 'created', 14, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"fgfsdfds\",\"id\":14}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:53:36'),
(509, 1, 'created', 15, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dsfdsf\",\"id\":15}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:54:43'),
(510, 1, 'created', 16, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dsfdsf\",\"id\":16}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:54:53'),
(511, 1, 'created', 17, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"fghgfh\",\"id\":17}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 00:55:24'),
(512, 1, 'created', 18, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dsfdsf\",\"id\":18}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:02:12'),
(513, 1, 'created', 27, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"fghfgh\",\"id\":27}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:13:26'),
(514, 1, 'created', 28, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"asdasd\",\"id\":28}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:13:57'),
(515, 1, 'created', 29, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"fghfgh\",\"id\":29}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:15:12'),
(516, 1, 'updated', 27, 'Syspamp\\TypeProduct', '{\"description\":\"fghfgh\"}', '{\"description\":\"ahi ta ?\"}', 'http://localhost:8000/tipo_productos/27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:17:49'),
(517, 1, 'deleted', 27, 'Syspamp\\TypeProduct', '{\"id\":27,\"description\":\"ahi ta ?\",\"deleted_at\":\"2017-09-03 22:17:54\"}', '[]', 'http://localhost:8000/tipo_productos/27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:17:54'),
(518, 1, 'deleted', 28, 'Syspamp\\TypeProduct', '{\"id\":28,\"description\":\"asdasd\",\"deleted_at\":\"2017-09-03 22:17:56\"}', '[]', 'http://localhost:8000/tipo_productos/28', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:17:56'),
(519, 1, 'deleted', 29, 'Syspamp\\TypeProduct', '{\"id\":29,\"description\":\"fghfgh\",\"deleted_at\":\"2017-09-03 22:18:33\"}', '[]', 'http://localhost:8000/tipo_productos/29', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:18:33'),
(520, 1, 'created', 30, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"DALE\",\"id\":30}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:18:36'),
(521, 1, 'updated', 30, 'Syspamp\\TypeProduct', '{\"description\":\"DALE\"}', '{\"description\":\"bueno bldo\"}', 'http://localhost:8000/tipo_productos/30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:18:48'),
(522, 1, 'deleted', 30, 'Syspamp\\TypeProduct', '{\"id\":30,\"description\":\"bueno bldo\",\"deleted_at\":\"2017-09-03 22:19:41\"}', '[]', 'http://localhost:8000/tipo_productos/30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:19:41'),
(523, 1, 'created', 31, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"SALEE\",\"id\":31}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:20:51'),
(524, 1, 'deleted', 31, 'Syspamp\\TypeProduct', '{\"id\":31,\"description\":\"SALEE\",\"deleted_at\":\"2017-09-03 22:22:21\"}', '[]', 'http://localhost:8000/tipo_productos/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:22:21'),
(525, 1, 'created', 32, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"dfgfdg\",\"id\":32}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:25:24'),
(526, 1, 'deleted', 32, 'Syspamp\\TypeProduct', '{\"id\":32,\"description\":\"dfgfdg\",\"deleted_at\":\"2017-09-03 22:26:55\"}', '[]', 'http://localhost:8000/tipo_productos/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:26:55'),
(527, 1, 'created', 33, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"dsfdsf\",\"id\":33}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:30:46'),
(528, 1, 'created', 34, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"sdfdsf\",\"id\":34}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:30:50'),
(529, 1, 'created', 35, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"sdfdsf\",\"id\":35}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:30:53'),
(530, 1, 'created', 36, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"dsfdsf\",\"id\":36}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:30:57'),
(531, 1, 'created', 37, 'Syspamp\\TypeProduct', '[]', '{\"description\":\"sfsdfds\",\"id\":37}', 'http://localhost:8000/tipo_productos', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:32:29'),
(532, 1, 'deleted', 37, 'Syspamp\\TypeProduct', '{\"id\":37,\"description\":\"sfsdfds\",\"deleted_at\":\"2017-09-03 22:32:44\"}', '[]', 'http://localhost:8000/tipo_productos/37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:32:44'),
(533, 1, 'deleted', 33, 'Syspamp\\TypeProduct', '{\"id\":33,\"description\":\"dsfdsf\",\"deleted_at\":\"2017-09-03 22:32:48\"}', '[]', 'http://localhost:8000/tipo_productos/33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:32:48'),
(534, 1, 'deleted', 36, 'Syspamp\\TypeProduct', '{\"id\":36,\"description\":\"dsfdsf\",\"deleted_at\":\"2017-09-03 22:32:52\"}', '[]', 'http://localhost:8000/tipo_productos/36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:32:52'),
(535, 1, 'deleted', 34, 'Syspamp\\TypeProduct', '{\"id\":34,\"description\":\"sdfdsf\",\"deleted_at\":\"2017-09-03 22:32:56\"}', '[]', 'http://localhost:8000/tipo_productos/34', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:32:56'),
(536, 1, 'deleted', 35, 'Syspamp\\TypeProduct', '{\"id\":35,\"description\":\"sdfdsf\",\"deleted_at\":\"2017-09-03 22:32:58\"}', '[]', 'http://localhost:8000/tipo_productos/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-04 01:32:58'),
(537, 1, 'updated', 31, 'Syspamp\\Person', '{\"name\":\"ASDasdasdasdsa\",\"lastname\":\"asdasd\",\"email\":\"sadsa@asdsad\",\"cuit\":\"3245234\",\"id_condicion_iva\":2,\"id_type_people\":3,\"id_city\":8,\"floor\":\"45\",\"department\":\"45\",\"phone_1\":\"64645\",\"phone_2\":\"45\"}', '{\"name\":\"No\",\"lastname\":\"Posee\",\"email\":\"admin@syspamp.com\",\"cuit\":\"123456789\",\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id_city\":\"1\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null}', 'http://localhost:8000/personas/31', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:38:25'),
(538, 1, 'updated', 1, 'Syspamp\\LegalPerson', '{\"name\":\"SDFDSF\",\"cuit\":\"345435\",\"id_city\":2,\"adress\":\"SDFDSF\",\"url\":\"SDFDSF\",\"phone_1\":\"SDFDS34534534\",\"phone_2\":\"34534\"}', '{\"name\":\"No Posee\",\"cuit\":\"-\",\"id_city\":\"1\",\"adress\":null,\"url\":null,\"phone_1\":null,\"phone_2\":null}', 'http://localhost:8000/empresas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:39:48'),
(539, 1, 'updated', 1, 'Syspamp\\Person', '{\"cuit\":\"123456789\"}', '{\"cuit\":\"-\"}', 'http://localhost:8000/personas/1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:39:57'),
(540, 1, 'created', 9, 'Syspamp\\Invoice', '[]', '{\"id_legal_person\":\"2\",\"id_person\":\"1\",\"invoice_number\":\"234234\",\"total_amount\":\"435345\",\"id_status_invoice\":\"6\",\"id_payment_method\":\"2\",\"fees\":null,\"parcial_amount\":null,\"administrative_expenses\":null,\"id_product\":\"28\",\"issue_date\":\"2017-09-05\",\"effective_date\":\"2017-09-05\",\"id\":9}', 'http://localhost:8000/facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:45:31'),
(541, 1, 'deleted', 16, 'Syspamp\\Person', '{\"id\":16,\"name\":\"sdfsdf\",\"lastname\":\"dsfds\",\"email\":\"dsfdsfsdfsd435@asdfasd\",\"cuit\":\"dfdsf35\",\"id_condicion_iva\":1,\"id_type_people\":1,\"id_city\":1,\"adress\":\"dsfdsf\",\"floor\":\"45435\",\"department\":\"435435\",\"phone_1\":null,\"phone_2\":null,\"phone_3\":\"345\",\"deleted_at\":\"2017-09-06 02:51:37\"}', '[]', 'http://localhost:8000/personas/16', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:51:37'),
(542, 1, 'deleted', 19, 'Syspamp\\Person', '{\"id\":19,\"name\":\"dsfdsf\",\"lastname\":\"dsfds\",\"email\":\"dsfdsdsaf@asdsad\",\"cuit\":\"435435\",\"id_condicion_iva\":1,\"id_type_people\":2,\"id_city\":1,\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-09-06 02:51:45\"}', '[]', 'http://localhost:8000/personas/19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:51:45'),
(543, 1, 'deleted', 32, 'Syspamp\\Person', '{\"id\":32,\"name\":\"asdsad\",\"lastname\":\"asdsa\",\"email\":\"asdas@asdsad\",\"cuit\":\"w4w5435\",\"id_condicion_iva\":1,\"id_type_people\":2,\"id_city\":1,\"adress\":\"sad234\",\"floor\":\"543\",\"department\":null,\"phone_1\":null,\"phone_2\":\"345\",\"phone_3\":\"345\",\"deleted_at\":\"2017-09-06 02:52:10\"}', '[]', 'http://localhost:8000/personas/32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:52:10'),
(544, 1, 'deleted', 35, 'Syspamp\\Person', '{\"id\":35,\"name\":\"JAJAJ\",\"lastname\":\"DALE\",\"email\":\"as@xn--asdsa-pja\",\"cuit\":\"235345\",\"id_condicion_iva\":1,\"id_type_people\":2,\"id_city\":8,\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":\"345\",\"phone_3\":\"345\",\"deleted_at\":\"2017-09-06 02:52:12\"}', '[]', 'http://localhost:8000/personas/35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:52:12'),
(545, 1, 'created', 41, 'Syspamp\\Person', '[]', '{\"name\":\"Santiago\",\"lastname\":\"Pujol\",\"email\":\"santiagopujol92@gmail.com\",\"cuit\":\"36354845\",\"adress\":\"Chascomus 1782\",\"floor\":null,\"department\":null,\"phone_1\":\"4643997\",\"phone_2\":\"152071376\",\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id_city\":\"1\",\"id\":41}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:52:48'),
(546, 1, 'created', 5, 'Syspamp\\LegalPerson', '[]', '{\"name\":\"asdsad\",\"cuit\":\"dsfdsf\",\"url\":\"dsfdsf\",\"adress\":null,\"phone_1\":null,\"phone_2\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id_city\":\"8\",\"id\":5}', 'http://localhost:8000/empresas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 05:53:13'),
(547, 1, 'created', 10, 'Syspamp\\Invoice', '[]', '{\"id_legal_person\":\"1\",\"id_person\":\"41\",\"invoice_number\":\"435345\",\"total_amount\":\"345345\",\"id_status_invoice\":\"4\",\"id_payment_method\":\"2\",\"fees\":null,\"parcial_amount\":null,\"administrative_expenses\":null,\"id_product\":\"28\",\"issue_date\":\"2017-09-06\",\"effective_date\":\"2017-09-06\",\"id\":10}', 'http://localhost:8000/facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 06:23:34'),
(548, 1, 'created', 8, 'Syspamp\\Machine', '[]', '{\"description\":\"asdsad\",\"id_status_machine\":\"1\",\"id\":8}', 'http://localhost:8000/maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 06:24:43'),
(549, 1, 'deleted', 8, 'Syspamp\\Machine', '{\"id\":8,\"description\":\"asdsad\",\"id_status_machine\":1,\"deleted_at\":\"2017-09-06 03:25:14\"}', '[]', 'http://localhost:8000/maquinas/8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 06:25:14'),
(550, 1, 'created', 19, 'Syspamp\\MachineStatus', '[]', '{\"description\":\"dsfdsf\",\"id\":19}', 'http://localhost:8000/estados_maquinas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 06:25:25'),
(551, 1, 'deleted', 9, 'Syspamp\\MachineStatus', '{\"id\":9,\"description\":\"NAASD\",\"deleted_at\":\"2017-09-06 03:25:29\"}', '[]', 'http://localhost:8000/estados_maquinas/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 06:25:29'),
(552, 1, 'created', 42, 'Syspamp\\Person', '[]', '{\"name\":\"TEST\",\"lastname\":\"TEST\",\"email\":\"TEST2.@2ERA\",\"cuit\":\"235324\",\"adress\":\"TEST\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"2\",\"id_city\":\"2\",\"id\":42}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 07:08:40'),
(553, 1, 'created', 43, 'Syspamp\\Person', '[]', '{\"name\":\"TEST23\",\"lastname\":\"TEST34\",\"email\":\"TEST2.@2ERA214234sadsad\",\"cuit\":\"435345\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"2\",\"id_type_people\":\"3\",\"id_city\":\"10\",\"id\":43}', 'http://localhost:8000/personas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 07:08:55'),
(554, 1, 'created', 11, 'Syspamp\\Invoice', '[]', '{\"id_legal_person\":\"2\",\"id_person\":\"1\",\"invoice_number\":\"34534\",\"total_amount\":\"345435\",\"id_status_invoice\":\"4\",\"id_payment_method\":\"3\",\"fees\":null,\"parcial_amount\":null,\"administrative_expenses\":null,\"id_product\":\"28\",\"issue_date\":\"2017-09-06\",\"effective_date\":\"2017-09-06\",\"id\":11}', 'http://localhost:8000/facturas', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 08:36:34'),
(555, 1, 'deleted', 9, 'Syspamp\\Invoice', '{\"id\":9,\"invoice_number\":\"234234\",\"id_person\":1,\"id_legal_person\":2,\"total_amount\":435345,\"parcial_amount\":null,\"fees\":null,\"administrative_expenses\":null,\"id_payment_method\":2,\"id_product\":28,\"id_status_invoice\":6,\"issue_date\":\"2017-09-05\",\"effective_date\":\"2017-09-05\",\"payment_date\":null,\"paid\":\"0\",\"deleted_at\":\"2017-09-06 05:40:17\"}', '[]', 'http://localhost:8000/facturas/9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-06 08:40:17'),
(556, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"m6kQCOp8nyynRH13AZx4mxfqWVBJRDFiISCMNlwQZ5NEfGqDBgUoVZafLrzl\"}', '{\"remember_token\":\"B7FZUko1tZBmSv9E6YmXqPVgRvXaMUAtTogJlOWNwtswVUTbsZ4VFP67NJCs\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-07 13:18:04'),
(557, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"B7FZUko1tZBmSv9E6YmXqPVgRvXaMUAtTogJlOWNwtswVUTbsZ4VFP67NJCs\"}', '{\"remember_token\":\"rn4LSQXKjMok4j0CrIt1ynnqqzg972aFESf444bPrtNdH0KCafdt5BsmMur5\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-07 13:19:54'),
(558, 1, 'deleted', 42, 'Syspamp\\Person', '{\"id\":42,\"name\":\"TEST\",\"lastname\":\"TEST\",\"email\":\"TEST2.@2ERA\",\"cuit\":\"235324\",\"id_condicion_iva\":1,\"id_type_people\":2,\"id_city\":2,\"adress\":\"TEST\",\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-09-07 13:45:25\"}', '[]', 'http://drinky.org/syspamp/public/index.php/personas/42', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-07 13:45:25'),
(559, 1, 'deleted', 43, 'Syspamp\\Person', '{\"id\":43,\"name\":\"TEST23\",\"lastname\":\"TEST34\",\"email\":\"TEST2.@2ERA214234sadsad\",\"cuit\":\"435345\",\"id_condicion_iva\":2,\"id_type_people\":3,\"id_city\":10,\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"deleted_at\":\"2017-09-07 13:45:39\"}', '[]', 'http://drinky.org/syspamp/public/index.php/personas/43', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-07 13:45:39'),
(560, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"rn4LSQXKjMok4j0CrIt1ynnqqzg972aFESf444bPrtNdH0KCafdt5BsmMur5\"}', '{\"remember_token\":\"v8LFCk5tb6Tkg04sBTP1ajnK5UEBQo1KeBUZ012Nj8epc4YEoxaa7JjqEfUQ\"}', 'http://www.drinky.org/syspamp/public/index.php/logout', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:46:35'),
(561, 1, 'created', 5, 'Syspamp\\TypePerson', '[]', '{\"description\":\"TEST\",\"id\":5}', 'http://www.drinky.org/syspamp/public/index.php/tipo_personas', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:49:04'),
(562, 1, 'created', 6, 'Syspamp\\TypePerson', '[]', '{\"description\":\"TEST2\",\"id\":6}', 'http://www.drinky.org/syspamp/public/index.php/tipo_personas', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:49:18'),
(563, 1, 'deleted', 6, 'Syspamp\\TypePerson', '{\"id\":6,\"description\":\"TEST2\",\"deleted_at\":\"2017-09-07 14:50:06\"}', '[]', 'http://www.drinky.org/syspamp/public/index.php/tipo_personas/6', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:50:06'),
(564, 1, 'deleted', 5, 'Syspamp\\TypePerson', '{\"id\":5,\"description\":\"TEST\",\"deleted_at\":\"2017-09-07 14:50:12\"}', '[]', 'http://www.drinky.org/syspamp/public/index.php/tipo_personas/5', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:50:12'),
(565, 1, 'created', 7, 'Syspamp\\TypePerson', '[]', '{\"description\":\"TEST2\",\"id\":7}', 'http://www.drinky.org/syspamp/public/index.php/tipo_personas', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:50:24'),
(566, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"v8LFCk5tb6Tkg04sBTP1ajnK5UEBQo1KeBUZ012Nj8epc4YEoxaa7JjqEfUQ\"}', '{\"remember_token\":\"TrvIk02Z9Q9YKaisXpdFeGYvCuEnyUmTOQCEdHVFCYWpKSsXr7uQ9z8Z36Op\"}', 'http://www.drinky.org/syspamp/public/index.php/logout', '186.139.96.162', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-07 14:50:31'),
(567, 1, 'created', 8, 'Syspamp\\StatusInvoice', '[]', '{\"description\":\"Anulado\",\"id\":8}', 'http://drinky.org/syspamp/public/index.php/estados_facturas', '170.51.27.86', 'Mozilla/5.0 (Linux; Android 4.4.4; SM-G530M Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-07 17:21:45'),
(568, 1, 'deleted', 8, 'Syspamp\\StatusInvoice', '{\"id\":8,\"description\":\"Anulado\",\"deleted_at\":\"2017-09-07 17:22:02\"}', '[]', 'http://drinky.org/syspamp/public/index.php/estados_facturas/8', '66.249.83.83', 'Mozilla/5.0 (Linux; Android 4.4.4; SM-G530M Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-07 17:22:02'),
(569, 1, 'deleted', 41, 'Syspamp\\Person', '{\"id\":41,\"name\":\"Santiago\",\"lastname\":\"Pujol\",\"email\":\"santiagopujol92@gmail.com\",\"cuit\":\"36354845\",\"id_condicion_iva\":1,\"id_type_people\":1,\"id_city\":1,\"adress\":\"Chascomus 1782\",\"floor\":null,\"department\":null,\"phone_1\":\"4643997\",\"phone_2\":\"152071376\",\"phone_3\":null,\"deleted_at\":\"2017-09-07 17:23:29\"}', '[]', 'http://drinky.org/syspamp/public/index.php/personas/41', '66.249.83.84', 'Mozilla/5.0 (Linux; Android 4.4.4; SM-G530M Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-07 17:23:29'),
(570, 1, 'created', 6, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"tesT\",\"id\":6}', 'http://drinky.org/syspamp/public/index.php/estados_remitos', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-08 02:23:53'),
(571, 1, 'deleted', 6, 'Syspamp\\StatusShipment', '{\"id\":6,\"description\":\"tesT\",\"deleted_at\":\"2017-09-08 02:24:01\"}', '[]', 'http://drinky.org/syspamp/public/index.php/estados_remitos/6', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-08 02:24:01'),
(572, 1, 'created', 7, 'Syspamp\\StatusShipment', '[]', '{\"description\":\"DALE\",\"id\":7}', 'http://drinky.org/syspamp/public/index.php/estados_remitos', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-08 02:24:06'),
(573, 1, 'updated', 7, 'Syspamp\\StatusShipment', '{\"description\":\"DALE\"}', '{\"description\":\"EDALEER\"}', 'http://drinky.org/syspamp/public/index.php/estados_remitos/7', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-08 02:24:12'),
(574, 1, 'deleted', 7, 'Syspamp\\StatusShipment', '{\"id\":7,\"description\":\"EDALEER\",\"deleted_at\":\"2017-09-08 02:24:15\"}', '[]', 'http://drinky.org/syspamp/public/index.php/estados_remitos/7', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-08 02:24:15'),
(575, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"TrvIk02Z9Q9YKaisXpdFeGYvCuEnyUmTOQCEdHVFCYWpKSsXr7uQ9z8Z36Op\"}', '{\"remember_token\":\"5mq55MgYUp58Vmj2rUf4CIotLAqHk6vGYWHoLWdQnu2CJZJ08zomgL0dazld\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-08 02:33:25'),
(576, 1, 'updated', 6, 'Syspamp\\Machine', '{\"id_status_machine\":3}', '{\"id_status_machine\":\"2\"}', 'http://drinky.org/syspamp/public/index.php/maquinas/6', '66.249.83.82', 'Mozilla/5.0 (Linux; Android 4.4.4; SM-G530M Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-08 13:38:36'),
(577, 1, 'deleted', 6, 'Syspamp\\Machine', '{\"id\":6,\"description\":\"Jaja\",\"id_status_machine\":2,\"deleted_at\":\"2017-09-08 13:38:43\"}', '[]', 'http://drinky.org/syspamp/public/index.php/maquinas/6', '66.249.83.83', 'Mozilla/5.0 (Linux; Android 4.4.4; SM-G530M Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-08 13:38:43'),
(578, 1, 'updated', 7, 'Syspamp\\TypePerson', '{\"description\":\"TEST2\"}', '{\"description\":\"GERENTE\"}', 'http://drinky.org/syspamp/public/index.php/tipo_personas/7', '181.95.72.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2017-09-09 02:45:57'),
(579, 1, 'created', 8, 'Syspamp\\Province', '[]', '{\"id_country\":\"1\",\"description\":\"SANTA FE\",\"id\":8}', 'http://drinky.org/syspamp/public/index.php/provincias', '181.95.72.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2017-09-09 02:59:00'),
(580, 1, 'created', 14, 'Syspamp\\City', '[]', '{\"id_province\":\"8\",\"description\":\"RAFAELA\",\"id\":14}', 'http://drinky.org/syspamp/public/index.php/ciudades', '181.95.72.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2017-09-09 03:02:05'),
(581, 1, 'created', 6, 'Syspamp\\LegalPerson', '[]', '{\"name\":\"JUAN PEREZ\",\"cuit\":\"20282828523\",\"url\":null,\"adress\":\"XXXXXXXXXX\",\"phone_1\":\"15000000\",\"phone_2\":\"1525435\",\"id_condicion_iva\":\"2\",\"id_type_people\":\"1\",\"id_city\":\"14\",\"id\":6}', 'http://drinky.org/syspamp/public/index.php/empresas', '181.95.72.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2017-09-09 03:03:36'),
(582, 1, 'created', 12, 'Syspamp\\Invoice', '[]', '{\"id_legal_person\":\"6\",\"id_person\":\"1\",\"invoice_number\":\"35665\",\"total_amount\":\"1525252\",\"id_status_invoice\":\"4\",\"id_payment_method\":\"4\",\"fees\":null,\"parcial_amount\":null,\"administrative_expenses\":null,\"id_product\":\"28\",\"issue_date\":\"2017-09-09\",\"effective_date\":\"2017-09-10\",\"id\":12}', 'http://drinky.org/syspamp/public/index.php/facturas', '181.95.72.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', '2017-09-09 03:08:32'),
(583, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"5mq55MgYUp58Vmj2rUf4CIotLAqHk6vGYWHoLWdQnu2CJZJ08zomgL0dazld\"}', '{\"remember_token\":\"FnDmxdBI4B9lkAbKgCUzHy9bNQAeDnAknBI7Xv7GiCPsDwqyFtyJ1kj35CoQ\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-10 19:30:08'),
(584, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"FnDmxdBI4B9lkAbKgCUzHy9bNQAeDnAknBI7Xv7GiCPsDwqyFtyJ1kj35CoQ\"}', '{\"remember_token\":\"2rsMrACm0TSL5q5jkOpvm7IjldjPia1Mq1TI3NCgX1HlpiYxgaQmNQ1wBegR\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-10 19:31:38'),
(585, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"2rsMrACm0TSL5q5jkOpvm7IjldjPia1Mq1TI3NCgX1HlpiYxgaQmNQ1wBegR\"}', '{\"remember_token\":\"eaJryymmXLVl157aleIhTMPew6E3KGZPIphjFsF2YTq9IF7eqrxhpEMjs5bD\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-09-10 19:31:50'),
(586, 1, 'created', 44, 'Syspamp\\Person', '[]', '{\"name\":\"KM\",\"lastname\":\"Jn\",\"email\":\"km@pg1.xn--cw-zja\",\"cuit\":\"Bd\",\"adress\":null,\"floor\":null,\"department\":null,\"phone_1\":null,\"phone_2\":null,\"phone_3\":null,\"id_condicion_iva\":\"1\",\"id_type_people\":\"1\",\"id_city\":\"14\",\"id\":44}', 'http://drinky.org/syspamp/public/index.php/personas', '181.95.72.121', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-J700M Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-13 01:56:48'),
(587, 1, 'updated', 2, 'Syspamp\\User', '{\"type\":2}', '{\"type\":\"3\"}', 'http://drinky.org/syspamp/public/index.php/usuarios/2', '181.95.72.121', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-J700M Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-13 02:00:06'),
(588, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"eaJryymmXLVl157aleIhTMPew6E3KGZPIphjFsF2YTq9IF7eqrxhpEMjs5bD\"}', '{\"remember_token\":\"QownwYp5CCacuUEi9PmCiy1tII5ZIYMtGFytkz9Sjg4DONfH1b0vCKI5nhsH\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-J700M Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-13 02:00:18'),
(589, 3, 'updated', 3, 'Syspamp\\User', '{\"remember_token\":\"6LtFSqb8bLdact1Xp0LLaHwvs6grP0IM3k5HjtatnTixKLKHiwosmWdvm5YR\"}', '{\"remember_token\":\"3mUOyZTiHvjhl9ETMEw2kG1Y6iELkFH7SUY9fJMTtrcmMiMlQR1MQZfuE0zi\"}', 'http://drinky.org/syspamp/public/index.php/logout', '181.95.72.121', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-J700M Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36', '2017-09-13 02:02:49'),
(590, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"QownwYp5CCacuUEi9PmCiy1tII5ZIYMtGFytkz9Sjg4DONfH1b0vCKI5nhsH\"}', '{\"remember_token\":\"pdKMhpYU2jqRZGrA64fvaonY5suV5KVE1ECFC87vYwemvcfr4ehlZMZKlkZB\"}', 'http://www.drinky.org/syspamp/public/index.php/logout', '181.167.163.63', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-09-28 16:15:30'),
(591, 1, 'updated', 1, 'Syspamp\\User', '{\"remember_token\":\"pdKMhpYU2jqRZGrA64fvaonY5suV5KVE1ECFC87vYwemvcfr4ehlZMZKlkZB\"}', '{\"remember_token\":\"2izHTqwC0Nyh6iQRCNSwUXFDQisiwwVkUvdXLt9yjfL8HgWcZANNGnHMb1Iy\"}', 'http://www.drinky.org/syspamp/public/index.php/logout', '181.167.163.63', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', '2017-10-10 21:14:00'),
(592, 1, 'created', 41, 'Syspamp\\Producto', '[]', '{\"description\":\"Tst\",\"type\":\"11\",\"id\":41}', 'http://drinky.org/syspamp/public/index.php/productos', '186.108.173.161', 'Mozilla/5.0 (Linux; Android 7.0; Moto G (5) Build/NPP25.137-76; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/62.0.3202.84 Mobile Safari/537.36', '2017-11-08 03:48:29'),
(593, 1, 'created', 5, 'Syspamp\\Country', '[]', '{\"description\":\"TESt\",\"id\":5}', 'http://drinky.org/syspamp/public/index.php/paises', '186.108.173.161', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-11-08 18:28:24'),
(594, 1, 'deleted', 5, 'Syspamp\\Country', '{\"id\":5,\"description\":\"TESt\",\"deleted_at\":\"2017-11-08 18:28:28\"}', '[]', 'http://drinky.org/syspamp/public/index.php/paises/5', '186.108.173.161', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', '2017-11-08 18:28:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_province` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `description`, `id_province`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Córdoba', 1, '2017-08-21 09:09:37', '2017-08-21 09:11:54', NULL),
(2, 'Alta Gracia', 1, '2017-08-21 09:10:41', '2017-08-21 09:10:41', NULL),
(3, 'TEST', 2, '2017-08-21 09:26:29', '2017-08-21 09:26:34', '2017-08-21 09:26:34'),
(4, 'TESt', 2, '2017-08-21 09:26:46', '2017-08-22 06:57:46', '2017-08-22 06:57:46'),
(6, 'JAJA', 2, '2017-08-21 09:32:11', '2017-08-22 03:55:02', '2017-08-22 03:55:02'),
(8, 'Monte Cristo', 3, '2017-08-22 06:44:18', '2017-08-22 08:10:57', NULL),
(9, 'JAJA', 2, '2017-08-22 06:59:13', '2017-08-22 06:59:16', '2017-08-22 06:59:16'),
(10, 'Mono', 2, '2017-08-22 07:54:11', '2017-08-22 07:54:11', NULL),
(12, 'teST', 7, '2017-08-26 22:12:48', '2017-08-26 22:13:00', '2017-08-26 22:13:00'),
(13, 'asdas', 7, '2017-08-26 22:13:09', '2017-08-26 22:13:20', '2017-08-26 22:13:20'),
(14, 'RAFAELA', 8, '2017-09-09 03:02:05', '2017-09-09 03:02:05', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion_ivas`
--

CREATE TABLE `condicion_ivas` (
  `id` int(11) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `condicion_ivas`
--

INSERT INTO `condicion_ivas` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Monotributo', '2017-08-02 06:13:04', '2017-08-26 22:06:15', NULL),
(2, 'Responsable Inscripto', '2017-08-02 06:13:15', '2017-08-02 06:13:15', NULL),
(3, 'asdsad', '2017-08-09 07:37:11', '2017-08-09 08:13:52', '2017-08-09 08:13:52'),
(4, 'na', '2017-08-21 04:42:18', '2017-08-21 04:42:20', '2017-08-21 04:42:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Argentina', '2017-08-21 08:19:03', '2017-08-21 08:21:15', NULL),
(2, 'Brazil', '2017-08-21 08:19:31', '2017-08-21 08:19:31', NULL),
(3, 'Venezuela', '2017-08-21 08:19:33', '2017-08-21 08:19:33', NULL),
(4, 'Bolivia', '2017-08-21 08:19:35', '2017-08-21 08:19:35', NULL),
(5, 'TESt', '2017-11-08 18:28:24', '2017-11-08 18:28:28', '2017-11-08 18:28:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoice_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_person` int(10) UNSIGNED DEFAULT NULL,
  `id_legal_person` int(10) UNSIGNED DEFAULT NULL,
  `total_amount` double(12,2) DEFAULT NULL,
  `parcial_amount` double(12,2) DEFAULT NULL,
  `fees` int(11) DEFAULT NULL,
  `administrative_expenses` double(12,2) DEFAULT NULL,
  `id_payment_method` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_status_invoice` int(10) UNSIGNED NOT NULL,
  `issue_date` date NOT NULL,
  `effective_date` date NOT NULL,
  `payment_date` date DEFAULT NULL,
  `paid` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_number`, `id_person`, `id_legal_person`, `total_amount`, `parcial_amount`, `fees`, `administrative_expenses`, `id_payment_method`, `id_product`, `id_status_invoice`, `issue_date`, `effective_date`, `payment_date`, `paid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, '4356543', 19, 1, 54654.00, NULL, NULL, NULL, 2, 29, 4, '2017-09-02', '2017-09-02', NULL, '0', '2017-09-03 03:12:05', '2017-09-03 03:12:05', NULL),
(9, '234234', 1, 2, 435345.00, NULL, NULL, NULL, 2, 28, 6, '2017-09-05', '2017-09-05', NULL, '0', '2017-09-06 05:45:31', '2017-09-06 08:40:17', '2017-09-06 08:40:17'),
(10, '435345', 41, 1, 345345.00, NULL, NULL, NULL, 2, 28, 4, '2017-09-06', '2017-09-06', NULL, '0', '2017-09-06 06:23:34', '2017-09-06 06:23:34', NULL),
(11, '34534', 1, 2, 345435.00, NULL, NULL, NULL, 3, 28, 4, '2017-09-06', '2017-09-06', NULL, '0', '2017-09-06 08:36:34', '2017-09-06 08:36:34', NULL),
(12, '35665', 1, 6, 1525252.00, NULL, NULL, NULL, 4, 28, 4, '2017-09-09', '2017-09-10', NULL, '0', '2017-09-09 03:08:32', '2017-09-09 03:08:32', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `legal_people`
--

CREATE TABLE `legal_people` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuit` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_condicion_iva` int(10) UNSIGNED NOT NULL,
  `id_type_people` int(10) UNSIGNED NOT NULL,
  `id_city` int(10) UNSIGNED NOT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `legal_people`
--

INSERT INTO `legal_people` (`id`, `name`, `cuit`, `id_condicion_iva`, `id_type_people`, `id_city`, `adress`, `url`, `phone_1`, `phone_2`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Empresa No Asignada', '', 1, 1, 1, NULL, NULL, NULL, NULL, '2017-08-26 21:42:54', '2017-09-06 05:39:47', NULL),
(2, 'fdgdf', 'fdg', 1, 2, 1, 'dfgfdg', 'dfgdfg', 'dfgdfg', 'dfgfdg', '2017-08-26 21:43:37', '2017-08-26 23:08:08', NULL),
(3, 'TEST', '32234', 1, 2, 10, 'DASFAD', 'ASDSAD', '345435', '345435', '2017-08-26 22:50:45', '2017-08-26 22:50:45', NULL),
(4, 'TEST', 'TEST', 2, 1, 8, 'ASDSAD', 'ASDFSA', '435345', '435', '2017-08-26 23:09:35', '2017-08-26 23:13:45', NULL),
(5, 'asdsad', 'dsfdsf', 1, 1, 8, NULL, 'dsfdsf', NULL, NULL, '2017-09-06 05:53:13', '2017-09-06 05:53:13', NULL),
(6, 'JUAN PEREZ', '20282828523', 2, 1, 14, 'XXXXXXXXXX', NULL, '15000000', '1525435', '2017-09-09 03:03:35', '2017-09-09 03:03:35', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `machines`
--

CREATE TABLE `machines` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_status_machine` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `machines`
--

INSERT INTO `machines` (`id`, `description`, `id_status_machine`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Molino', 2, '2017-08-21 01:49:30', '2017-08-21 06:16:02', NULL),
(2, 'Molino 2', 3, '2017-08-21 02:00:06', '2017-08-21 07:44:47', NULL),
(3, 'LA NUEVA', 1, '2017-09-04 00:28:15', '2017-09-04 00:30:40', '2017-09-04 00:30:40'),
(6, 'Jaja', 2, '2017-09-04 00:28:33', '2017-09-08 13:38:43', '2017-09-08 13:38:43'),
(7, 'sabee', 1, '2017-09-04 00:29:12', '2017-09-04 00:30:45', '2017-09-04 00:30:45'),
(8, 'asdsad', 1, '2017-09-06 06:24:43', '2017-09-06 06:25:14', '2017-09-06 06:25:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `machine_statuses`
--

CREATE TABLE `machine_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `machine_statuses`
--

INSERT INTO `machine_statuses` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'En Reparación', '2017-08-21 01:30:29', '2017-08-21 01:30:57', NULL),
(2, 'Activa', '2017-08-21 01:30:43', '2017-08-21 01:30:43', NULL),
(3, 'Inactivo', '2017-08-21 01:30:47', '2017-08-21 01:30:47', NULL),
(4, 'JAJa', '2017-08-21 01:31:00', '2017-08-21 01:31:33', '2017-08-21 01:31:33'),
(5, 'asd', '2017-08-27 02:02:00', '2017-08-27 02:02:20', '2017-08-27 02:02:20'),
(6, 'asdsad', '2017-08-27 02:02:09', '2017-08-27 02:02:22', '2017-08-27 02:02:22'),
(7, 'asd', '2017-09-04 00:46:29', '2017-09-04 00:46:29', NULL),
(8, 'AHORA SI ?', '2017-09-04 00:50:49', '2017-09-04 00:50:49', NULL),
(9, 'NAASD', '2017-09-04 00:51:02', '2017-09-06 06:25:29', '2017-09-06 06:25:29'),
(10, 'dfgfdg', '2017-09-04 00:51:34', '2017-09-04 00:51:34', NULL),
(11, 'dfgd', '2017-09-04 00:51:42', '2017-09-04 00:51:42', NULL),
(12, 'hjhjhj', '2017-09-04 00:51:52', '2017-09-04 00:51:52', NULL),
(13, 'dfgfdg', '2017-09-04 00:53:27', '2017-09-04 00:53:27', NULL),
(14, 'fgfsdfds', '2017-09-04 00:53:36', '2017-09-04 00:53:36', NULL),
(15, 'dsfdsf', '2017-09-04 00:54:43', '2017-09-04 00:54:43', NULL),
(16, 'dsfdsf', '2017-09-04 00:54:53', '2017-09-04 00:54:53', NULL),
(17, 'fghgfh', '2017-09-04 00:55:24', '2017-09-04 00:55:24', NULL),
(18, 'dsfdsf', '2017-09-04 01:02:12', '2017-09-04 01:02:12', NULL),
(19, 'dsfdsf', '2017-09-06 06:25:25', '2017-09-06 06:25:25', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_04_04_000000_create_type_users_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password__resets_table', 1),
(4, '2017_03_26_190800_create_type_documents_table', 1),
(5, '2017_03_26_191358_create_clients_table', 1),
(6, '2017_03_30_002218_create_products_table', 1),
(7, '2017_04_14_030442_add_deleted_to_users_table', 1),
(8, '2017_05_22_001637_create_modules_table', 2),
(9, '2017_05_23_001637_create_modules_table', 3),
(10, '2017_05_22_011531_create_modulos_table', 4),
(11, '2017_05_22_012531_create_modulos_table', 5),
(12, '2017_05_24_013531_create_modulos_table', 6),
(13, '2017_07_03_001118_create_type_products_table', 7),
(14, '2017_07_03_002218_create_type_products_table', 8),
(15, '2017_07_03_111111_create_clients_table', 9),
(16, '2017_07_03_000000_create_type_products_table', 10),
(17, '2017_07_03_111111_create_productos_table', 10),
(18, '2017_07_03_000001_create_type_products_table', 11),
(19, '2017_07_15_000001_create_type_products_table', 12),
(20, '2017_07_15_000222_create_productos_table', 12),
(21, '2017_07_14_000001_create_type_products_table', 13),
(22, '2017_07_14_000111_create_type_products_table', 14),
(23, '2017_07_14_00011_create_type_products_table', 15),
(24, '2017_07_30_010427_create_machines_table', 16),
(25, '2017_08_02_025312_create_condicion_ivas_table', 17),
(26, '2017_08_02_031611_create_payment_methods_table', 18),
(27, '2017_08_02_031612_create_payment_methods_table', 19),
(28, '2017_08_03_033955_create_type_people_table', 20),
(29, '2017_08_03_035742_create_people_table', 21),
(30, '2017_09_04_044454_create_people_table', 22),
(31, '2017_09_04_048454_create_people_table', 23),
(32, '2018_10_12_000000_create_users_table', 24),
(33, '2017_10_12_000000_create_users_table', 25),
(34, '2017_08_20_221422_create_machine_statuses_table', 26),
(35, '2017_09_30_010427_create_machines_table', 27),
(36, '2017_08_21_050517_create_countries_table', 28),
(37, '2017_08_21_050524_create_provinces_table', 28),
(38, '2017_08_21_050528_create_cities_table', 28),
(39, '2017_08_26_164926_create_legal_people_table', 29),
(40, '2017_08_26_221218_create_status_invoices_table', 30),
(41, '2017_08_26_221247_create_status_shipments_table', 30),
(42, '2017_08_31_045537_create_invoices_table', 31),
(45, '2017_08_31_045550_create_invoices_table', 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$.6R/srIeNZguf04qZw1Wnu6RQOof3D9tAJM9pqz6VUByPyK7MtilW', '2017-06-18 02:54:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Tarjeta', '2017-08-02 06:26:43', '2017-08-02 06:26:43', NULL),
(2, 'Efectivo', '2017-08-02 06:26:47', '2017-08-02 06:26:47', NULL),
(3, 'Debito', '2017-08-02 06:26:53', '2017-08-02 06:26:53', NULL),
(4, 'Cheque', '2017-08-02 06:26:57', '2017-08-02 06:26:57', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuit` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_condicion_iva` int(10) UNSIGNED DEFAULT NULL,
  `id_type_people` int(10) UNSIGNED DEFAULT NULL,
  `id_city` int(10) UNSIGNED DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_3` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `name`, `lastname`, `email`, `cuit`, `id_condicion_iva`, `id_type_people`, `id_city`, `adress`, `floor`, `department`, `phone_1`, `phone_2`, `phone_3`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Persona', 'No Asignada', 'admin@syspamp.com', '', 1, 2, 1, 'sdfdsfdsf', NULL, NULL, NULL, NULL, NULL, '2017-08-06 06:17:22', '2017-09-06 05:39:57', NULL),
(2, 'SANTIAGO', 'PUJOL', 'ASD', '23423423', 1, 3, 1, 'SFDFDSFDS', NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-06 19:02:12', '2017-08-06 19:02:12'),
(3, 'ADRIANA', 'RUBIO', 'SADASDAS', '3453453', 2, 2, 1, 'DSFDSF', NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-06 19:02:02', '2017-08-06 19:02:02'),
(4, 'TEST2JAJA', 'TEST', 'TEST@TEST324324.COM', 'TESTT', 1, 2, 1, 'EST', '54545', '32423', '32423', '32423', '32423', '2017-08-05 10:19:00', '2017-08-06 19:02:07', '2017-08-06 19:02:07'),
(7, 'DA', 'KE', 'santiagopujol92@gmail.co', '546546', 2, 3, 1, 'Chascomus 1782', NULL, NULL, NULL, NULL, NULL, '2017-08-05 19:21:09', '2017-08-26 21:46:04', '2017-08-26 21:46:04'),
(9, 'TEST', 'TET', 'SAS@ASDSAD.COM', '234234', 1, 2, 1, 'SDASDASD', 'SAD', '324', '324', '324', '324', '2017-08-06 03:57:42', '2017-08-06 19:02:15', '2017-08-06 19:02:15'),
(10, 'ASDASD', 'SADASDSAD', 'SADSA@ASDSA.COM', '4353455', 2, 3, 1, '34534', 'DSAFDS', '345', '345', '345', '345', '2017-08-06 03:58:49', '2017-08-06 19:01:59', '2017-08-06 19:01:59'),
(19, 'dsfdsf', 'dsfds', 'dsfdsdsaf@asdsad', '435435', 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-06 05:58:49', '2017-09-06 05:51:45', '2017-09-06 05:51:45'),
(41, 'Santiago', 'Pujol', 'santiagopujol92@gmail.com', '36354845', 1, 1, 1, 'Chascomus 1782', NULL, NULL, '4643997', '152071376', NULL, '2017-09-06 05:52:48', '2017-09-07 17:23:29', '2017-09-07 17:23:29'),
(42, 'TEST', 'TEST', 'TEST2.@2ERA', '235324', 1, 2, 2, 'TEST', NULL, NULL, NULL, NULL, NULL, '2017-09-06 07:08:40', '2017-09-07 13:45:25', '2017-09-07 13:45:25'),
(43, 'TEST23', 'TEST34', 'TEST2.@2ERA214234sadsad', '435345', 2, 3, 10, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-06 07:08:55', '2017-09-07 13:45:39', '2017-09-07 13:45:39'),
(44, 'KM', 'Jn', 'km@pg1.xn--cw-zja', 'Bd', 1, 1, 14, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-13 01:56:47', '2017-09-13 01:56:47', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `description`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Feldespato', 11, '2017-07-17 06:45:38', '2017-07-17 06:52:11', '2017-07-17 06:52:11'),
(2, 'Moco', 12, '2017-07-17 06:51:17', '2017-07-30 23:56:01', NULL),
(3, 'caca', 11, '2017-07-17 06:52:04', '2017-07-17 06:52:08', '2017-07-17 06:52:08'),
(4, 'asdsa', 12, '2017-07-17 07:07:15', '2017-07-22 21:30:12', '2017-07-22 21:30:12'),
(5, 'adas', 11, '2017-07-17 07:10:03', '2017-07-17 07:10:17', '2017-07-17 07:10:17'),
(6, '45435', 11, '2017-07-17 07:11:13', '2017-07-22 21:30:08', '2017-07-22 21:30:08'),
(7, '435435', 12, '2017-07-17 07:11:21', '2017-07-22 21:30:06', '2017-07-22 21:30:06'),
(8, 'sdfdsf', 11, '2017-07-17 07:12:29', '2017-07-22 21:30:04', '2017-07-22 21:30:04'),
(9, '435', 12, '2017-07-17 07:14:34', '2017-07-22 21:30:02', '2017-07-22 21:30:02'),
(10, 'Addsa', 11, '2017-07-23 00:02:46', '2017-07-23 00:02:52', '2017-07-23 00:02:52'),
(11, 'TEST', 12, '2017-07-23 00:28:19', '2017-07-23 00:28:26', '2017-07-23 00:28:26'),
(12, 'test', 12, '2017-07-23 00:33:47', '2017-07-23 00:33:50', '2017-07-23 00:33:50'),
(13, 'sadsa', 11, '2017-07-30 07:13:42', '2017-07-30 07:13:45', '2017-07-30 07:13:45'),
(14, 'JAja', 12, '2017-07-30 23:27:51', '2017-07-30 23:27:54', '2017-07-30 23:27:54'),
(15, 'JAaa', 12, '2017-07-30 23:55:46', '2017-07-31 00:29:20', '2017-07-31 00:29:20'),
(16, 'JAJAsee', 12, '2017-07-31 00:29:05', '2017-07-31 00:29:18', '2017-07-31 00:29:18'),
(17, 'Administracione', 11, '2017-08-05 23:25:27', '2017-08-05 23:25:38', '2017-08-05 23:25:38'),
(26, 'dsfdsf', 12, '2017-08-06 19:20:37', '2017-08-06 19:20:44', '2017-08-06 19:20:44'),
(27, 'Asdas', 11, '2017-08-06 19:40:26', '2017-08-06 19:40:29', '2017-08-06 19:40:29'),
(28, 'Feldespato', 11, '2017-09-03 01:24:18', '2017-09-03 01:24:18', NULL),
(29, 'Molido', 12, '2017-09-03 01:24:26', '2017-09-03 01:24:26', NULL),
(30, 'ASD', 11, '2017-09-04 00:32:42', '2017-09-04 00:42:03', '2017-09-04 00:42:03'),
(31, 'Addsad', 11, '2017-09-04 00:33:29', '2017-09-04 00:41:58', '2017-09-04 00:41:58'),
(32, 'sdfds', 11, '2017-09-04 00:34:09', '2017-09-04 00:42:01', '2017-09-04 00:42:01'),
(33, 'sef', 12, '2017-09-04 00:37:24', '2017-09-04 00:41:53', '2017-09-04 00:41:53'),
(34, 'sdfds', 12, '2017-09-04 00:37:48', '2017-09-04 00:41:52', '2017-09-04 00:41:52'),
(35, 'asd', 12, '2017-09-04 00:38:04', '2017-09-04 00:41:50', '2017-09-04 00:41:50'),
(36, 'sdf', 12, '2017-09-04 00:38:25', '2017-09-04 00:41:49', '2017-09-04 00:41:49'),
(37, 'sdfsdf', 11, '2017-09-04 00:39:32', '2017-09-04 00:41:48', '2017-09-04 00:41:48'),
(38, 'dsfdsf', 11, '2017-09-04 00:39:49', '2017-09-04 00:41:46', '2017-09-04 00:41:46'),
(39, 'sdfdsf', 12, '2017-09-04 00:41:25', '2017-09-04 00:41:55', '2017-09-04 00:41:55'),
(40, 'sdfds', 12, '2017-09-04 00:41:33', '2017-09-04 00:41:44', '2017-09-04 00:41:44'),
(41, 'Tst', 11, '2017-11-08 03:48:29', '2017-11-08 03:48:29', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provinces`
--

CREATE TABLE `provinces` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `provinces`
--

INSERT INTO `provinces` (`id`, `description`, `id_country`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cordoba', 1, '2017-08-21 08:28:45', '2017-08-22 06:17:29', NULL),
(2, 'Sao Pablo', 2, '2017-08-21 08:29:37', '2017-08-22 06:17:34', NULL),
(3, 'Tucuman', 1, '2017-08-21 08:31:51', '2017-08-22 06:17:45', NULL),
(4, '', 4, '2017-08-21 08:37:26', '2017-08-21 08:37:29', '2017-08-21 08:37:29'),
(5, 'Buenos Aires', 1, '2017-08-22 06:15:00', '2017-08-22 06:17:40', NULL),
(7, 'Caracas', 3, '2017-08-22 06:17:54', '2017-08-22 06:17:54', NULL),
(8, 'SANTA FE', 1, '2017-09-09 02:59:00', '2017-09-09 02:59:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_invoices`
--

CREATE TABLE `status_invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_invoices`
--

INSERT INTO `status_invoices` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Activa', '2017-08-27 01:58:47', '2017-08-27 02:04:36', '2017-08-27 02:04:36'),
(2, 'JAJA', '2017-08-27 02:00:21', '2017-08-27 02:04:37', '2017-08-27 02:04:37'),
(3, 'asdsad', '2017-08-27 02:01:10', '2017-08-27 02:04:34', '2017-08-27 02:04:34'),
(4, 'Activa', '2017-08-27 02:04:41', '2017-09-03 01:38:59', NULL),
(5, 'Pendiente de Pago', '2017-08-27 02:04:44', '2017-09-03 01:39:09', '2017-09-03 01:39:09'),
(6, 'Anulada', '2017-08-27 02:04:50', '2017-08-27 02:04:50', NULL),
(7, 'Finalizada', '2017-09-03 01:39:19', '2017-09-03 01:39:19', NULL),
(8, 'Anulado', '2017-09-07 17:21:45', '2017-09-07 17:22:02', '2017-09-07 17:22:02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_shipments`
--

CREATE TABLE `status_shipments` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_shipments`
--

INSERT INTO `status_shipments` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Activo', '2017-08-27 02:03:12', '2017-08-27 02:03:31', '2017-08-27 02:03:31'),
(2, 'Pendiente de Pago', '2017-08-27 02:03:15', '2017-08-27 02:04:27', '2017-08-27 02:04:27'),
(3, 'Pagado', '2017-08-27 02:03:20', '2017-08-27 02:06:32', '2017-08-27 02:06:32'),
(4, 'A confirmar', '2017-08-27 02:06:40', '2017-08-27 02:06:40', NULL),
(5, 'Preparado Para Facturar', '2017-08-27 02:06:59', '2017-08-27 02:06:59', NULL),
(6, 'tesT', '2017-09-08 02:23:53', '2017-09-08 02:24:01', '2017-09-08 02:24:01'),
(7, 'EDALEER', '2017-09-08 02:24:06', '2017-09-08 02:24:15', '2017-09-08 02:24:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_people`
--

CREATE TABLE `type_people` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_people`
--

INSERT INTO `type_people` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CLIENTE', '2017-08-03 06:53:57', '2017-08-03 06:53:57', NULL),
(2, 'PROVEEDOR', '2017-08-03 06:54:43', '2017-08-03 06:54:43', NULL),
(3, 'EMPLEADO', '2017-08-03 06:55:05', '2017-08-09 08:25:15', NULL),
(4, 'MONO', '2017-08-09 08:36:07', '2017-08-09 08:36:10', '2017-08-09 08:36:10'),
(5, 'TEST', '2017-09-07 14:49:04', '2017-09-07 14:50:12', '2017-09-07 14:50:12'),
(6, 'TEST2', '2017-09-07 14:49:18', '2017-09-07 14:50:06', '2017-09-07 14:50:06'),
(7, 'GERENTE', '2017-09-07 14:50:24', '2017-09-09 02:45:57', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_products`
--

CREATE TABLE `type_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_products`
--

INSERT INTO `type_products` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'madsad', '2017-07-16 21:34:46', '2017-07-16 22:32:54', '2017-07-16 22:32:54'),
(2, 'ASDSAD', '2017-07-16 21:42:18', '2017-07-16 22:05:50', '2017-07-16 22:05:50'),
(3, 'Asdsad', '2017-07-16 22:05:29', '2017-07-16 22:05:42', '2017-07-16 22:05:42'),
(4, 'SI BOLUDO', '2017-07-16 22:32:58', '2017-07-17 02:04:05', '2017-07-17 02:04:05'),
(5, 'feas', '2017-07-16 22:33:08', '2017-07-17 02:04:09', '2017-07-17 02:04:09'),
(6, 'no no va nada', '2017-07-16 22:33:12', '2017-07-17 02:06:40', '2017-07-17 02:06:40'),
(7, 'JAJAJA', '2017-07-17 02:05:36', '2017-07-17 02:05:48', '2017-07-17 02:05:48'),
(8, 'JAJAJA', '2017-07-17 02:06:08', '2017-07-17 02:10:27', '2017-07-17 02:10:27'),
(9, 'DADSAD', '2017-07-17 02:06:32', '2017-07-17 02:11:53', '2017-07-17 02:11:53'),
(10, 'Feldespato', '2017-07-17 02:11:57', '2017-07-17 02:12:21', '2017-07-17 02:12:21'),
(11, 'Piedra', '2017-07-17 02:12:14', '2017-07-30 06:36:43', NULL),
(12, 'Polvo', '2017-07-17 02:12:18', '2017-07-30 03:59:53', NULL),
(13, 'JAJAJA', '2017-07-17 03:44:42', '2017-07-17 03:45:32', '2017-07-17 03:45:32'),
(14, 'Asdsad', '2017-07-17 03:45:09', '2017-07-17 03:45:30', '2017-07-17 03:45:30'),
(15, 'DSFDS', '2017-07-17 03:45:23', '2017-07-17 03:45:25', '2017-07-17 03:45:25'),
(16, 'CACA', '2017-07-17 06:58:47', '2017-07-17 06:59:01', '2017-07-17 06:59:01'),
(17, 'TEST', '2017-07-23 00:28:38', '2017-07-23 00:28:43', '2017-07-23 00:28:43'),
(18, 'jua', '2017-07-30 03:59:02', '2017-07-30 03:59:35', '2017-07-30 03:59:35'),
(19, 'asd', '2017-07-30 06:35:35', '2017-07-30 06:35:38', '2017-07-30 06:35:38'),
(20, 'asd', '2017-07-30 06:36:47', '2017-07-30 06:36:50', '2017-07-30 06:36:50'),
(21, 'asd', '2017-07-30 06:40:53', '2017-07-30 06:40:56', '2017-07-30 06:40:56'),
(22, 'asdsad', '2017-07-30 07:13:57', '2017-07-30 07:14:00', '2017-07-30 07:14:00'),
(23, 'JAJAJA', '2017-07-30 23:18:54', '2017-07-30 23:19:01', '2017-07-30 23:19:01'),
(24, 'Daaae', '2017-07-31 00:29:46', '2017-07-31 00:29:52', '2017-07-31 00:29:52'),
(25, 'asdasd', '2017-08-09 08:34:31', '2017-08-09 08:35:13', '2017-08-09 08:35:13'),
(26, 'NO BLDOasdasdas', '2017-08-09 08:35:16', '2017-08-09 08:35:47', '2017-08-09 08:35:47'),
(27, 'ahi ta ?', '2017-09-04 01:13:26', '2017-09-04 01:17:54', '2017-09-04 01:17:54'),
(28, 'asdasd', '2017-09-04 01:13:57', '2017-09-04 01:17:56', '2017-09-04 01:17:56'),
(29, 'fghfgh', '2017-09-04 01:15:12', '2017-09-04 01:18:33', '2017-09-04 01:18:33'),
(30, 'bueno bldo', '2017-09-04 01:18:36', '2017-09-04 01:19:41', '2017-09-04 01:19:41'),
(31, 'SALEE', '2017-09-04 01:20:51', '2017-09-04 01:22:21', '2017-09-04 01:22:21'),
(32, 'dfgfdg', '2017-09-04 01:25:24', '2017-09-04 01:26:55', '2017-09-04 01:26:55'),
(33, 'dsfdsf', '2017-09-04 01:30:46', '2017-09-04 01:32:48', '2017-09-04 01:32:48'),
(34, 'sdfdsf', '2017-09-04 01:30:50', '2017-09-04 01:32:56', '2017-09-04 01:32:56'),
(35, 'sdfdsf', '2017-09-04 01:30:53', '2017-09-04 01:32:58', '2017-09-04 01:32:58'),
(36, 'dsfdsf', '2017-09-04 01:30:57', '2017-09-04 01:32:52', '2017-09-04 01:32:52'),
(37, 'sfsdfds', '2017-09-04 01:32:29', '2017-09-04 01:32:44', '2017-09-04 01:32:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_users`
--

CREATE TABLE `type_users` (
  `id` int(11) NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_users`
--

INSERT INTO `type_users` (`id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'Administracion', NULL, '2017-08-09 07:28:31', NULL),
(3, 'Produccion', NULL, '2017-08-09 07:38:09', NULL),
(4, 'Mantenimiento', NULL, NULL, NULL),
(5, 'Laboratorio', NULL, '2017-08-09 07:23:07', NULL),
(6, 'Consultor', NULL, NULL, NULL),
(8, 'tester', '2017-08-06 06:32:20', '2017-08-06 06:32:27', '2017-08-06 06:32:27'),
(9, 'HAASD', '2017-08-07 01:46:31', '2017-08-07 01:46:39', '2017-08-07 01:46:39'),
(11, 'asd', '2017-08-07 01:47:01', '2017-08-09 06:50:00', '2017-08-09 06:50:00'),
(34, 'asdsaTEST6', '2017-08-06 22:59:14', '2017-08-06 23:00:22', '2017-08-06 23:00:22'),
(45, 'DSFDS', '2017-08-07 01:49:25', '2017-08-09 06:53:19', '2017-08-09 06:53:19'),
(56, 'TESTTE', '2017-08-07 02:21:00', '2017-08-09 07:28:33', '2017-08-09 07:28:33'),
(99, 'teST', '2017-08-06 23:00:15', '2017-08-06 23:00:18', '2017-08-06 23:00:18'),
(123, 'TESTasdsa', '2017-08-06 22:56:21', '2017-08-06 22:56:26', '2017-08-06 22:56:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `status` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `password`, `type`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'Admin', 'admin', '$2y$10$2hqnGsiOJvXlA5LdBJgNSOWZBeJjrG3OookVtnoLYlZaSj.5qlzui', 1, 'on', '2izHTqwC0Nyh6iQRCNSwUXFDQisiwwVkUvdXLt9yjfL8HgWcZANNGnHMb1Iy', NULL, '2017-09-03 20:48:03', NULL),
(2, 'Administracion', 'Administracion', 'administracion', '$2y$10$cwZA8FHXI17VTtWRWFGcWuBURfbTTRPm1Vt.GYX33aPqzATAzd28q', 3, 'on', NULL, NULL, '2017-09-13 02:00:06', NULL),
(3, 'Produccion', 'Produccion', 'produccion', '$2y$10$eQonoZlSZpQ1HwPsHBTPPuCDNgXJXDXdB6FEd2tFVDUec3AsJo1Ya', 3, 'on', '3mUOyZTiHvjhl9ETMEw2kG1Y6iELkFH7SUY9fJMTtrcmMiMlQR1MQZfuE0zi', '2017-08-06 22:37:46', '2017-08-07 01:14:21', NULL),
(4, 'Mantenimiento', 'Mantenimiento', 'mantenimiento', '$2y$10$kQghFJPSUMgOmNxGHC0pT.qno1ctpaByBAwgoVWM72Wueh6mS437e', 4, 'on', NULL, NULL, '2017-08-07 01:14:13', NULL),
(5, 'Laboratorio', 'Laboratorio', 'laboratorio', '$2y$10$fMFCcS/beGdrIp2SJiIZ6.XlRKkeUiInafdkPYVe203Y0mjbyiruO', 5, 'on', NULL, NULL, '2017-09-03 20:49:30', NULL),
(6, 'Consultor', 'Consultor', 'consultor', '$2y$10$wa2FUEXEGffZ3k4kXGny9.meses7Egn/dJtxDcTdL/kPO8AZUtMWq', 6, 'on', NULL, '2017-08-07 01:03:57', '2017-08-07 01:13:29', NULL),
(7, 'TESTTE', 'TEST', 'TEST@asd.com', '$2y$10$ip7.Mjmu9alexOWA8H1RAOlYEmTHgHniApswDlUdc6eJAD7EEA/oq', 1, 'on', NULL, '2017-08-06 22:37:46', '2017-08-06 22:37:50', '2017-08-06 22:37:50'),
(8, 'TEST2', 'TEST23', 'TEST45@ASDSAD.COM', '$2y$10$QDzXN29eGA926YmA4m6uI.J0GUAbYFl/bXzR2s07h5mBTHaulSM6q', 2, 'on', NULL, '2017-08-06 22:53:07', '2017-08-06 22:54:44', '2017-08-06 22:54:44'),
(9, 'TEST23', 'TEST4', '444@ASDSAD.COM', '$2y$10$M.0gL7skT./wnnQDFPfP..sO1xUouWFvqWEIBiXJOEInvyx7D5S36', 5, 'on', NULL, '2017-08-06 23:02:29', '2017-08-06 23:02:40', '2017-08-06 23:02:40'),
(10, 'ASDSAd', 'asdsad', 'asdsa@asdsad.com', '$2y$10$Q34sP728V.O53y0cCEf2OulYH37OFNKlKd1/5HHIKL7HLbCqL5hG6', 5, 'on', NULL, '2017-08-06 23:03:36', '2017-08-06 23:38:10', '2017-08-06 23:38:10'),
(11, 'ASD', 'ASDSA', 'ASDA2ASDSAD@ASDSAD.COM', '$2y$10$bFFdoMmW57fwtj9m1vrHUOB5sq.uKYg/VgQc65Dn1DXZs2oFylN6C', 2, 'on', NULL, '2017-08-06 23:37:57', '2017-08-06 23:38:07', '2017-08-06 23:38:07'),
(12, 'TEST', 'TEST34', 'TEST@TEST.COM', '$2y$10$qkgSSZRVwG1LJK/kVUtAWeL6DmfUVvnN7AvpwX0igm8ouwh06bNKC', 2, 'on', 'tOJqy7yUWvRbJQmuDrb3Pn2gvEtwnwnGHulqoEf9oHIMEQc1iNjGcL5MDsDU', '2017-08-07 00:38:01', '2017-08-07 01:13:01', '2017-08-07 01:13:01'),
(13, 'ASDSAD', 'ASDSAD', 'ASDSA@SADASD.com', '$2y$10$RThabVdqVDnIA6LD0YGWJew2jB/UUudWUBdeSzBos.CL1zXzRySJq', 2, 'off', NULL, '2017-08-07 01:03:57', '2017-08-07 01:04:03', '2017-08-07 01:04:03'),
(14, 'TEST', 'TEST', 'TEST@xn--est-o0a.com', '$2y$10$xawOCLa4nd5RVLr1pPt6NeOd4UgPD6ZI1CiSbjQHShyrVNsh4CZUO', 2, 'on', NULL, '2017-08-07 01:10:27', '2017-08-07 01:10:33', '2017-08-07 01:10:33'),
(15, 'ASDSAD', 'ASDASD', 'ASDSAD@HOTMOAIL.COM', '$2y$10$2iYf/4MZ6IeJEtXIwG3DG.7YZzHEQOJXsBxd4snqiFcn7TfUYN9rS', 6, 'on', NULL, '2017-09-03 02:26:02', '2017-09-03 02:26:14', '2017-09-03 02:26:14');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_id_province_foreign` (`id_province`);

--
-- Indices de la tabla `condicion_ivas`
--
ALTER TABLE `condicion_ivas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_invoice_number_unique` (`invoice_number`),
  ADD KEY `invoices_id_person_foreign` (`id_person`),
  ADD KEY `invoices_id_payment_method_foreign` (`id_payment_method`),
  ADD KEY `invoices_id_product_foreign` (`id_product`),
  ADD KEY `invoices_id_status_invoice_foreign` (`id_status_invoice`),
  ADD KEY `invoices_id_legal_person_foreign` (`id_legal_person`);

--
-- Indices de la tabla `legal_people`
--
ALTER TABLE `legal_people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `legal_people_cuit_unique` (`cuit`),
  ADD KEY `legal_people_id_condicion_iva_foreign` (`id_condicion_iva`),
  ADD KEY `legal_people_id_type_people_foreign` (`id_type_people`),
  ADD KEY `legal_people_id_city_foreign` (`id_city`);

--
-- Indices de la tabla `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `machines_id_status_machine_foreign` (`id_status_machine`);

--
-- Indices de la tabla `machine_statuses`
--
ALTER TABLE `machine_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `people_cuit_unique` (`cuit`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `people_id_condicion_iva_foreign` (`id_condicion_iva`),
  ADD KEY `people_id_type_people_foreign` (`id_type_people`),
  ADD KEY `id_city` (`id_city`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indices de la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinces_id_country_foreign` (`id_country`);

--
-- Indices de la tabla `status_invoices`
--
ALTER TABLE `status_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_shipments`
--
ALTER TABLE `status_shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_people`
--
ALTER TABLE `type_people`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_products`
--
ALTER TABLE `type_products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_users`
--
ALTER TABLE `type_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_type_foreign` (`type`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `condicion_ivas`
--
ALTER TABLE `condicion_ivas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `legal_people`
--
ALTER TABLE `legal_people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `machines`
--
ALTER TABLE `machines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `machine_statuses`
--
ALTER TABLE `machine_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `status_invoices`
--
ALTER TABLE `status_invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `status_shipments`
--
ALTER TABLE `status_shipments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `type_people`
--
ALTER TABLE `type_people`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `type_products`
--
ALTER TABLE `type_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_id_province_foreign` FOREIGN KEY (`id_province`) REFERENCES `provinces` (`id`);

--
-- Filtros para la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_id_legal_person_foreign` FOREIGN KEY (`id_legal_person`) REFERENCES `legal_people` (`id`),
  ADD CONSTRAINT `invoices_id_payment_method_foreign` FOREIGN KEY (`id_payment_method`) REFERENCES `payment_methods` (`id`),
  ADD CONSTRAINT `invoices_id_person_foreign` FOREIGN KEY (`id_person`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `invoices_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `invoices_id_status_invoice_foreign` FOREIGN KEY (`id_status_invoice`) REFERENCES `status_invoices` (`id`);

--
-- Filtros para la tabla `legal_people`
--
ALTER TABLE `legal_people`
  ADD CONSTRAINT `legal_people_id_city_foreign` FOREIGN KEY (`id_city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `legal_people_id_condicion_iva_foreign` FOREIGN KEY (`id_condicion_iva`) REFERENCES `condicion_ivas` (`id`),
  ADD CONSTRAINT `legal_people_id_type_people_foreign` FOREIGN KEY (`id_type_people`) REFERENCES `type_people` (`id`);

--
-- Filtros para la tabla `machines`
--
ALTER TABLE `machines`
  ADD CONSTRAINT `machines_id_status_machine_foreign` FOREIGN KEY (`id_status_machine`) REFERENCES `machine_statuses` (`id`);

--
-- Filtros para la tabla `people`
--
ALTER TABLE `people`
  ADD CONSTRAINT `people_ibfk_1` FOREIGN KEY (`id_city`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `people_id_condicion_iva_foreign` FOREIGN KEY (`id_condicion_iva`) REFERENCES `condicion_ivas` (`id`),
  ADD CONSTRAINT `people_id_type_people_foreign` FOREIGN KEY (`id_type_people`) REFERENCES `type_people` (`id`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type_products` (`id`);

--
-- Filtros para la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `provinces_id_country_foreign` FOREIGN KEY (`id_country`) REFERENCES `countries` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_type_foreign` FOREIGN KEY (`type`) REFERENCES `type_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
